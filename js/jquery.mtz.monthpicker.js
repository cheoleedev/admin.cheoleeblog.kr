/*
 * jQuery UI Monthpicker
 *
 * @licensed MIT <see below>
 * @licensed GPL <see below>
 *
 * @author Luciano Costa
 * http://lucianocosta.com.br/jquery.mtz.monthpicker/
 *
 * Depends:
 *  jquery.ui.core.js
 */

/**
 * MIT License
 * Copyright (c) 2011, Luciano Costa
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * GPL LIcense
 * Copyright (c) 2011, Luciano Costa
 * 
 * This program is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by the 
 * Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/** -------------
 * 2021/06/09(수)
 * start_month는 end_month 이후를 선택할 수 없고, 
 * end_month는 start_month 이전을 선택할 수 없도록 수정
----------------*/

;(function ($) {
    var year_old = 0;   // 기존 선택 연도
    var month_old = 0;  // 기존 선택 월
    var id = '';    // 클릭한 요소 id를 담을 변수를 전역 변수로 선언한다.
    var methods = {
        init : function (options) { 
            return this.each(function () {
                var 
                    $this = $(this),
                    data = $this.data('monthpicker'),
                    year = (options && options.year) ? options.year : (new Date()).getFullYear(),
                    settings = $.extend({
                        pattern: 'mm/yyyy',
                        selectedMonth: null,
                        selectedMonthName: '',
                        selectedYear: year,
                        startYear: year - 10,
                        finalYear: year + 10,
                        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                        id: "monthpicker_" + (Math.random() * Math.random()).toString().replace('.', ''),
                        openOnFocus: true,
                        disabledMonths: []
                    }, options);

                settings.dateSeparator = settings.pattern.replace(/(mmm|mm|m|yyyy|yy|y)/ig,'');
                
                // If the plugin hasn't been initialized yet for this element
                if (!data) {

                    $(this).data('monthpicker', {
                        'target': $this,
                        'settings': settings
                    });
                    
                    if (settings.openOnFocus === true) {
                        $this.on('focus', function () {
                            $this.monthpicker('show');
                        });
                    }
                    // 요소 구분을 위한 id를 가져온다.
                    id = $this.context.id;

                    $this.monthpicker('parseInputValue', settings);

                    $this.monthpicker('mountWidget', settings);

                    $this.on('monthpicker-click-month', function (e, month) {
                        console.log(settings);
                        $this.monthpicker('setValue', settings);
                        $this.monthpicker('hide');
                    });

                    // hide widget when user clicks elsewhere on page
                    $this.addClass("mtz-monthpicker-widgetcontainer");
                    $(document).unbind("mousedown.mtzmonthpicker").on("mousedown.mtzmonthpicker", function (e) {
                        if (!e.target.className || e.target.className.toString().indexOf('mtz-monthpicker') < 0) {
                            $(this).monthpicker('hideAll'); 
                        }
                    });
                }
            });
        },

        show: function () {
            $(this).monthpicker('hideAll'); 
            var widget = $('#' + this.data('monthpicker').settings.id);
            widget.css("top", this.offset().top  + this.outerHeight());
            if ($(window).width() > (widget.width() + this.offset().left) ){
                widget.css("left", this.offset().left);
            } else {
                widget.css("left", this.offset().left - widget.width());
            }
            widget.show();
            widget.find('select').focus();
            this.trigger('monthpicker-show');
        },

        hide: function () {
            var widget = $('#' + this.data('monthpicker').settings.id);
            if (widget.is(':visible')) {
                widget.hide();
                this.trigger('monthpicker-hide');
            }
        },

        hideAll: function () {
            $(".mtz-monthpicker-widgetcontainer").each(function () {
                if (typeof($(this).data("monthpicker"))!="undefined") { 
                    $(this).monthpicker('hide'); 
                }
            });
        },

        setValue: function (settings) {
            year_old = $('#year_old').val();    // 기존 선택한 연도의 값을 가져온다.
            month_old = $('#month_old').val();  // 기존 선택한 월의 값을 가져온다.
            var             
                month = settings.selectedMonth,
                year = settings.selectedYear;
            
                console.log('setValue : '+ year_old+' , '+month_old);

                if(settings.pattern.indexOf('mmm') >= 0) {
                    month = settings.selectedMonthName;
                } else if(settings.pattern.indexOf('mm') >= 0 && settings.selectedMonth < 10) {
                    month = '0' + settings.selectedMonth;
                }

                if(settings.pattern.indexOf('yyyy') < 0) {
                    year = year.toString().substr(2,2);
                } 

                if (settings.pattern.indexOf('y') > settings.pattern.indexOf(settings.dateSeparator)) {
                    this.val(month + settings.dateSeparator + year);
                } else {
                    this.val(year + settings.dateSeparator + month);
                }
                this.change();
        },

        disableMonths: function (months) {
            var 
                settings = this.data('monthpicker').settings,
                container = $('#' + settings.id);

            settings.disabledMonths = months;

            container.find('.mtz-monthpicker-month').each(function () {
                var m = parseInt($(this).data('month'));
                if ($.inArray(m, months) >= 0) {
                    $(this).addClass('ui-state-disabled');
                } else {
                    $(this).removeClass('ui-state-disabled');
                }
            });
        },

        mountWidget: function (settings) {
                
            var
                monthpicker = this,
                container = $('<div id="'+ settings.id +'" mark="'+id+'"class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" />'),
                header = $('<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all mtz-monthpicker" />'),
                combo = $('<select class="mtz-monthpicker mtz-monthpicker-year" />'),
                table = $('<table class="mtz-monthpicker" />'),
                tbody = $('<tbody class="mtz-monthpicker" />'),
                tr = $('<tr class="mtz-monthpicker" />'),
                td = '',
                selectedYear = settings.selectedYear,
                option = null,
                attrSelectedYear = $(this).data('monthpicker').settings.selectedYear,
                attrStartYear = $(this).data('monthpicker').settings.sartYear,
                attrFinalYear = $(this).data('monthpicker').settings.finalYear;
            // console.log('attrSelectedYear : '+attrSelectedYear);
            // console.log('mountWidget');
            console.log($(this).data('monthpicker').settings.selectedYear);
            if (attrSelectedYear) {
                settings.selectedYear = attrSelectedYear;
            }

            if (attrStartYear) {
                settings.startYear = attrStartYear;
            }

            if (attrFinalYear) {
                settings.finalYear = attrFinalYear;
            }

            container.css({
                position:'absolute',
                zIndex:999999,
                whiteSpace:'nowrap',
                width:'250px',
                // fontSize: '.75rem',
                overflow:'hidden',
                textAlign:'center',
                display:'none',
                top: monthpicker.offset().top + monthpicker.outerHeight(),
                left: monthpicker.offset().left
            });

            combo.on('change', function () { 
                var months = $(this).parent().parent().find('td[data-month]');
                year_old = $('#year_old').val();    // 기존 선택한 연도의 값을 가져온다.
                var start_year_old = Number($('#start_month_old').val().split('-')[0]);
                var end_year_old = Number($('#end_month_old').val().split('-')[0]);
                month_old = $('#month_old').val();  // 기존 선택한 월의 값을 가져온다.
                id =$("#"+settings.id+"").attr('mark'); // monthpicker 컨테이너 요소에서 mark 값을 가져온다.
                if(id == 'start_month'){ // 시작월의 monthpicker일 경우
                    if($(this).val() == end_year_old) { // 새로 선택한 연도가 현재 설정된 종료월 연도와 같을 경우
                        // console.log('change_selected_1 : '+id+' , '+$(this).val()+' , '+end_year_old+' , '+year_old+' , '+settings.selectedYear+' , '+month_old+' , '+settings.selectedMonth);
                        months.removeClass('ui-state-active');
                        if ($(this).val() == settings.selectedYear) {
                            months.filter('td[data-month='+ Number(settings.selectedMonth) +']').addClass('ui-state-active');
                        }
                    } else if ($(this).val() < end_year_old){ // 새로 선택한 연도가 현재 설정된 종료월 연도보다 작거을 경우
                        // console.log('change_selected_2 : '+id+' , '+$(this).val()+' , '+end_year_old+' , '+year_old+' , '+settings.selectedYear+' , '+month_old+' , '+settings.selectedMonth);
                        months.removeClass('ui-state-active');
                    } else { // 새로 선택한 연도가 현재 설정된 종료월 연도보다 클 경우
                        // console.log('change_year_old : '+id+' , '+$(this).val()+' , '+end_year_old+' , '+year_old+' , '+settings.selectedYear+' , '+month_old+' , '+settings.selectedMonth);
                        combo.empty();
                        for (var i = settings.startYear; i <= settings.finalYear; i++) {
                            var option = $('<option class="mtz-monthpicker" />').attr('value', i).append(i);
                            // console.log('기존에 선택된 연도 : '+start_year_old+' , '+i);
                            if (start_year_old == i) {
                                option.attr('selected', 'selected');
                            }
                            combo.append(option);
                        }
                        $('#toast_message').remove();
                        container.append(
                            '<!-- 토스트 메세지 요소 -->'+
                            '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100 " id="toast_message">'+
                                '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
                                '<div class="d-flex justify-content-center">'+
                                    '<div class="toast-body bg-warning">'+
                                    '종료월 이후 연도는 선택할 수 없습니다.'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                            '</div>' 
                        )
                        toast_init();

                    }
                } else { // 종료월의 monthpicker일 경우
                    if($(this).val() == start_year_old) { // 새로 선택한 연도가 현재 설정된 시작월 연도와 같을 경우
                        // console.log('change_selected_1 : '+id+' , '+$(this).val()+' , '+start_year_old+' , '+year_old+' , '+settings.selectedYear+' , '+month_old+' , '+settings.selectedMonth);
                        months.removeClass('ui-state-active');
                        if ($(this).val() == settings.selectedYear) {
                            months.filter('td[data-month='+ Number(settings.selectedMonth) +']').addClass('ui-state-active');
                        }
                    } else if ($(this).val() > start_year_old){ // 새로 선택한 연도가 현재 설정된 시작월 연도보다 클 경우
                        // console.log('change_selected_2 : '+id+' , '+$(this).val()+' , '+start_year_old+' , '+year_old+' , '+settings.selectedYear+' , '+month_old+' , '+settings.selectedMonth);
                        months.removeClass('ui-state-active');
                    } else { // 새로 선택한 연도가 현재 설정된 시작월 연도보다 작을 경우
                        // console.log('change_year_old : '+id+' , '+$(this).val()+' , '+start_year_old+' , '+year_old+' , '+settings.selectedYear+' , '+month_old+' , '+settings.selectedMonth);
                        combo.empty();
                        for (var i = settings.startYear; i <= settings.finalYear; i++) {
                            var option = $('<option class="mtz-monthpicker" />').attr('value', i).append(i);
                            // console.log('기존에 선택된 연도 : '+end_year_old+' , '+i);
                            if (end_year_old == i) {
                                option.attr('selected', 'selected');
                            }
                            combo.append(option);
                        }
                        $('#toast_message').remove();
                        container.append(
                            '<!-- 토스트 메세지 요소 -->'+
                            '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
                                '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
                                '<div class="d-flex justify-content-center">'+
                                    '<div class="toast-body bg-warning">'+
                                    '시작월 이전 연도는 선택할 수 없습니다.'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                            '</div>' 
                        )
                        toast_init();
                    }
                }
                monthpicker.trigger('monthpicker-change-year', $(this).val());
            });
            console.log('combo : '+year_old+' , '+settings.selectedYear+' , '+month_old+' , '+settings.selectedMonth);
            // mount years combo
            for (var i = settings.startYear; i <= settings.finalYear; i++) {
                var option = $('<option class="mtz-monthpicker" />').attr('value', i).append(i);
                // console.log('선택된 연도 : '+settings.selectedYear+' , '+i);
                if(year_old == 0){
                    if (settings.selectedYear == i) {
                        option.attr('selected', 'selected');
                    }
                } else {
                    if (year_old == i) {
                        option.attr('selected', 'selected');
                    }
                }
                combo.append(option);
            }
            header.append(combo).appendTo(container);

            // mount months table
            for (var i=1; i<=12; i++) {
                td = $('<td class="ui-state-default mtz-monthpicker mtz-monthpicker-month" style="padding:5px;cursor:default;" />').attr('data-month',i);
                if(month_old == 0){
                    if (settings.selectedMonth == i) {
                        td.addClass('ui-state-active');
                    }
                } else {
                    if (month_old == i) {
                        td.addClass('ui-state-active');
                    }
                }
                td.append(settings.monthNames[i-1]);
                tr.append(td).appendTo(tbody);
                if (i % 3 === 0) {
                    tr = $('<tr class="mtz-monthpicker" />'); 
                }
            }

            tbody.find('.mtz-monthpicker-month').on('click', function () {
                var m = parseInt($(this).data('month'));
                if ($.inArray(m, settings.disabledMonths) < 0 ) {
                    settings.selectedYear = $(this).closest('.ui-datepicker').find('.mtz-monthpicker-year').first().val();
                    settings.selectedMonth = $(this).data('month');
                    settings.selectedMonthName = $(this).text();
                    console.log('mtz-monthpicker-month_click : '+month_old+' ,'+year_old);
                    monthpicker.trigger('monthpicker-click-month', $(this).data('month'));
                    if(year_old == 0 && month_old == 0){ // 기존 선택한 연도와 월의 값이 모두 0일 경우만 클래스를 추가한다.
                        $(this).closest('table').find('.ui-state-active').removeClass('ui-state-active');
                        $(this).addClass('ui-state-active');
                    }
                }
            });

            table.append(tbody).appendTo(container);

            container.appendTo('body');
        },

        destroy: function () {
            return this.each(function () {
                $(this).removeClass('mtz-monthpicker-widgetcontainer').unbind('focus').removeData('monthpicker');
            });
        },

        getDate: function () {
            var settings = this.data('monthpicker').settings;
            if (settings.selectedMonth && settings.selectedYear) {
                return new Date(settings.selectedYear, settings.selectedMonth -1);
            } else {
                return null;
            }
        },

        parseInputValue: function (settings) {
            if (this.val()) {
                console.log('parseInputValue');
                console.log(this.val());
                if (settings.dateSeparator) {
                    var val = this.val().toString().split(settings.dateSeparator);
                    if (settings.pattern.indexOf('m') === 0) {
                        settings.selectedMonth = val[0];
                        settings.selectedYear = val[1];
                    } else {
                        settings.selectedMonth = val[1];
                        settings.selectedYear = val[0];                                
                    }
                }
            }
        }
        
    };

    $.fn.monthpicker = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.mtz.monthpicker');
        }    
    };

})(jQuery);
