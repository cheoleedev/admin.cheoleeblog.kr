function printClock() {
    
    var clock = document.getElementById("clock");            // 출력할 장소 선택
    var currentDate = new Date();                                     // 현재시간
    var day_num = Number(currentDate.getDay());
    var day = day_kor(day_num);
    var calendar = currentDate.getFullYear() + "-" + (currentDate.getMonth()+1) + "-" + currentDate.getDate() + "(" + day +")";// 현재 날짜
    var amPm = 'AM'; // 초기값 AM
    var currentHours = addZeros(currentDate.getHours(),2); 
    var currentMinute = addZeros(currentDate.getMinutes() ,2);
    var currentSeconds =  addZeros(currentDate.getSeconds(),2);
    
    // if(currentHours >= 12){ // 시간이 12보다 클 때 PM으로 세팅, 12를 빼줌
    // 	amPm = 'PM';
    // 	currentHours = addZeros(currentHours - 12,2);
    // }

    // if(currentSeconds >= 50){// 50초 이상일 때 색을 변환해 준다.
    //    currentSeconds = '<span class="border-start-0" style="color:#de1951; font-size:1rem; margin-bottom: 1rem;">'+currentSeconds+'</span>'
    // }
    // clock.innerHTML = calendar+" "+currentHours+":"+currentMinute+":"+currentSeconds +" <span style='font-size: 1rem; margin-bottom: 1rem;'>"+ amPm+"</span>";
    // clock.innerHTML = '<span class="border rounded" style="color:white; font-size:1rem; background-color:#7885ac; padding: 5px; margin-bottom: 1rem;">' +calendar+" "+currentHours+":"+currentMinute+":" + currentSeconds +'</span>'; //날짜를 출력해 줌
    setTimeout("printClock()",1000);         // 1초마다 printClock() 함수 호출
}

function addZeros(num, digit) { // 자릿수 맞춰주기
	  var zero = '';
	  num = num.toString();
	  if (num.length < digit) {
	    for (i = 0; i < digit - num.length; i++) {
	      zero += '0';
	    }
	  }
	  return zero + num;
}
function day_kor(num) { // 한글 요일이름 가져오기
    var day_list= ['일','월','화','수','목','금','토'];
    var day_kor = '';
    // console.log(day_list);
    for (i=0; i < day_list.length; ++i) {
        if(i == num) {
            day_kor = day_list[i];
        }
    }
    return day_kor;
}

function nav_click(obj){
  var menu_name = $(obj).attr('name');
  $('li').find('a').each(function(index){
    var name_tmp= $(this).attr('name');
    // console.log(menu_name+','+name_tmp);
    // if(menu_name == name_tmp){
    //   $(this).attr('class','nav-link active');
    //   $(this).attr('aria-current','page');
    // } 
    // else {
    //   // $(this).attr('class','nav-link');
    //   $(this).removeAttr('aria-current');
    //   // $(this).removeAttr('class','active');
    // }
  });
}

// -- end of ckeditor 4

// flash alert delete
function flash_alert_delete(){
  $('#flash_alert').remove();
}

// 선택한 타이틀 이미지 파일명을 해당 요소에 입력
function title_select(obj){
  var selected_title = obj.getAttribute('file_name');
  // console.log(selected_title);
  $("#selected_title").val(selected_title);
  var is_use = $("input[name='is_use']");
  var is_activate = $("input[name='is_activate']");
  is_use[0].id = "is_use_"+selected_title; // 체크 박스 요소의 id를 선택한 타이틀에 맞게 업데이트 한다.
  is_activate[0].id = "is_activate_"+selected_title; // 체크 박스 요소의 id를 선택한 타이틀에 맞게 업데이트 한다.
  // console.log(is_use[0].id);
  // console.log(is_activate[0].id);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/gettitleimg',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      selected_title : selected_title,
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var is_activate = result_array.is_activate;
      var is_use = result_array.is_use;
      var title_href = result_array.title_href;
      console.log(selected_title +' is_activate : '+ is_activate+' is_use : '+ is_use);
      
      $("#title_href").val(title_href);
      // 사용 여부 체크 상태 반영
      $('#is_use_checkbox').empty();
      if (is_use == 'Y' ){
        // console.log('is_use_'+selected_title+','+is_use);
        $('#is_use_checkbox').append(
          '<span title="사용 여부"><input class="form-check-input ms-1" type="checkbox" id="is_use_'+ selected_title+'" name="is_use" checked  onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_use"></span>'
        );
      } else {
        // console.log('is_use_'+selected_title+','+is_use);
        $('#is_use_checkbox').append(
          '<span title="사용 여부"><input class="form-check-input ms-1" type="checkbox" id="is_use_'+ selected_title+'" name="is_use" onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_use"></span>'
        );
      }
      // 대표 이미지 여부 체크 상태 반영
      $('#is_activate_checkbox').empty();
      if (is_activate == 'Y' ){
        // console.log('is_activate_'+selected_title+','+is_activate);
        $('#is_activate_checkbox').append(
          '<span title="사용 여부"><input class="form-check-input ms-1" type="checkbox" id="is_activate_'+ selected_title+'" name="is_activate" checked  onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_activate"></span>'
        );  
      } else {
        // console.log('is_activate_'+selected_title+','+is_activate);
        $('#is_activate_checkbox').append(
          '<span title="사용 여부"><input class="form-check-input ms-1" type="checkbox" id="is_activate_'+ selected_title+'" name="is_activate" onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_activate"></span>'
        );
      }
    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 선택한 타이틀 이미지의 설정 상태를 변경한다.
function title_href_update(){
  var selected_title = $('#selected_title').val();
  var title_href = $('#title_href').val();
  console.log('selected_title :'+selected_title+', title_href : '+ title_href);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/titlehrefupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      selected_title : selected_title,
      title_href : title_href,
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var new_title_img = result_array.title_img;
      console.log(new_title_img +' title_href : '+ result_array.title_href);
      alert('타이틀 이미지의 링크가 업데이트 되었습니다.\r\r'+result_array.title_href);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 선택한 타이틀 이미지의 설정 상태를 변경한다.
function title_status_update(obj){
  var mark = $(obj).attr('mark');
  var status = $(obj).is(':checked') ? 'Y' : 'N'; // 체크 여부 확인
  var id = $(obj).attr('id');
  console.log(id +' status : '+ status +' mark : '+ mark);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/titlestatusupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      mark : mark,
      status : status,
      id : id
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var new_title_img = result_array.title_img;
      console.log(new_title_img +' is_activate : '+ result_array.is_activate+' is_use : '+ result_array.is_use);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 메인 주제 및 하위 주제 체크 박스 체크 내용 반영
function subject_check(obj){
  var is_activate = $(obj).is(':checked') ? 'Y' : 'N'; // 체크 여부 확인
  var subject_code = $(obj).attr('id');
  var main_subject_code = document.getElementById('main_subject_code').value;
 
  // var host = location.host ; 
  console.log(is_activate +','+ subject_code + ',' + main_subject_code);
  // 실시간 처리를 위한 uri redirect (필요한 변수를 get방식으로 보낸다.)
  // window.location.href = '/setting/subjectcheck'+'?maincode='+main_subject_code+'&subject='+subject_code+'&activate='+is_activate;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/subjectcheck',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      main_subject_code : main_subject_code,
      subject_code : subject_code,
      is_activate : is_activate

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var main_subject_code = result_array.main_subject_code;
      console.log(main_subject_code);


    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 메인 주제를 추가한다.
function main_subject_add(){
  var main_subject = $('#main_subject').val();
  var color = get_random_color();

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/mainsubjectadd',   //정보 처리를 할 서버 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      
      main_subject : main_subject,
      color : color
    
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var main_subject = data.extra;

      var main_subject_name = main_subject.main_subject_name;
      var main_subject_code = main_subject.main_subject_code;
      var color = main_subject.color;
      var is_activate = main_subject.is_activate;
      
      if(is_activate == 'Y') { // 활성화 여부를 구분하여 요소를 생성해준다.
        $("#main_subject_list").append(
          '<li class="list-group-item border-bottom subject">'+
            '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'+
              '<div class="flex-grow-1">'+
                '<label class="form-check-label w-100" for="" onclick="" code="'+ main_subject_code +'" >'+ '<input class="form-control subject_input" id="subject_value_'+main_subject_code+'" type="text" value="'+main_subject_name+'"></label>'+
                '<input type="hidden" id="old_subject_value_'+main_subject_code+'" value="'+main_subject_name+'">'+
              '</div>'+
              '<input type="color" value="'+color+'" id="subject_color_'+main_subject_code+'" style="width:25px; height:25px; border:none; margin-right: 8px; padding:0;">'+
              '<button type="button" class="btn btn-sm btn-outline-primary" onclick="update_subject(this)" code="'+ main_subject_code +'" style="margin-right:40px;">'+
                '<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">'+
                '<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                '</svg></span>'+
              '</button>'+
              '<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="'+ main_subject_code +'" checked onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>'+
            '</div>'+
        '</li>'                 
        );
      } else {
        $("#main_subject_list").append(
          '<li class="list-group-item border-bottom subject">'+
            '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'+
              '<div class="flex-grow-1">'+
                '<label class="form-check-label w-100" for="" onclick="" code="'+ main_subject_code +'" >'+ '<input class="form-control subject_input" id="subject_value_'+main_subject_code+'" type="text" value="'+main_subject_name+'"></label>'+
                '<input type="hidden" id="old_subject_value_'+main_subject_code+'" value="'+main_subject_name+'">'+
              '</div>'+
              '<input type="color" value="'+color+'" id="subject_color_'+main_subject_code+'" style="width:25px; height:25px; border:none; margin-right: 8px; padding:0;">'+
              '<button type="button" class="btn btn-sm btn-outline-primary" onclick="update_subject(this)" code="'+ main_subject_code +'" style="margin-right:40px;">'+
                '<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">'+
                '<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                '</svg></span>'+
              '</button>'+
              '<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="'+ main_subject_code +'" onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>'+
            '</div>'+
        '</li>'
        );
      }

      alert('메인 주제가 추가 되었습니다.\n\n'+'- 새로운 주제 : "'+main_subject_name+'\n\n"'+'- color : "'+color+'"');
      
    },
    error: function(err) {
      // $('#loading').fadeOut(200);
      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 메인 주제 선택 시 하위 주제를 가져와 리스트에 담는다.
function selected_subject(obj){
  var selected_subject = obj.outerText; // label 요소 텍스트 값 가져오기
  var selected_subject = obj.getAttribute('code');
  // console.log(selected_subject);

  // window.location.href = '/setting/getsubsubject'+'?subject='+selected_subject;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/getsubsubject',   //정보 처리를 할 서버 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      selected_subject : selected_subject
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var sub_subject = data.extra;
      console.log(sub_subject);

      //메인 주제 이름이 담긴 배열을 꺼낸다. (마지막 배열 제거 및 리턴)
      var main_subject_name_array = sub_subject.pop();
      var main_subject_name = main_subject_name_array.main_subject_name;
      // console.log('배열값:'+main_subject_name);
      if(sub_subject.length == 0) {
        alert('하위 주제가 없습니다.');
        $("#sub_subject_list").empty();
        // $("#sub_subject_list").append('<span class="text" style="color:gray;">메인 주제를 선택해주세요.</span>');
      } else {
        $("#sub_subject_list").empty();
        for(i=0; i < sub_subject.length; i++) {
          var sub_subject_name = sub_subject[i].sub_subject_name;
          var sub_subject_code = sub_subject[i].sub_subject_code;
          var color = sub_subject[i].color;
          var is_activate = sub_subject[i].is_activate;
         
          if(is_activate == 'Y') { // 활성화 여부를 구분하여 요소를 생성해준다.
            $("#sub_subject_list").append(
              '<li class="list-group-item border-bottom subject">'+
                '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'+
                  '<div class="flex-grow-1">'+
                    '<label class="form-check-label w-100" for="" onclick="" code="'+ sub_subject_code +'" >'+ '<input class="form-control subject_input" id="subject_value_'+sub_subject_code+'" type="text" value="'+sub_subject_name+'"></label>'+
                    '<input type="hidden" id="old_subject_value_'+sub_subject_code+'" value="'+sub_subject_name+'">'+
                 '</div>'+
                 '<input type="color" value="'+color+'" id="subject_color_'+sub_subject_code+'" style="width:25px; height:25px; border:none; margin-right: 8px; padding:0;">'+
                 '<button type="button" class="btn btn-sm btn-outline-primary" onclick="update_subject(this)" code="'+ sub_subject_code +'" style="margin-right:40px;">'+
                   '<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">'+
                   '<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                   '</svg></span>'+
                 '</button>'+
                 '<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="'+ sub_subject_code +'" checked onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>'+
               '</div>'+
            '</li>'                 
            );
          } else {
            $("#sub_subject_list").append(
              '<li class="list-group-item border-bottom subject">'+
                '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'+
                 '<div class="flex-grow-1">'+
                    '<label class="form-check-label w-100" for="" onclick="" code="'+ sub_subject_code +'" >'+ '<input class="form-control subject_input" id="subject_value_'+sub_subject_code+'" type="text" value="'+sub_subject_name+'"></label>'+
                    '<input type="hidden" id="old_subject_value_'+sub_subject_code+'" value="'+sub_subject_name+'">'+
                 '</div>'+
                 '<input type="color" value="'+color+'" id="subject_color_'+sub_subject_code+'" style="width:25px; height:25px; border:none; margin-right: 8px; padding:0;">'+
                 '<button type="button" class="btn btn-sm btn-outline-primary" onclick="update_subject(this)" code="'+ sub_subject_code +'" style="margin-right:40px;">'+
                   '<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">'+
                   '<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                   '</svg></span>'+
                 '</button>'+
                 '<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="'+ sub_subject_code +'" onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>'+
               '</div>'+
            '</li>'
            );
          }
        }
      }

      // 선택한 메인 주제 코드를 넣어준다.
      $('#main_subject_code').val(selected_subject);

      // 선택한 메인 주제 이름을 넣어준다.
      $('#main_subject_name').val(main_subject_name);

      // 선택한 메인 주제의 배경색 처리를 해준다.
      $('#main_subject_list').find('li').each(function() {
        var li_name = $(this).attr('name');
        // console.log(li_name+','+main_subject_name);
        if(li_name == main_subject_name) {
          $(this).css('backgroundColor','#f8f9fa');
        } else {
          $(this).css('backgroundColor','');
        }

      });

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 하위 주제를 추가한다.
function sub_subject_add(){
  var main_subject_code = $('#main_subject_code').val();
  var sub_subject = $('#sub_subject').val();
  var color = get_random_color();

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/subsubjectadd',   //정보 처리를 할 서버 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      main_subject_code : main_subject_code,
      sub_subject : sub_subject,
      color : color
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var sub_subject = data.extra;

      var sub_subject_name = sub_subject.sub_subject_name;
      var sub_subject_code = sub_subject.sub_subject_code;
      var color = sub_subject.color;
      var is_activate = sub_subject.is_activate;
      
      if(is_activate == 'Y') { // 활성화 여부를 구분하여 요소를 생성해준다.
        $("#sub_subject_list").append(
          '<li class="list-group-item border-bottom subject">'+
            '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'+
              '<div class="flex-grow-1">'+
                '<label class="form-check-label w-100" for="" onclick="" code="'+ sub_subject_code +'" >'+ '<input class="form-control subject_input" id="subject_value_'+sub_subject_code+'" type="text" value="'+sub_subject_name+'"></label>'+
                '<input type="hidden" id="old_subject_value_'+sub_subject_code+'" value="'+sub_subject_name+'">'+
              '</div>'+
              '<input type="color" value="'+color+'" id="subject_color_'+sub_subject_code+'" style="width:25px; height:25px; border:none; margin-right: 8px; padding:0;">'+
              '<button type="button" class="btn btn-sm btn-outline-primary" onclick="update_subject(this)" code="'+ sub_subject_code +'" style="margin-right:40px;">'+
                '<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">'+
                '<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                '</svg></span>'+
              '</button>'+
              '<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="'+ sub_subject_code +'" checked onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>'+
            '</div>'+
        '</li>'                 
        );
      } else {
        $("#sub_subject_list").append(
          '<li class="list-group-item border-bottom subject">'+
            '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'+
              '<div class="flex-grow-1">'+
                '<label class="form-check-label w-100" for="" onclick="" code="'+ sub_subject_code +'" >'+ '<input class="form-control subject_input" id="subject_value_'+sub_subject_code+'" type="text" value="'+sub_subject_name+'"></label>'+
                '<input type="hidden" id="old_subject_value_'+sub_subject_code+'" value="'+sub_subject_name+'">'+
              '</div>'+
              '<input type="color" value="'+color+'" id="subject_color_'+sub_subject_code+'" style="width:25px; height:25px; border:none; margin-right: 8px; padding:0;">'+
              '<button type="button" class="btn btn-sm btn-outline-primary" onclick="update_subject(this)" code="'+ sub_subject_code +'" style="margin-right:40px;">'+
                '<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">'+
                '<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>'+
                '</svg></span>'+
              '</button>'+
              '<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="'+ sub_subject_code +'" onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>'+
            '</div>'+
        '</li>'
        );
      }

      alert('하위 주제가 추가 되었습니다.\n\n'+'- 새로운 주제 : "'+sub_subject_name+'\n\n"'+'- color : "'+color+'"');
      
    },
    error: function(err) {
      // $('#loading').fadeOut(200);
      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}
// 주제 정보를 업데이트 한다.
function update_subject(obj){
  var code = $(obj).attr('code');  // 변경할 주제 코드
  var subject_type = code.split('_')[0]; // 주제 종류
  var old_subject_name = $("#old_subject_value_"+code+"").val(); // 기존 주제 이름
  var new_subject_name = $("#subject_value_"+code+"").val(); // 변경될 주제 이름
  var new_subject_color = $("#subject_color_"+code+"").val(); // 변경될 주제 색상
  var main_subject_code = $('#main_subject_code').val(); // 메인 주제 코드
  var main_subject_name = $('#main_subject_name').val(); // 메인 주제 이름
  console.log('주제 종류 : '+subject_type+'\n'+'새로운 주제 이름 : '+new_subject_name);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/updatesubject',   //정보 처리를 할 서버 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      code : code,
      subject_type : subject_type,
      new_subject_name : new_subject_name,
      new_subject_color : new_subject_color,
      main_subject_code : main_subject_code
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var new_subject = data.extra;
      // console.log(new_subject.idx);

      if(subject_type == 'main'){
        alert(new_subject.idx+'번 메인 주제가 업데이트 되었습니다. \n'+
              old_subject_name +' => '+new_subject.main_subject_name+'\n'+
              'color => '+new_subject.color
              );
      } else {
        var sub_subject_order = Number(new_subject.sub_subject_order) + 1;
        alert('메인 주제 "'+main_subject_name+'"의 ' + sub_subject_order+'번 하위 주제가 업데이트 되었습니다. \n'+
              old_subject_name +' => '+new_subject.sub_subject_name+'\n'+
              'color => '+new_subject.color
              );
      }
    },
    error: function(err) {
      // $('#loading').fadeOut(200);
      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 계정 상태를 변경한다.
function id_status_change(obj) {
  var status = $(obj).is(':checked') ? 'Y' : 'N'; // 체크 여부 확인
  if (status == 'Y') {
    status = 'O'; // 정상
  } else {
    status = 'B'; // 중지
  }
  var id = $(obj).attr('id');
 
  // var host = location.host ; 
  // console.log(is_activate +','+ subject_code + ',' + host);
  // 실시간 처리를 위한 uri redirect (필요한 변수를 get방식으로 보낸다.)
  // window.location.href = '/setting/subjectcheck'+'?maincode='+main_subject_code+'&subject='+subject_code+'&activate='+is_activate;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/accountmanage/statusupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      id : id,
      status : status

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var admin_status = result_array.admin_status;
      console.log(id+' : '+admin_status);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });

}

// 프로필 이미지 변경 요소를 토글한다.
function profile_change(obj) {
  var admin_id = obj.getAttribute('id');
  $('#'+admin_id+'_profile_change').slideToggle('fast');
  
}

// 계정 정보를 업데이트 한다.
function account_update(obj){
  
  var admin_id = obj.getAttribute('admin_id');
  var nickname = $('div[admin_id='+admin_id+']').find('input').eq(0).val();
  var level = $('div[admin_id='+admin_id+']').find('input').eq(1).val();
  // console.log('['+admin_id+'] nickname : '+nickname+', level : '+level);
 
  if( confirm('['+admin_id+']님의 정보를 업데이트 하시겠습니까?') == false){
    return;
  }

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/accountmanage/accountupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      admin_id : admin_id,
      nickname : nickname,
      level : level

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var new_nickname = result_array.nickname;
      var new_level = result_array.level;
      console.log(new_nickname+' : '+new_level);

      alert('['+admin_id+']님의 계정 정보가 업데이트 되었습니다 \r'
            +' - nickname => '+new_nickname+' \r'
            +' - level    => '+new_level
      );

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// nickname 중복 여부를 확인한다.
function is_nickname() {
  var nickname = $('#nickname').val();
  console.log(nickname);
  if (nickname == '') {
    alert('닉네임을 입력해주세요');
    return;
  } else {
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/accountmanage/nicknamecheck',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        nickname : nickname
  
      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);
  
        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var nickname_count = result_array.nickname_count;
        
        console.log(nickname+' : '+nickname_count+'개');

        if (nickname_count > 0) {
          alert("해당 닉네임을 사용할 수 없습니다.");
          $('#nickname').empty();
        } else {
          alert("해당 닉네임은 사용 가능합니다.");
          $('#nickname_check').val('Y');
        }
  
  
      },
      error: function(err) {
        // $('#loading').fadeOut(200);
  
        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
  }
}

// 포스트 목록 설정 변경 사항을 적용한다.
function post_listing_setting() {
  var posts_per_page = $('#posts_per_page').val();
  var page_item_count = $('#page_item_count').val();

  // console.log('페이지 당 포스트 개수 : '+posts_per_page+'\n' // 줄바꿈 처리 '\n'
  //            +'페이지 아이템 표시 개수 : '+page_item_conunt
  // );
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/postlistingsetting',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      posts_per_page : posts_per_page,
      page_item_count : page_item_count

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var posts_per_page = result_array.posts_per_page;
      var page_item_count = result_array.page_item_count;
      
      console.log('페이지 당 포스트 개수 : '+posts_per_page+'\n' // 줄바꿈 처리 '\n'
             +'페이지 아이템 표시 개수 : '+page_item_count
      );

      if (result_array != null) {
        alert("포스트 목록 설정 내용이 적용되었습니다.");
      } else {
        alert("포스트 목록 설정 내용이 적용되지 않았습니다.");
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });

}

// post 상태를 변경한다.
function post_status_change(obj) {
  var status = $(obj).is(':checked') ? 'Y' : 'N'; // 체크 여부 확인

  var slug = $(obj).attr('slug');
 
  // var host = location.host ; 
  // console.log(is_activate +','+ subject_code + ',' + host);
  // 실시간 처리를 위한 uri redirect (필요한 변수를 get방식으로 보낸다.)
  // window.location.href = '/setting/subjectcheck'+'?maincode='+main_subject_code+'&subject='+subject_code+'&activate='+is_activate;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/statusupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      slug : slug,
      status : status

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var post_status = result_array.post_status;
      console.log(slug+' : '+post_status);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });

}

// 색상을 랜덤으로 생성한다.
function get_random_color() {
	return "#" + Math.floor(Math.random() * 16777215).toString(16);
}

// 주제별 색상 정보를 초기화 한다.
function subject_color_init() {
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getsubjectcolor',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      main_subject_colors = result_array.main_subject_colors;
      sub_subject_colors = result_array.sub_subject_colors;

      console.log(main_subject_colors);
      console.log(sub_subject_colors);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 메인 주제별 포스트 현황 데이터를 가져와 파이 차트를 그린다.
function get_main_subject_post_data(callback) {
  
  var id = 'post_statics_pie'; // 차트를 그릴 요소의 id를 생성해준다.
  var subject_title = '전체';
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array(); // 각 주제별 색상을 담을 배열을 초기화 한다.
  // 각 주제의 이름을 가져온다.
  $('#main_subject').find('.main_subject_name').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제의 색상 속성의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  });

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#main_subject').find('.main_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  });

  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr);
  console.log(post_statics_data);
  console.log(subject_colors);

  if(callback !== undefined) {
    post_statics_pie_draw(id,subject_title);
  }
}

// 설정된 기간의(일별) 메인 주제별 포스트 현황 데이터를 가져와 파이 차트를 그린다.
function get_main_subject_post_data_daily_period(callback) {
  
  var id = 'post_statics_pie_daily_period'; // 차트를 그릴 요소의 id를 생성해준다.
  var subject_title = '전체';
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#daily_statics_content').find('.main_subject_name_daily_period').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제 색상 속성의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  });

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#daily_statics_content').find('.main_subject_post_daily_period').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  });
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr);
  console.log(post_statics_data);
  console.log(subject_colors);

  if(callback !== undefined) {
    post_statics_pie_draw(id,subject_title);
  }
}

// 설정된 기간의(월별) 메인 주제별 포스트 현황 데이터를 가져와 파이 차트를 그린다.
function get_main_subject_post_data_monthly_period(callback) {
  
  var id = 'post_statics_pie_monthly_period'; // 차트를 그릴 요소의 id를 생성해준다.
  var subject_title = '전체';
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#monthly_statics_content').find('.main_subject_name_monthly_period').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제 색상 속성의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  });

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.main_subject_post_monthly_period').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  });
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr);
  console.log(post_statics_data);
  console.log(subject_colors);

  if(callback !== undefined) {
    post_statics_pie_draw(id,subject_title);
  }
}

// 하위 주제별 포스트 현황 데이터를 가져와 파이 차트를 그린다.
function get_sub_subject_post_data(subject_code,subject_title,callback) {

  var id = subject_code+'_post_statics_pie';  // 차트를 그릴 요소의 id를 생성해준다.
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#main_subject').find('.'+subject_code+'_sub_subject_name').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제 색상 속성의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#main_subject').find('.'+subject_code+'_sub_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr);
  console.log(post_statics_data);
  console.log(subject_colors);

  if(callback !== undefined) {
    post_statics_pie_draw(id,subject_title);
  }
}

// 선택한 기간(일별)의 하위 주제별 포스트 현황 데이터를 가져와 파이 차트를 그린다.
function get_sub_subject_post_data_daily_period(subject_code,subject_name,callback) {

  var id = subject_code+'_post_statics_pie';  // 차트를 그릴 요소의 id를 생성해준다.
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#daily_statics_content').find('.'+subject_code+'_sub_subject_name').each(function(index,item){
    var sub_subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제의 색상 요소의 값을 가져온다.
    subject_name_arr.push(sub_subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#daily_statics_content').find('.'+subject_code+'_sub_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr);
  console.log(post_statics_data);
  console.log(subject_colors);

  if(callback !== undefined) {
    post_statics_pie_draw(id,subject_name);
  }
}

// 선택한 기간(월별)의 하위 주제별 포스트 현황 데이터를 가져와 파이 차트를 그린다.
function get_sub_subject_post_data_monthly_period(subject_code,subject_name,callback) {

  var id = subject_code+'_post_statics_pie';  // 차트를 그릴 요소의 id를 생성해준다.
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#monthly_statics_content').find('.'+subject_code+'_sub_subject_name').each(function(index,item){
    var sub_subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제 색상 속성의 값을 가져온다.
    subject_name_arr.push(sub_subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  });

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.'+subject_code+'_sub_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  });
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr);
  console.log(post_statics_data);
  console.log(subject_colors);

  if(callback !== undefined) {
    post_statics_pie_draw(id,subject_name);
  }
}

// 포스트 등록 현황 파이차트를 그린다.
function post_statics_pie_draw(id,subject_name) {
  $("#"+id+"").empty(); // 차트를 그릴 요소를 비워준다.
  if(post_statics_data.length == 0){
    // alert("파이 차트 데이터가 없습니다.");
    return;
  }


  // console.log(main_subject_name_arr); // 각 주제 이름 데이터 배열
  // console.log(post_statics_data); // 주제별 포스트 등록 현황 데이터 배열
  // var member_type = [data_name];
  var options = {
    // title: {  // 차트 제목 속성
    //   text: '['+subject_title+'] '+'포스트 등록 비율',
    //   style: {
    //     fontSize:  '14px',
    //     fontWeight:  'bold',
    //     fontFamily:  undefined,
    //     color:  '#263238'
    //   },
    //   offsetX: 10
      // offsetY: 10
    // },
    series: post_statics_data, // 차트 표시 값
    dataLabels: { // 계열 이름이 포함된 라벨을 표시한다.
      formatter(val, opts) {
        const name = opts.w.globals.labels[opts.seriesIndex]
        return [name, val.toFixed(1) + '%']
      }
    },
    // colors: ['#0B2161', '#0B615E','#ED7D31','#4472C4'],
    colors: subject_colors,
    chart: {
      type: 'pie',
      height: '290'
      
    },
    
    plotOptions: {
      pie:{
        // donut 형태일때 안쪽에 값 보이도록 하는 속성
        // donut:{
        //     labels:{
        //         show:true,
        //         name:{
        //             show:true,
        //             fontSize: '13px',
        //             offsertX: 10,
        //             formatter: function (val) {
        //                 return val
        //             }
        //         }
        //     }
        // }
        customScale: 1,
        dataLabels: {
          offset: -30, //pie안의 labels의 위치 (pie의 외곽선을 기준으로 '+'는 원의 바깥 방향, '-'는 원의 중심 방향)
          minAngleToShowLabel: 10,

        }
      }
    },
    labels: subject_name_arr, // 차트 계열 이름
    
    responsive: [{
      breakpoint: 480,
      options: {
        chart: {
          width: '100%'
        },
        legend: {
          position: 'right'
        }
      }
    }]
  };

  var chart = new ApexCharts(document.querySelector("#"+id+""), options);
  chart.render();
}

// 메인 주제별 포스트 및 댓글 현황 데이터를 가져와 막대 차트를 그린다.
function get_main_subject_post_reply_data(callback) {
  
  var id = 'post_statics_bar';
  var subject_title = '전체';
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  comment_statics_data = Array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다.
  comment_reply_statics_data = Array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#main_subject').find('.main_subject_name').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제 요소 색상 속성의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#main_subject').find('.main_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글 등록 현황을 가져온다.
  $('#main_subject').find('.main_subject_comment').each(function(index,item){
    var comment_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_statics_data.push(Number(comment_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글에 대한 댓글 등록 현황을 가져온다.
  $('#main_subject').find('.main_subject_comment_reply').each(function(index,item){
    var comment_reply_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_reply_statics_data.push(Number(comment_reply_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr+': 1');
  console.log(post_statics_data+': 2');
  console.log(comment_statics_data+': 3');
  console.log(comment_reply_statics_data+': 4');
  console.log(subject_colors+': 5');

  if(callback !== undefined) {
    post_statics_bar_draw(id,subject_title);
  }
}

// 설정된 기간(일별)의 메인 주제별 포스트 및 댓글 현황 데이터를 가져와 막대 차트를 그린다.
function get_main_subject_post_reply_data_bar_daily_period(callback) {
  
  var id = 'post_statics_bar_daily_period';
  var subject_title = '선택 기간';
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  comment_statics_data = Array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다.
  comment_reply_statics_data = Array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#daily_statics_content').find('.main_subject_name_daily_period').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제 색상 속성의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#daily_statics_content').find('.main_subject_post_daily_period').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글 등록 현황을 가져온다.
  $('#daily_statics_content').find('.main_subject_comment_daily_period').each(function(index,item){
    var comment_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_statics_data.push(Number(comment_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글에 대한 댓글 등록 현황을 가져온다.
  $('#daily_statics_content').find('.main_subject_comment_reply_daily_period').each(function(index,item){
    var comment_reply_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_reply_statics_data.push(Number(comment_reply_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr+': 1');
  console.log(post_statics_data+': 2');
  console.log(comment_statics_data+': 3');
  console.log(comment_reply_statics_data+': 4');
  console.log(subject_colors+': 5');

  if(callback !== undefined) {
    post_statics_bar_draw(id,subject_title);
  }
}

// 설정된 기간(월별)의 메인 주제별 포스트 및 댓글 현황 데이터를 가져와 막대 차트를 그린다.
function get_main_subject_post_reply_data_bar_monthly_period(callback) {
  
  var id = 'post_statics_bar_monthly_period';
  var subject_title = '선택 기간';
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  comment_statics_data = Array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다.
  comment_reply_statics_data = Array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다.
  subject_colors = Array();
  // 각 주제의 이름을 가져온다.
  $('#monthly_statics_content').find('.main_subject_name_monthly_period').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    var subject_color = $(item).attr('color'); // 각 주제 색상 속성의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
    subject_colors.push(subject_color);
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.main_subject_post_monthly_period').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.main_subject_comment_monthly_period').each(function(index,item){
    var comment_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_statics_data.push(Number(comment_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글에 대한 댓글 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.main_subject_comment_reply_monthly_period').each(function(index,item){
    var comment_reply_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_reply_statics_data.push(Number(comment_reply_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr+': 1');
  console.log(post_statics_data+': 2');
  console.log(comment_statics_data+': 3');
  console.log(comment_reply_statics_data+': 4');
  console.log(subject_colors+': 4');

  if(callback !== undefined) {
    post_statics_bar_draw(id,subject_title);
  }
}

// 설정된 기간(일별)의 메인 주제별 포스트 및 댓글 현황 데이터를 가져와 라인 차트를 그린다.
// function get_main_subject_post_reply_data_line_daily_period(mark,callback,callback_2,callback_3) {
function get_main_subject_post_reply_data_line_daily_period(mark,callback) {
  
  var id = '';
  var start_day = $('#start_day').val(); // 시작일을 가져온다.
  var end_day = $('#end_day').val();  // 종료일을 가져온다.

  var subject_title = '선택 기간';

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getdailystatics',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      start_day : start_day,
      end_day : end_day

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var statics_data = data.extra;
      // var each_day = statics_data.each_day; // 설정된 기간내 각 날짜를 담을 배열을 가져온다. category
      // var series_name_arr = statics_data.series_name_arr; // seriesName 배열을 가져온다. seriese-name
      // var post_statics_data = statics_data.post_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_statics_data = statics_data.comment_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_reply_statics_data = statics_data.comment_reply_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var each_day_total_post = statics_data.each_day_total_post; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment = statics_data.each_day_total_comment; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment_reply = statics_data.each_day_total_comment_reply; // 각 날짜별 통계 합계 배열을 가져온다. series-data

      // console.log(each_day+': 1');
      // console.log(series_name_arr+': 2');
      // console.log(post_statics_data+': 3');
      // console.log(comment_statics_data+': 4');
      // console.log(comment_reply_statics_data+': 5');
      // console.log(each_day_total_post+': 6');
      // console.log(each_day_total_comment+': 7');
      // console.log(each_day_total_comment_reply+': 8');

      if(callback !== undefined) {
        id = 'post_statics_line_daily_period';
        var data_type = 'post';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = 'comment_statics_line_daily_period';
        var data_type = 'comment';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = 'comment_reply_statics_line_daily_period';
        var data_type = 'comment_reply';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  
}

// 설정된 기간(월별)의 메인 주제별 포스트 및 댓글 현황 데이터를 가져와 라인 차트를 그린다.
function get_main_subject_post_reply_data_line_monthly_period(mark,start_day,end_day,callback) {
  
  var id = '';

  var subject_title = '선택 기간';

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getmonthlystatics',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      start_day : start_day,
      end_day : end_day

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var statics_data = data.extra;
      console.log(statics_data);
      // var each_day = statics_data.each_day; // 설정된 기간내 각 날짜를 담을 배열을 가져온다. category
      // var series_name_arr = statics_data.series_name_arr; // seriesName 배열을 가져온다. seriese-name
      // var post_statics_data = statics_data.post_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_statics_data = statics_data.comment_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_reply_statics_data = statics_data.comment_reply_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var each_day_total_post = statics_data.each_day_total_post; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment = statics_data.each_day_total_comment; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment_reply = statics_data.each_day_total_comment_reply; // 각 날짜별 통계 합계 배열을 가져온다. series-data

      // console.log(each_day+': 1');
      // console.log(series_name_arr+': 2');
      // console.log(post_statics_data+': 3');
      // console.log(comment_statics_data+': 4');
      // console.log(comment_reply_statics_data+': 5');
      // console.log(each_day_total_post+': 6');
      // console.log(each_day_total_comment+': 7');
      // console.log(each_day_total_comment_reply+': 8');

      if(callback !== undefined) {
        id = 'post_statics_line_monthly_period';
        var data_type = 'post';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = 'comment_statics_line_monthly_period';
        var data_type = 'comment';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = 'comment_reply_statics_line_monthly_period';
        var data_type = 'comment_reply';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }
      
    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
}

// 설정된 기간(일별)의 메인 주제의 하위 주제별 포스트 및 댓글 현황 데이터를 가져와 라인 차트를 그린다.
function get_sub_subject_post_reply_data_line_daily_period(mark,main_subject_statics_id,subject_name,subject_code,callback) {
  
  var id = '';
  var start_day = $('#start_day').val(); // 시작일을 가져온다.
  var end_day = $('#end_day').val();  // 종료일을 가져온다.

  console.log('sub_daily_period : '+start_day+' ~ '+end_day);
  var subject_title = '선택 기간';

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getdailystaticsbymainsubject',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      
      subject_name : subject_name,
      subject_code : subject_code,
      start_day : start_day,
      end_day : end_day

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var statics_data = data.extra;
      console.log(statics_data);
      // var each_day = statics_data.each_day; // 설정된 기간내 각 날짜를 담을 배열을 가져온다. category
      // var series_name_arr = statics_data.series_name_arr; // seriesName 배열을 가져온다. seriese-name
      // var post_statics_data = statics_data.post_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_statics_data = statics_data.comment_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_reply_statics_data = statics_data.comment_reply_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var each_day_total_post = statics_data.each_day_total_post; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment = statics_data.each_day_total_comment; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment_reply = statics_data.each_day_total_comment_reply; // 각 날짜별 통계 합계 배열을 가져온다. series-data

      // console.log(each_day+': 1');
      // console.log(series_name_arr+': 2');
      // console.log(post_statics_data+': 3');
      // console.log(comment_statics_data+': 4');
      // console.log(comment_reply_statics_data+': 5');
      // console.log(each_day_total_post+': 6');
      // console.log(each_day_total_comment+': 7');
      // console.log(each_day_total_comment_reply+': 8');

      if(callback !== undefined) {
        id = main_subject_statics_id+'_post_statics_line';
        var data_type = 'post';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = main_subject_statics_id+'_comment_statics_line';
        var data_type = 'comment';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = main_subject_statics_id+'_comment_reply_statics_line';
        var data_type = 'comment_reply';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }
    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
    
}

// 설정된 기간(월별)의 메인 주제의 하위 주제별 포스트 및 댓글 현황 데이터를 가져와 라인 차트를 그린다.
function get_sub_subject_post_reply_data_line_monthly_period(mark,main_subject_statics_id,subject_name,subject_code,callback,callback_2,callback_3) {
  
  var id = '';
  var start_month = $('#start_month').val();
  var start_day = start_month+'-01';
  var end_month = $('#end_month').val();
  var end_day_array = end_month.split('-');
  var last  = new Date(end_day_array[0], end_day_array[1],0).getDate();
  // last = new Date( last - 1 );
  var end_day = end_month+'-'+last;
  console.log('end_day : '+end_day);
  
  console.log('sub_monthly_period : '+start_day+' ~ '+end_day);

  var subject_title = '선택 기간';

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getmonthlystaticsbymainsubject',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      
      subject_name : subject_name,
      subject_code : subject_code,
      start_day : start_day,
      end_day : end_day

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var statics_data = data.extra;
      console.log(statics_data);
      // var each_day = statics_data.each_day; // 설정된 기간내 각 날짜를 담을 배열을 가져온다. category
      // var series_name_arr = statics_data.series_name_arr; // seriesName 배열을 가져온다. seriese-name
      // var post_statics_data = statics_data.post_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_statics_data = statics_data.comment_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var comment_reply_statics_data = statics_data.comment_reply_statics_data; // 각 주제별 포스트 등록 현황 데이터 배열을 가져온다. series-data
      // var each_day_total_post = statics_data.each_day_total_post; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment = statics_data.each_day_total_comment; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      // var each_day_total_comment_reply = statics_data.each_day_total_comment_reply; // 각 날짜별 통계 합계 배열을 가져온다. series-data

      // console.log(each_day+': 1');
      // console.log(series_name_arr+': 2');
      // console.log(post_statics_data+': 3');
      // console.log(comment_statics_data+': 4');
      // console.log(comment_reply_statics_data+': 5');
      // console.log(each_day_total_post+': 6');
      // console.log(each_day_total_comment+': 7');
      // console.log(each_day_total_comment_reply+': 8');

      if(callback !== undefined) {
        id = main_subject_statics_id+'_post_statics_line';
        var data_type = 'post';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = main_subject_statics_id+'_comment_statics_line';
        var data_type = 'comment';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }

      if(callback !== undefined) {
        id = main_subject_statics_id+'_comment_reply_statics_line';
        var data_type = 'comment_reply';
        statics_line_draw(id,mark,data_type,subject_title,statics_data);
      }
    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
    
}

// 하위 주제별 포스트 및 댓글 현황 데이터를 가져와 막대 차트를 그린다.
function get_sub_subject_post_reply_data(subject_code,subject_title,callback) {
  
  var id = subject_code+'_post_statics_bar'; // 차트를 그릴 요소의 id를 생성해준다.
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  comment_statics_data = Array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다.
  comment_reply_statics_data = Array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다.
  // 각 주제의 이름을 가져온다.
  $('#main_subject').find('.'+subject_code+'_sub_subject_name').each(function(index,item){
    var subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    subject_name_arr.push(subject_name);  // 주제 이름 배열에 담는다.
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#main_subject').find('.'+subject_code+'_sub_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글 등록 현황을 가져온다.
  $('#main_subject').find('.'+subject_code+'_sub_subject_comment').each(function(index,item){
    var comment_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_statics_data.push(Number(comment_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글에 대한 댓글 등록 현황을 가져온다.
  $('#main_subject').find('.'+subject_code+'_sub_subject_comment_reply').each(function(index,item){
    var comment_reply_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_reply_statics_data.push(Number(comment_reply_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr+': 1');
  console.log(post_statics_data+': 2');
  console.log(comment_statics_data+': 3');
  console.log(comment_reply_statics_data+': 4');

  if(callback !== undefined) {
    post_statics_bar_draw(id,subject_title);
  }
}

// 설정된 기간(일별)의 하위 주제별 포스트 및 댓글 현황 데이터를 가져와 막대 차트를 그린다.
function get_sub_subject_post_reply_data_daily_period(subject_code,subject_name,callback) {
  
  var id = subject_code+'_post_statics_bar'; // 차트를 그릴 요소의 id를 생성해준다.
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  comment_statics_data = Array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다.
  comment_reply_statics_data = Array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다.
  // 각 주제의 이름을 가져온다.
  $('#daily_statics_content').find('.'+subject_code+'_sub_subject_name').each(function(index,item){
    var sub_subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    subject_name_arr.push(sub_subject_name);  // 주제 이름 배열에 담는다.
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#daily_statics_content').find('.'+subject_code+'_sub_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글 등록 현황을 가져온다.
  $('#daily_statics_content').find('.'+subject_code+'_sub_subject_comment').each(function(index,item){
    var comment_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_statics_data.push(Number(comment_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글에 대한 댓글 등록 현황을 가져온다.
  $('#daily_statics_content').find('.'+subject_code+'_sub_subject_comment_reply').each(function(index,item){
    var comment_reply_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_reply_statics_data.push(Number(comment_reply_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr+': 1');
  console.log(post_statics_data+': 2');
  console.log(comment_statics_data+': 3');
  console.log(comment_reply_statics_data+': 4');

  if(callback !== undefined) {
    post_statics_bar_draw(id,subject_name);
  }
}

// 설정된 기간(월별)의 하위 주제별 포스트 및 댓글 현황 데이터를 가져와 막대 차트를 그린다.
function get_sub_subject_post_reply_data_monthly_period(subject_code,subject_name,callback) {
  
  var id = subject_code+'_post_statics_bar'; // 차트를 그릴 요소의 id를 생성해준다.
  subject_name_arr = Array(); // 각 주제 이름을 담을 배열을 초기화 한다.
  post_statics_data = Array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다.
  comment_statics_data = Array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다.
  comment_reply_statics_data = Array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다.
  // 각 주제의 이름을 가져온다.
  $('#monthly_statics_content').find('.'+subject_code+'_sub_subject_name').each(function(index,item){
    var sub_subject_name = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    subject_name_arr.push(sub_subject_name);  // 주제 이름 배열에 담는다.
  })

  // 각 주제별 포스트 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.'+subject_code+'_sub_subject_post').each(function(index,item){
    var post_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    post_statics_data.push(Number(post_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.'+subject_code+'_sub_subject_comment').each(function(index,item){
    var comment_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_statics_data.push(Number(comment_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })

  // 각 주제별 댓글에 대한 댓글 등록 현황을 가져온다.
  $('#monthly_statics_content').find('.'+subject_code+'_sub_subject_comment_reply').each(function(index,item){
    var comment_reply_statics = $(item).find('span').text(); // 각 요소의 텍스트 요소의 값을 가져온다.
    comment_reply_statics_data.push(Number(comment_reply_statics));  // 포스트 등록 현황 데이터 배열에 담는다.
  })
  
  // post_statics_data = [main_01_post, main_02_post, main_03_post, main_04_post]; // 포스트 등록 현황 데이터 배열
  // post_statics_data = {main_01_post, main_02_post, main_03_post, main_04_post}; //대괄호로 묶으면 객체로 반환이 된다.
  
  console.log(subject_name_arr+': 1');
  console.log(post_statics_data+': 2');
  console.log(comment_statics_data+': 3');
  console.log(comment_reply_statics_data+': 4');

  if(callback !== undefined) {
    post_statics_bar_draw(id,subject_name);
  }
}

// 포스트 등록현황 막대차트를 그린다.
function post_statics_bar_draw(id,subject_name){
  if(post_statics_data.length == 0){
      // alert("막대 차트 데이터가 없습니다.");
      return;
  }
       
  var options = {
    title: {  // 차트 제목 속성
      text: '['+subject_name+'] '+'댓글 등록 현황',
      style: {
        fontSize:  '14px',
        fontWeight:  'bold',
        fontFamily:  undefined,
        color:  '#263238'
      },
      offsetX: 10
      // offsetY: 10
    },
    series: [{
    name: '포스트',
    data: post_statics_data
  }, {
    name: '댓글',
    data: comment_statics_data
  },{
    name: '대댓글',
    data: comment_reply_statics_data
  }],
    chart: {
    type: 'bar',
    height: '290',

  },
  plotOptions: {
    bar: {
      horizontal: false,
      columnWidth: '55%',
      endingShape: 'rounded'
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  xaxis: {
    categories: subject_name_arr,
  },
  yaxis: {
    title: {
      text: '등록수(개)',
      style: {
        fontSize:  '14px',
        fontWeight:  'bold',
        fontFamily:  undefined,
        color:  '#263238'
      }
    }
  },
  fill: {
    opacity: 1
  },
  tooltip: {
    y: {
      formatter: function (val) {
        return val + " 개"
      }
    }
  }
  };

  $("#"+id+"").empty(); // 막대 차트를 그릴 요소를 비워준다.

  var chart = new ApexCharts(document.querySelector("#"+id+""), options);

  chart.render();
}

// 통계 데이터 라인차트를 그린다.
function statics_line_draw(id,mark,data_type,subject_title,statics_data){
  if(statics_data.length == 0){
      alert("라인 차트 데이터가 없습니다.");
      return;
  }
  
  if(mark == 'daily'){ // 일별 현황 통계
    var line_category = statics_data.each_day; // 설정된 기간내 각 날짜를 담을 배열을 가져온다. category
    var y_axis_text = '일별 현황 (건)';
    if( data_type == 'post' ) {
      var line_title = "일별 포스트 등록 현황";
      var line_lable = statics_data.each_day_total_post; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      var line_statics_data = statics_data.post_statics_data;
    } else if( data_type == 'comment') {
      var line_title = "일별 댓글 등록 현황";
      var line_lable = statics_data.each_day_total_comment; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      var line_statics_data = statics_data.comment_statics_data;
    } else {
      var line_title = "일별 대댓글 등록 현황";
      var line_lable = statics_data.each_day_total_comment_reply; // 각 날짜별 통계 합계 배열을 가져온다. series-data
      var line_statics_data = statics_data.comment_reply_statics_data;
    }
  } else{ // 월별 현황 통계
    var line_category = statics_data.each_month; // 설정된 기간내 각 월을 담을 배열을 가져온다. category
    var y_axis_text = '월별 현황 (건)';
    if( data_type == 'post' ) {
      var line_title = "월별 포스트 등록 현황";
      var line_lable = statics_data.each_month_total_post; // 각 월별 통계 합계 배열을 가져온다. series-data
      var line_statics_data = statics_data.post_statics_data;
    } else if( data_type == 'comment') {
      var line_title = "월별 댓글 등록 현황";
      var line_lable = statics_data.each_month_total_comment; // 각 월별 통계 합계 배열을 가져온다. series-data
      var line_statics_data = statics_data.comment_statics_data;
    } else {
      var line_title = "월별 대댓글 등록 현황";
      var line_lable = statics_data.each_month_total_comment_reply; // 각 월별 통계 합계 배열을 가져온다. series-data
      var line_statics_data = statics_data.comment_reply_statics_data;
    }
  }
  
  var series_name_arr = statics_data.series_name_arr; // seriesName 배열을 가져온다. seriese-name
  var series_arr = Array(); // 시리즈를 담을 변수를 초기화 한다.
  var yaxis_arr = Array(); // y축 설정을 담을 변수를 초기화 한다.
 
  var post_statics = Array();
  $.each(line_statics_data,function(index,item){
    for(i=0; i < item.length; i++){
      post_statics.push(Number(item[i]));
    }
  });
  
  // 숫자 배열의 최대값을 얻으려면 아래와 같이 사용해야 한다.
  var max_r = Math.max.apply(Math, post_statics);
  console.log('data_type : '+data_type+'\n'+'post_statics : '+post_statics+'\n'+'max : '+ max_r);
  
  for(i=0; i < series_name_arr.length; i++) {
    series_arr.push({
      name: series_name_arr[i],
      data: line_statics_data[i]
    });
    if(i == 0) { // 처음 요소를 기준으로 y축을 설정한다.
      yaxis_arr.push({
        seriesName: series_name_arr[0],
        tickAmount : 5, //몇 칸으로 나눌건 지 기준
        min: 0, // 최솟값
        // max: 10, //최대값
        max: max_r,
        
        title:{
            text: y_axis_text,
            style: {
                fontSize: '14px',
            },
        offsetX: 0
                    },
        labels: {
          show: true,
          offsetX: -5,
        }
      });
    } else {
      yaxis_arr.push({
        seriesName: series_name_arr[0],
        show:false
      });
    }
  }
  console.log(series_arr);
  console.log(yaxis_arr);

  var options = {
    title: {  // 차트 제목 속성
      text: '['+subject_title+'] '+line_title,
      style: {
        fontSize:  '14px',
        fontWeight:  'bold',
        fontFamily:  undefined,
        color:  '#263238'
      },
      offsetX: 10
      // offsetY: 10
    },
    chart: {
        type: 'line',
        height:'290',
        width: '100%',
              },
    // colors: ['#0489B1', '#B40404','#0057ff', '#0404B4'], // 차트 색깔을 지정한다. series 순서대로 적용된다.
    colors : subject_colors,
    series: series_arr
    ,
    xaxis: { // x축 항목 데이터
        // categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
            categories: line_category,
            labels:{
                style: {
                    fontSize: '10px',
                },
            },
    },

    yaxis: yaxis_arr,

    dataLabels: {
      enabled: true,
      formatter: function(val, opts) {
          if(opts.dataPointIndex == (line_lable.length - 1)){
              var prefix = '';
              if(opts.seriesIndex == 0){
                  prefix = series_name_arr[0];
              } else if(opts.seriesIndex == 1){
                  prefix = series_name_arr[1];
              } else if(opts.seriesIndex == 2){
                  prefix = series_name_arr[2];
              } else if(opts.seriesIndex == 3){
                  prefix = series_name_arr[3];
              }
              val = trd_comma_add(val);
              var label = prefix + '\r' + ' ' + val;
              return label;
          }else{
              val = trd_comma_add(val);
              return val;
          }

      },
      textAnchor: 'end'
    },
  }

  $("#"+id+"").empty(); // 라인 차트를 그릴 요소를 비워준다.

  var chart = new ApexCharts(document.querySelector("#"+id+""), options);

  chart.render();
}

// 메인 주제의 하위 주제 통계 요소를 활성화 한다.
function open_sub_subject_statics(obj,callback,callback_2){
  var main_subject_statics_id = $(obj).attr('id'); // 클릭한 메인 주제 요소의 id를 가져온다.
  var subject_title = $(obj).attr('subject_title'); // 클릭한 메인 주제 요소의 주제 타이틀을 가져온다.
  var sub_status = $(obj).attr('sub_status'); // 클릭한 메인 주제의 하위 주제 통계 요소 상태를 가져온다.
  var sub_count = $(obj).attr('sub_count'); // 클릭한 메인 주제의 하위 주제 개수를 가져온다.
  var sub_subject_statics_id = main_subject_statics_id+'_sub';
  $("#"+sub_subject_statics_id+"").slideToggle(); // 하위 주제 통계 요소를 활성화해준다.
  if(sub_count > 0) { // 하위 주제가 있는 경우
    if (sub_status == 'close') { // 하위 주제 통계 요소가 활성화되지 않은 상태인 경우
      $(obj).attr({"sub_status":"open"});
      $("#"+main_subject_statics_id+"").attr({"class":"row border-bottom border-1 py-1 main_subject_statics"}); // 메인 주제 통계 요소의 클래스 속성을 업데이트 해준다.
      if(callback !== undefined) {
        // 하위 주제 포스트 등록 현황 파이차트를 그린다.
        get_sub_subject_post_data(main_subject_statics_id,subject_title,post_statics_pie_draw);
      }
      
      if(callback_2 !== undefined) {
        // 하위 주제 포스트 등록 및 댓글 등록 현황 막대차트를 그린다.
        get_sub_subject_post_reply_data(main_subject_statics_id,subject_title,post_statics_bar_draw);
      }
    } else { // 하위 주제 통계 요소가 활성화되어 있는 상태인 경우
      $(obj).attr({"sub_status":"close"});
      $("#"+main_subject_statics_id+"").attr({"class":"row border-bottom border-3 py-1 main_subject_statics"}); // 메인 주제 통계 요소의 클래스 속성을 업데이트 해준다.
    }

    // 차트 영역의 클래스를 업데이트 해준다.
    $("#"+main_subject_statics_id+"_charts").attr({"class":"row border-bottom border-3"});
  } else {

    $("#"+main_subject_statics_id+"_charts").empty().append('<p class="text-center my-0" style="font-size:0.75rem;"><span>하위 주제가 없습니다.</span></p>');
    $("#"+main_subject_statics_id+"_charts").attr({"class":"row border-bottom border-3"});
  }
}

// 선택한 기간(일별)의 메인 주제의 하위 주제 통계 요소를 활성화 한다.
function open_sub_subject_statics_daily_period(obj,callback,callback_2,callback_3){
  var main_subject_statics_id = $(obj).attr('id'); // 클릭한 메인 주제 요소의 id를 가져온다.
  console.log(main_subject_statics_id);
  var subject_name = $(obj).attr('subject_name'); // 클릭한 메인 주제 요소의 주제 타이틀을 가져온다.
  var subject_code = $(obj).attr('subject_code'); // 클릭한 메인 주제 요소의 주제 타이틀을 가져온다.
  var sub_status = $(obj).attr('sub_status'); // 클릭한 메인 주제의 하위 주제 통계 요소 상태를 가져온다.
  var sub_count = $(obj).attr('sub_count'); // 클릭한 메인 주제의 하위 주제 개수를 가져온다.
  var sub_subject_statics_id = main_subject_statics_id+'_sub';
  var mark = 'daily';
  console.log(sub_subject_statics_id);
  $("#"+sub_subject_statics_id+"").slideToggle(); // 하위 주제 통계 요소를 활성화해준다.
  if(sub_count > 0) { // 하위 주제가 있는 경우
    if (sub_status == 'close') { // 하위 주제 통계 요소가 활성화되지 않은 상태인 경우
      $(obj).attr({"sub_status":"open"});
      $("#"+main_subject_statics_id+"").attr({"class":"row border-bottom border-1 py-1 main_subject_statics"}); // 메인 주제 통계 요소의 클래스 속성을 업데이트 해준다.

      if(callback !== undefined) {
        // 하위 주제 포스트 등록 현황 파이차트를 그린다.
        get_sub_subject_post_data_daily_period(main_subject_statics_id,subject_name,post_statics_pie_draw);
      }
      
      if(callback_2 !== undefined) {
        // 하위 주제 포스트 등록 및 댓글 등록 현황 막대차트를 그린다.
        get_sub_subject_post_reply_data_daily_period(main_subject_statics_id,subject_name,post_statics_bar_draw);
      }
  
      if(callback_3 !== undefined) {
        // 포스트 등록 및 댓글 현황 라인 차트를 그린다.
        get_sub_subject_post_reply_data_line_daily_period(mark,main_subject_statics_id,subject_name,subject_code,statics_line_draw);
      }

    } else { // 하위 주제 통계 요소가 활성화되어 있는 상태인 경우
      $(obj).attr({"sub_status":"close"});
      $("#"+main_subject_statics_id+"").attr({"class":"row border-bottom border-3 py-1 main_subject_statics"}); // 메인 주제 통계 요소의 클래스 속성을 업데이트 해준다.
    }

    
      
    // 차트 영역의 클래스를 업데이트 해준다.
    $("#"+main_subject_statics_id+"_charts").attr({"class":"row border-bottom border-3"});
  } else {

    $("#"+main_subject_statics_id+"_charts").empty().append('<p class="text-center my-0" style="font-size:0.75rem;"><span>하위 주제가 없습니다.</span></p>');
    $("#"+main_subject_statics_id+"_charts").attr({"class":"row border-bottom border-3"});
  }
}

// 선택한 기간(월별)의 메인 주제의 하위 주제 통계 요소를 활성화 한다.
function open_sub_subject_statics_monthly_period(obj,callback,callback_2,callback_3){
  var main_subject_statics_id = $(obj).attr('id'); // 클릭한 메인 주제 요소의 id를 가져온다.
  console.log(main_subject_statics_id);
  var subject_name = $(obj).attr('subject_name'); // 클릭한 메인 주제 요소의 주제 타이틀을 가져온다.
  var subject_code = $(obj).attr('subject_code'); // 클릭한 메인 주제 요소의 주제 타이틀을 가져온다.
  var sub_status = $(obj).attr('sub_status'); // 클릭한 메인 주제의 하위 주제 통계 요소 상태를 가져온다.
  var sub_count = $(obj).attr('sub_count'); // 클릭한 메인 주제의 하위 주제 개수를 가져온다.
  var sub_subject_statics_id = main_subject_statics_id+'_sub';
  var mark = 'monthly';
  console.log(sub_subject_statics_id);
  $("#"+sub_subject_statics_id+"").slideToggle(); // 하위 주제 통계 요소를 활성화해준다.
  if(sub_count > 0) { // 하위 주제가 있는 경우
    if (sub_status == 'close') { // 하위 주제 통계 요소가 활성화되지 않은 상태인 경우
      $(obj).attr({"sub_status":"open"});
      $("#"+main_subject_statics_id+"").attr({"class":"row border-bottom border-1 py-1 main_subject_statics"}); // 메인 주제 통계 요소의 클래스 속성을 업데이트 해준다.

      if(callback !== undefined) {
        // 하위 주제 포스트 등록 현황 파이차트를 그린다.
        get_sub_subject_post_data_monthly_period(main_subject_statics_id,subject_name,post_statics_pie_draw);
      }
      
      if(callback_2 !== undefined) {
        // 하위 주제 포스트 등록 및 댓글 등록 현황 막대차트를 그린다.
        get_sub_subject_post_reply_data_monthly_period(main_subject_statics_id,subject_name,post_statics_bar_draw);
      }
  
      if(callback_3 !== undefined) {
        // 포스트 등록 및 댓글 현황 라인 차트를 그린다.
        get_sub_subject_post_reply_data_line_monthly_period(mark,main_subject_statics_id,subject_name,subject_code,statics_line_draw);
      }

    } else { // 하위 주제 통계 요소가 활성화되어 있는 상태인 경우
      $(obj).attr({"sub_status":"close"});
      $("#"+main_subject_statics_id+"").attr({"class":"row border-bottom border-3 py-1 main_subject_statics"}); // 메인 주제 통계 요소의 클래스 속성을 업데이트 해준다.
    }

    // 차트 영역의 클래스를 업데이트 해준다.
    $("#"+main_subject_statics_id+"_charts").attr({"class":"row border-bottom border-3"});
  } else {

    $("#"+main_subject_statics_id+"_charts").empty().append('<p class="text-center my-0" style="font-size:0.75rem;"><span>하위 주제가 없습니다.</span></p>');
    $("#"+main_subject_statics_id+"_charts").attr({"class":"row border-bottom border-3"});
  }
}

// 일별 현황(daily_staus) 요소를 갱신한다.
function daily_statics(callback,callback_2) {
  var start_day = $('#start_day').val();
  var end_day = $('#end_day').val();
  $('#start_order_daily_period').val('1'); // 포스트 순위 아이템 시작 순서를 초기화 한다.

  console.log('시작일 : '+start_day+', 종료일 : '+end_day);

  var query_string = '?'+'start_day='+start_day+'&end_day='+end_day; // get방식으로 보낼 시작일, 종료일 쿼리스트링을 만들어 준다.
  var protocol = window.location.protocol;
  var hostname = window.location.hostname;
  var href = protocol+'//'+hostname+'/main/dailystatics'+query_string; // url을 만들어 준다.
  $('#daily_statics_content_wrap').empty();
    // 로딩중 spinner 요소를 넣어준다.
    $('#daily_statics_content_wrap').append('<div class="d-flex justify-content-center align-items-center my-5" id="loading">'+
        '<div class="spinner-border text-secondary" role="status">'+
        '<span class="visually-hidden">Loading...</span>'+
        '</div>'+
      '</div>'
    );
  $('#daily_statics_content_wrap').load(href,null,function(){
    $('#loading').remove(); // 로딩 요소를 제거한다.
    // 포스트 인기 순위 영역에 로딩 요소를 넣어준다.
    $('#post_ranking_list_item_daily_period_wrap').append(
      '<div class="d-flex justify-content-center align-items-center my-5" id="loading_daily">'+
        '<div class="spinner-border text-secondary" role="status">'+
          '<span class="visually-hidden">Loading...</span>'+
        '</div>'+
      '</div>'
    );
    if(callback !== undefined) {
      setTimeout(draw_daily_chart,500); // 일별 현황 차트를 그린다.(dom 요소가 다 그려질 때까지 지연시간을 준다.)
    }
    if(callback_2 !== undefined) {
      post_ranking_list_daily_period(); // 랭킹 리스트를 가져온다.
    }
  }); // 일별 현황 요소를 갱신한다.
}

// 월별 현황(monthly_sticss) 요소를 갱신한다.
function monthly_statics(callback,callback_2) {
  var start_month = $('#start_month').val();
  var start_day = start_month+'-01';
  var end_month = $('#end_month').val();
  var end_day_array = end_month.split('-');
  // 해당월의 마지막 날을 구한다.
  var last  = new Date(end_day_array[0], end_day_array[1],0).getDate(); 
  // last = new Date( last - 1 ); 
  var end_day = end_month+'-'+last;
  console.log('end_day : '+end_day);

  $('#start_order_monthly_period').val('1'); // 포스트 순위 아이템 시작 순서를 초기화 한다.

  console.log('시작일 : '+start_day+', 종료일 : '+end_day);

  var query_string = '?'+'start_day='+start_day+'&end_day='+end_day; // get방식으로 보낼 시작일, 종료일 쿼리스트링을 만들어 준다.
  var protocol = window.location.protocol;
  var hostname = window.location.hostname;
  var href = protocol+'//'+hostname+'/main/monthlystatics'+query_string; // url을 만들어 준다.
  $('#monthly_statics_content_wrap').empty();
    // 로딩중 spinner 요소를 넣어준다.
    $('#monthly_statics_content_wrap').append('<div class="d-flex justify-content-center align-items-center my-5" id="loading">'+
        '<div class="spinner-border text-secondary" role="status">'+
        '<span class="visually-hidden">Loading...</span>'+
        '</div>'+
      '</div>'
    );
  $('#monthly_statics_content_wrap').load(href,null,function(){
    $('#loading').remove(); // 로딩 요소를 제거한다.
    // 포스트 인기 순위 영역에 로딩 요소를 넣어준다.
    $('#post_ranking_list_item_monthly_period_wrap').append(
      '<div class="d-flex justify-content-center align-items-center my-5" id="loading_monthly">'+
        '<div class="spinner-border text-secondary" role="status">'+
          '<span class="visually-hidden">Loading...</span>'+
        '</div>'+
      '</div>'
    );
    if(callback !== undefined) {
      // draw_monthly_chart(start_day,end_day);
      setTimeout(draw_monthly_chart,500,start_day,end_day); // 월별 현황 차트를 그린다.(dom 요소가 다 그려질 때까지 지연시간을 준다.)
    }
    if(callback_2 !== undefined) {
      post_ranking_list_monthly_period(start_day,end_day); // 랭킹 리스트를 가져온다.
    }
  }); // 월별 현황 요소를 갱신한다.
}

// 일별 현황 차트를 그린다.
function draw_daily_chart() {
  console.log('일별 현황 차트를 그립니다.')
  var mark = 'daily';
  
  // 포스트 등록 비율 현황 파이 차트를 그린다.
  get_main_subject_post_data_daily_period(post_statics_pie_draw);

  // 포스트 등록 및 댓글 현황 막대 차트를 그린다.
  get_main_subject_post_reply_data_bar_daily_period(post_statics_bar_draw);

  // 포스트 등록 및 댓글 현황 라인 차트를 그린다.
  // get_main_subject_post_reply_data_line_daily_period(mark,post_statics_line_draw,comment_statics_line_draw,comment_reply_statics_line_draw);
  get_main_subject_post_reply_data_line_daily_period(mark,statics_line_draw);

}

// 월별 현황 차트를 그린다.
function draw_monthly_chart(start_day,end_day) {
  console.log('월별 현황 차트를 그립니다.')
  var mark = 'monthly';

  // 포스트 등록 비율 현황 파이 차트를 그린다.
  get_main_subject_post_data_monthly_period(post_statics_pie_draw);

  // 포스트 등록 및 댓글 현황 막대 차트를 그린다.
  get_main_subject_post_reply_data_bar_monthly_period(post_statics_bar_draw);

  // 포스트 등록 및 댓글 현황 라인 차트를 그린다.
  get_main_subject_post_reply_data_line_monthly_period(mark,start_day,end_day,statics_line_draw);

}

// 줄바꿈 문자를 '<br />'로 바꿔준다.
function nl2br (str) {
  if (typeof str === 'undefined' || str === null) {
      return '';
  }
  return str.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
}

// 포스트 코멘트를 저장한다.
function save_comment(obj) {
  var post_slug = $(obj).attr('slug');
  var main_subject_name = $('#main_subject_name_'+post_slug).text();
  var sub_subject_name = $('#sub_subject_name_'+post_slug).text();
  var comment_nickname = $('#comment_nickname_'+post_slug).val();
  var comment_passwd = $('#comment_passwd_'+post_slug).val();
  var post_comment = $('#post_comment_'+post_slug).val();
  var is_secret = $('#is_secret_'+post_slug).is(':checked') ? 'Y' : 'N';

  console.log(post_slug+','+comment_nickname+','+comment_passwd+','+is_secret+'\\r'+post_comment);

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/savecomment',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      post_slug : post_slug,
      main_subject_name : main_subject_name,
      sub_subject_name : sub_subject_name,
      comment_nickname : comment_nickname,
      comment_passwd : comment_passwd,
      post_comment : post_comment,
      is_secret : is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment = data.extra;
      // var new_comment_id = 'short_comment_'+comment.idx;
      console.log(comment.post_comment);
      var comment_count = comment.count_all_count; // 코멘트 개수를 가져온다.
      var porotocol = window.location.protocol; // 프로토콜을 가져온다.
      var host = window.location.hostname; //호스트 네임을 가져온다.
      var href = porotocol+'//'+host+'/postmanage/commentlist/'+post_slug; // 댓글 목록을 갱신할 url을 생성한다.
      console.log(href);
      if(comment != null) {
        
        $('#comment_nickname_'+post_slug).val(''); // 별명 요소값을 제거한다.
        $('#comment_passwd_'+post_slug).val('');  // 비밀번호 요소값을 제거한다.
        $('#is_secret').prop('checked',false);
        // if($('#is_secret').is(':checked')){
        //   $('#is_secret').click();
        // }
        ; // 체크박스를 해체 상태로 속성을 변경한다.
        $('#post_comment_'+post_slug).val(''); // 댓글 입력 요소값을 제거한다.
        ///////////////////////////////////////
        // 새로운 댓글 요소만 추가하는 방식
        ///////////////////////////////////////
        $('#comment_count_'+post_slug).text(comment_count); // 코멘트 개수를 업데이트 해준다.
        var status = $('#comment_list_'+post_slug).attr('status');
        if(status == 'close') {
          $('#comment_list_open_'+post_slug).click(); // 지연 시간을 0.5초를 준다.
          input_comment(comment,post_slug);
          document.getElementById("footer").scrollIntoView(false);
        } else {
          // $("#"+new_comment_id+"").click();
          input_comment(comment,post_slug);
          document.getElementById("footer").scrollIntoView(false); // 새로 추가된 댓글요소로 포커싱한다.
        }
        // alert('댓글이 추가 되었습니다.');
        // 토스트 메세지 요소를 추가한다.
        // $('#post_list').append('<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
        //                           '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
        //                             '<div class="d-flex justify-content-center">'+
        //                               '<div class="toast-body">'+
        //                                 '댓글이 추가 되었습니다.'+
        //                               '</div>'+
        //                               '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
        //                             '</div>'+
        //                           '</div>'+
        //                         '</div>'
        //                       );
        toast_init();
        $('#comment_save_btn_'+post_slug+'').blur(); // 댓글 저장 버튼에서 포커스를 아웃 시킨다.
        ////////////////////////////////////////
        // 댓글 목록을 다시 로딩하는 방식
        ////////////////////////////////////////
        // $('#comment_list_'+post_slug).empty();
        //  // 로딩중 spinner 요소를 넣어준다.
        //   $('#comment_list_'+post_slug).append('<div class="d-flex justify-content-center align-items-center my-5" id="loading">'+
        //                           '<div class="spinner-border text-secondary" role="status">'+
        //                           '<span class="visually-hidden">Loading...</span>'+
        //                           '</div>'+
        //                         '</div>'
        //     );
        // $('#comment_list_'+post_slug).load(href,'comment_count='+comment.length,function(){
        //   $('#loading').remove(); // 로딩 요소를 제거한다.
        //   console.log(new_comment_id);
        //   $('#comment_list_open_'+post_slug).click(); // '댓글 보이기' 요소를 클릭한다.
        //   document.getElementById(new_comment_id).scrollIntoView({block:"center"}); // 새로 추가된 댓글요소로 포커싱한다.
        //   // $('#comment_save_btn_'+post_slug).css({"cursor":"none"}); // 저장 버튼 비활성화 처리를 위해 클래스를 다시 초기화 한다.
        // }); // 댓글 목록을 갱신한다.
        // alert('댓글이 추가 되었습니다.');
      } else {
        alert('댓글 추가에 실패하였습니다.');
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 포스트 코멘트에 대한 댓글을 저장한다.
function save_comment_reply(obj) {
  var post_slug = $(obj).attr('slug');  // 포스트 url 식별 번호
  var org_idx = $(obj).attr('org_idx');
  var comment_reply_nickname = $('#comment_reply_nickname_'+org_idx).val();
  var comment_reply_passwd = $('#comment_reply_passwd_'+org_idx).val();
  var post_comment_reply = $('#post_comment_reply_'+org_idx).val();
  var comment_reply_is_secret = $('#comment_reply_is_secret_'+org_idx).is(':checked') ? 'Y' : 'N';

  console.log(post_slug+','+org_idx+','+comment_reply_nickname+','+comment_reply_passwd+','+comment_reply_is_secret+'\n'+post_comment_reply);

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/savecommentreply',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      org_idx : org_idx,
      post_slug : post_slug,
      comment_reply_nickname : comment_reply_nickname,
      comment_reply_passwd : comment_reply_passwd,
      post_comment_reply : post_comment_reply,
      comment_reply_is_secret : comment_reply_is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment_reply = data.extra;
      var comment_reply_id = 'comment_reply_'+comment_reply.idx;
      var comment_reply_count = comment_reply.comment_reply_count; // 대댓글 개수를 가져온다.
      $('#comment_reply_count_'+org_idx).val(comment_reply_count); // 대댓글 개수 표시 요소값을 업데이트 해준다.
      console.log(comment_reply.post_comment_reply);
      var protocol = window.location.protocol; // 프로토콜을 가져온다.
      var host = window.location.hostname; //호스트 네임을 가져온다.
      var href = protocol+'//'+host+'/postmanage/commentlist/'+post_slug; // 댓글 목록을 갱신할 url을 생성한다.
      console.log(href);
      if(comment_reply != null) {
        // alert('댓글에 댓글이 추가 되었습니다.');
        $('#comment_reply_nickname_'+org_idx).val(''); // 별명 요소값을 제거한다.
        $('#comment_reply_passwd_'+org_idx).val('');  // 비밀번호 요소값을 제거한다.
        $('#comment_reply_is_secret').prop('checked',false);
        // if($('#is_secret').is(':checked')){
        //   $('#is_secret').click();
        // }
        ; // 체크박스를 해체 상태로 속성을 변경한다.
        $('#post_comment_reply_'+org_idx).val(''); // 댓글 입력 요소값을 제거한다.
        $('#comment_reply_reg_'+org_idx).hide();
        input_comment_reply(comment_reply,org_idx); // 댓글의 댓글 요소를 추가한다.
        // $('#comment_list_'+post_slug).empty();
        // $('#comment_list_'+post_slug).load(href,'comment_count='+comment_reply.length,function(){
        //   console.log(comment_id);
        //   $('#comment_list_open_'+post_slug).click(); // '댓글 보이기' 요소를 클릭한다.
        document.getElementById(comment_reply_id).scrollIntoView({block:"center"}); // 새로 추가된 댓글요소로 포커싱한다.         
        // }); // 댓글 목록을 갱신한다.
        // $('#toast_message').remove(); // 기존의 토스트 메세지 요소를 제거한다.
        // // 토스트 메세지 요소를 추가한다.
        // $('#post_list').append('<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
        //                           '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
        //                             '<div class="d-flex justify-content-center">'+
        //                               '<div class="toast-body">'+
        //                                 '댓글에 대한 댓글이 추가 되었습니다.'+
        //                               '</div>'+
        //                               '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
        //                             '</div>'+
        //                           '</div>'+
        //                         '</div>'
        //                       );
        toast_init();
      } else {
        alert('댓글 추가에 실패하였습니다.');
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 코멘트 리스트 보이기/안보이기
function comment_list_toggle(obj){
  var $comment_list_status = $(obj).attr('status');
  var slug = $(obj).attr('slug');
  console.log(slug+'번 포스트 댓글 리스트:'+$comment_list_status);
  if ($comment_list_status == 'open') {
    $('#comment_list_open_'+slug).hide();
    $('#comment_list_close_'+slug).show();
    $('#comment_list_item_'+slug).show();
    $('#comment_list_'+slug).attr({"status":"open"}); // 코멘트 리스트 status 속성값을 변경한다.

  } else {
    $('#comment_list_close_'+slug).hide();
    $('#comment_list_open_'+slug).show();
    $('#comment_list_item_'+slug).hide();
    $('#comment_list_'+slug).attr({"status":"close"}); // 코멘트 리스트 status 속성값을 변경한다.
  }
}  

// 댓글 펼치기
function unfold_comment(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_id = 'short_comment_'+idx; // 댓글 요약 내용 요소
  var long_comment_id = 'long_comment_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_id = 'hidden_comment_'+idx // 댓글 수정폼 요소
  var comment_modify_save_btn_id = 'comment_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+short_comment_id+"").hide();
  $("#"+hidden_comment_id+"").hide();
  $("#"+comment_modify_save_btn_id+"").hide();
  $("#"+long_comment_id+"").show();
}

// 댓글에 대한 댓글 요소 펼치기
function unfold_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_reply_id = 'short_comment_reply_'+idx; // 댓글 요약 내용 요소
  var long_comment_reply_id = 'long_comment_reply_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_reply_id = 'hidden_comment_reply_'+idx // 댓글 수정폼 요소
  var comment_reply_modify_save_btn_id = 'comment_reply_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+short_comment_reply_id+"").hide();
  $("#"+hidden_comment_reply_id+"").hide();
  $("#"+comment_reply_modify_save_btn_id+"").hide();
  $("#"+long_comment_reply_id+"").show();
}

// 댓글 접기
function fold_comment(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_id = 'short_comment_'+idx;
  var long_comment_id = 'long_comment_'+idx;
  var hidden_comment_id = 'hidden_comment_'+idx // 댓글 수정폼 요소
  var comment_modify_save_btn_id = 'comment_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+long_comment_id+"").hide();
  $("#"+hidden_comment_id+"").hide();
  $("#"+comment_modify_save_btn_id+"").hide();
  $("#"+short_comment_id+"").show();
}

// 댓글에 대한 댓글 요소 접기
function fold_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_reply_id = 'short_comment_reply_'+idx; // 댓글 요약 내용 요소
  var long_comment_reply_id = 'long_comment_reply_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_reply_id = 'hidden_comment_reply_'+idx // 댓글 수정폼 요소
  var comment_reply_modify_save_btn_id = 'comment_reply_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+long_comment_reply_id+"").hide();
  $("#"+hidden_comment_reply_id+"").hide();
  $("#"+comment_reply_modify_save_btn_id+"").hide();
  $("#"+short_comment_reply_id+"").show();
}

// 댓글 수정하기 버튼 이벤트
function modify_comment(obj) {
  var idx = $(obj).attr('idx');
  var nickname = $("#comment_"+idx+"").find('span').eq(0).text().trim();
  var input_div_id = 'passwd_check_'+idx;
  var input_id = 'modify_passwd_'+idx;
  console.log(input_id + ' nickname : '+ nickname);
  if(nickname == '운영자') { // 운영자 댓글인 경우 비밀번호 확인 요소를 펼친다.
    $("#"+input_div_id+"").slideToggle();
    $("#"+input_id+"").attr({"title":"modify"}).focus(); // title 속성값 변경 및 커서 이동

  } else {
    // 운영자는 댓글 비번 입력 및 확인 과정을 생략한다. 
    unfold_hidden_comment(obj);  // 댓글 수정 요소를 펼친다.
  }
}

// 댓글에 대한 댓글 수정하기 버튼 이벤트
function modify_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var nickname = $("#comment_reply_"+idx+"").find('span').eq(1).text().trim();
  var input_div_id = 'reply_passwd_check_'+idx;
  var input_id = 'modify_reply_passwd_'+idx;
  console.log(input_id + ' nickname : '+ nickname);
  if(nickname == '운영자') { // 운영자 댓글인 경우 비밀번호 확인 요소를 펼친다.
    $("#"+input_div_id+"").slideToggle();
    $("#"+input_id+"").attr({"title":"modify"}).focus(); // title 속성값 변경 및 커서 이동

  } else {
    // 운영자는 댓글 비번 입력 및 확인 과정을 생략한다. 
    unfold_hidden_comment_reply(obj);  // 댓글 수정 요소를 펼친다.
  }
}

// DOM 내의 모든 delete 버튼에 이벤트를 등록해준다.
function trash_init(){
  $('.del_btn').each(function(index,item){
    var mark = $(item).attr('mark');
    var del_btn = $(item).find('span').eq(1); // trash_body 부분 인스턴스 생성
    console.log(item);
    item.addEventListener('mouseover',function(){
      del_btn.css("content" , "url( '/img/trash_body_hover.svg' )");
    });
    item.addEventListener('mouseout',function(){
      del_btn.css("content" , "url( '/img/trash_body.svg' )");
    });
    item.addEventListener('click',function(){
      if(mark == 'comment'){
        del_comment(item);
      } else {
        del_comment_reply(item);
      }
    });
  });
  console.log($('.del_btn').length+'개 삭제 버튼 요소에 이벤트가 등록되었습니다');
}

// 새로 추가된 delete 버튼에 이벤트를 등록해준다.
function trash_init_each(mark,idx){
  if(mark == 'comment'){
    var id = 'delete_button_'+idx; // 새로 추가된 댓글 삭제 버튼 id
  } else {
    var id = 'reply_delete_button_'+idx; // 새로 추가된 대댓글 삭제 버튼 id
  }
  var btn_el = document.getElementById(id); // 새로 추가된 버튼 요소를 변수에 담는다.
  var del_btn = $(btn_el).find('span').eq(1); // trash_body 부분 인스턴스 생성
  console.log(del_btn);
  btn_el.addEventListener('mouseover',function(){
    del_btn.css("content" , "url( '/img/trash_body_hover.svg' )");
  });
  btn_el.addEventListener('mouseout',function(){
    del_btn.css("content" , "url( '/img/trash_body.svg' )");
  });
  btn_el.addEventListener('click',function(){
    if(mark == 'comment'){
      del_comment(btn_el);
    } else {
      del_comment_reply(btn_el);
    }
  });
  console.log(id+'요소에 이벤트가 등록되었습니다');
}

// 댓글 삭제하기 버튼 이벤트
function del_comment(obj) {
  var idx = $(obj).attr('idx');
  var nickname = $("#comment_"+idx+"").find('span').eq(0).text().trim();
  var post_slug = $(obj).attr('post_slug');
  var input_div_id = 'passwd_check_'+idx;
  var input_id = 'modify_passwd_'+idx;
  if(nickname == '운영자') { // 운영자 댓글인 경우 비밀번호 확인 요소를 펼친다.
    $("#"+input_div_id+"").slideToggle();
    $("#"+input_id+"").attr({"title":"delete"}).focus(); // title 속성값 변경 및 커서 이동
  } else {
    // 운영자는 댓글 비번 입력 및 확인 과정을 생략한다.
    if(confirm('댓글이 삭제됩니다.')){
      delete_comment(idx,post_slug); // 댓글을 삭제한다.
    }
  }
}

// 댓글에 대한 댓글 삭제하기 버튼 이벤트
function del_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var nickname = $("#comment_reply_"+idx+"").find('span').eq(1).text().trim();
  var input_div_id = 'reply_passwd_check_'+idx;
  var input_id = 'modify_reply_passwd_'+idx;
  if(nickname == '운영자') { // 운영자 댓글인 경우 비밀번호 확인 요소를 펼친다.
    $("#"+input_div_id+"").slideToggle();
    $("#"+input_id+"").attr({"title":"delete"}).focus(); // title 속성값 변경 및 커서 이동
  } else {
    // 운영자는 댓글 비번 입력 및 확인 과정을 생략한다.
    if(confirm('댓글이 삭제됩니다.')){
      delete_comment_reply(idx); // 댓글을 삭제한다.
    }
  }
}

// 댓글 비밀번호값을 체크하여 비밀번호 수정 요소를 활성화 한다.
function check_value(obj,callback,callback_2,callback_3) {
  var passwd_value = $(obj).val();
  var idx = $(obj).attr('idx');
  var title = $(obj).attr('title'); 
  
  var cur_length = passwd_value.length; // 현재 문자열 길이
  var last_char = passwd_value.charAt(cur_length - 1);
  // passwd_temp = passwd_value;
  // console.log(passwd_value+','+passwd_temp.length+','+cur_length+','+last_char);
  // 입력값 길이가 4자를 초과하는지 마지막 입력 문자가 숫자인지 확인
  if(isNaN(last_char) == true) {
    alert("비밀번호는 4자리 숫자만 입력가능합니다.");
    if(callback != undefined) {
      undo_input(obj,passwd_value); //숫자가 아닌 값을 입력했을 입력값 제거한다.
    }
  }

  if(passwd_value.length == 4 && isNaN(last_char) == false){
    console.log(title);

    // 해당 댓글의 정보를 가져와 입력한 비밀번호와 해당 댓글의 비밀번호를 비교한다.
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/postmanage/getcomment',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        idx : idx,

      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);

        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var comment_passwd = result_array.comment_passwd;
        console.log(passwd_value+','+comment_passwd);

        // 비밀번호 입력값이 댓글의 비밀번호와 같을 경우 댓글 수정 요소를 펼친다.
        if(passwd_value == comment_passwd) {
          if(title == 'modify'){
            if(callback_2 != undefined){
              unfold_hidden_comment(obj);  // 댓글 수정 요소를 펼친다.
            }
          } else {
            if(confirm('댓글이 삭제됩니다.')){
              if(callback_3 != undefined){
                delete_comment(idx); // 댓글을 삭제한다.
              }
            }          
          }
        } else {
          alert("비밀번호를 정확히 입력해주세요.");
        }

      },
      error: function(err) {
        // $('#loading').fadeOut(200);

        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
    
  } 
}

// 댓글에 대한 댓글 비밀번호값을 체크하여 비밀번호 수정 요소를 활성화 한다.
function check_value_reply(obj,callback,callback_2,callback_3) {
  var passwd_value = $(obj).val();
  var idx = $(obj).attr('idx');
  var title = $(obj).attr('title'); 
  
  var cur_length = passwd_value.length; // 현재 문자열 길이
  var last_char = passwd_value.charAt(cur_length - 1);
  // passwd_temp = passwd_value;
  // console.log(passwd_value+','+passwd_temp.length+','+cur_length+','+last_char);
  // 입력값 길이가 4자를 초과하는지 마지막 입력 문자가 숫자인지 확인
  if(isNaN(last_char) == true) {
    alert("비밀번호는 4자리 숫자만 입력가능합니다.");
    if(callback != undefined) {
      undo_input(obj,passwd_value); //숫자가 아닌 값을 입력했을 입력값 제거한다.
    }
  }

  if(passwd_value.length == 4 && isNaN(last_char) == false){
    console.log(title);

    // 해당 댓글의 정보를 가져와 입력한 비밀번호와 해당 댓글의 비밀번호를 비교한다.
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/postmanage/getcommentreply',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        idx : idx,

      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);

        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var comment_reply_passwd = result_array.comment_reply_passwd;
        console.log(passwd_value+','+comment_reply_passwd);

        // 비밀번호 입력값이 댓글의 비밀번호와 같을 경우 댓글 수정 요소를 펼친다.
        if(passwd_value == comment_reply_passwd) {
          if(title == 'modify'){
            if(callback_2 != undefined){
              unfold_hidden_comment_reply(obj);
            }
          } else {
            if(confirm('댓글이 삭제됩니다.')){
              if(callback_3 != undefined){
                delete_comment_reply(idx);
              }
            }          
          }
        } else {
          alert("비밀번호를 정확히 입력해주세요.");
        }

      },
      error: function(err) {
        // $('#loading').fadeOut(200);

        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
    
  } 
}

// 댓글 비밀번호값을 체크한다.
function check_passwd_value(obj,callback) {
  var passwd_value = $(obj).val();
  var cur_length = passwd_value.length; // 현재 문자열 길이
  var last_char = passwd_value.charAt(cur_length - 1);
 
  // 입력값 길이가 4자를 초과하는지 마지막 입력 문자가 숫자인지 확인
  if(isNaN(last_char) == true) {
    alert("비밀번호는 4자리 숫자만 입력가능합니다.");
    if(callback != undefined) {
      undo_input(obj,passwd_value); //숫자가 아닌 값을 입력했을 입력값 제거한다.
    }
  }
}
// 비밀번호 입력값에서 숫자가 아닌값을 제외하고 다시 넣어준다.
function undo_input(obj, passwd_value) {
  var new_value = '';
  for(i=0; i < passwd_value.length; i++){
    if(isNaN(passwd_value.charAt(i)) == true){
      break;
    } else { // 숫자인 값만 새로운 변수에 담는다.
      new_value += passwd_value.charAt(i);
    }
  }
      
    $(obj).val(new_value); 
}

// 댓글 수정 요소 펼치기
function unfold_hidden_comment(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_id = 'short_comment_'+idx; // 댓글 요약 내용 요소
  var long_comment_id = 'long_comment_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_id = 'hidden_comment_'+idx; // 댓글 수정폼 요소
  var comment_modify_id = 'comment_modify_'+idx; // 댓글 수정 textarea 요소
  var comment_modify_save_btn_id = 'comment_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  console.log(hidden_comment_id);
  $("#"+short_comment_id+"").hide();
  $("#"+long_comment_id+"").hide();
  $("#"+hidden_comment_id+"").show();
  $("#"+comment_modify_save_btn_id+"").show();
  $("#"+comment_modify_id+"").focus(); // 댓글 수정 textarea 요소로 커서를 옮긴다.
}

// 댓글에 대한 댓글 수정 요소 펼치기
function unfold_hidden_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_reply_id = 'short_comment_reply_'+idx; // 댓글 요약 내용 요소
  var long_comment_reply_id = 'long_comment_reply_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_reply_id = 'hidden_comment_reply_'+idx; // 댓글 수정폼 요소
  var comment_reply_modify_id = 'comment_reply_modify_'+idx; // 댓글 수정 textarea 요소
  var comment_reply_modify_save_btn_id = 'comment_reply_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  console.log(hidden_comment_reply_id);
  $("#"+short_comment_reply_id+"").hide();
  $("#"+long_comment_reply_id+"").hide();
  $("#"+hidden_comment_reply_id+"").show();
  $("#"+comment_reply_modify_save_btn_id+"").show();
  $("#"+comment_reply_modify_id+"").focus(); // 댓글 수정 textarea 요소로 커서를 옮긴다.
}

// 댓글 내용 업데이트
function update_comment(obj){
  var idx = $(obj).attr('idx');
  // var short_comment_id = 'short_comment_'+idx;
  var long_comment_id = 'long_comment_'+idx;
  var new_is_secret_id = 'is_secret_'+idx;
  var new_comment_id = 'comment_modify_'+idx;
  var new_comment_passwd_id = 'modify_passwd_'+idx;  // 수정 요소의 비밀번호를 가져온다.
  var new_is_secret = $("#"+new_is_secret_id+"").is(':checked') ? 'Y' : 'N';
  var new_comment = $("#"+new_comment_id+"").val(); // 댓글 수정하기 textarea 요소 값을 가져온다.
  // new_comment = new_comment.replace(/(?:\r\n|\r|\n)/g,'<br/>');
  var new_comment_passwd = $("#"+new_comment_passwd_id+"").val();
  console.log(new_comment+', 비밀글 : '+new_is_secret+', 비번 : '+new_comment_passwd);

  // 댓글 정보를 업데이트 한다.
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/updatecomment',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      idx : idx,
      new_comment : new_comment,
      new_comment_passwd : new_comment_passwd,
      new_is_secret : new_is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment = data.extra;
      var updated_comment = comment.post_comment;
      console.log(updated_comment);

      if(comment != null) {
        alert('댓글이 수정 되었습니다.');
        // 수정된 댓글을 화면에 반영한다.
        $("#"+long_comment_id+"").empty().html(nl2br(updated_comment));

        // 엔터문자 개수를 센다.
        var count = count_str(updated_comment,'\n')
        
        // 코멘트 텍스트 단어 요소로된 배열로 만든다.
        var comment_text = text_to_word(updated_comment);

        // 단어의 개수가 4개 초과이거나 엔터문자가 1개 초과인 경우 요소를 넣어준다.
        if(comment_text.length > 4 || count > 0) {
          $("#"+long_comment_id+"").append('<div><span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+ idx +'" onclick="fold_comment(this)">간략히 보기</span></div>');
        }
        
        unfold_comment(obj);
        console.log(comment_text);
        // $("#"+new_comment_passwd_id+"").val(''); // 수정 댓글 비밀번호를 초기화 한다.
        // $("#modify_button_"+idx+"").click();

      } else {
        alert('댓글 수정에 실패하였습니다.')
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 댓글에 대한 댓글 내용 업데이트
function update_comment_reply(obj){
  var idx = $(obj).attr('idx');
  // var short_comment_id = 'short_comment_'+idx;
  var long_comment_reply_id = 'long_comment_reply_'+idx;
  var new_reply_is_secret_id = 'reply_is_secret_'+idx;
  var new_comment_reply_id = 'comment_reply_modify_'+idx;
  var new_comment_reply_passwd_id = 'modify_reply_passwd_'+idx;  // 수정 요소의 비밀번호를 가져온다.
  var new_reply_is_secret = $("#"+new_reply_is_secret_id+"").is(':checked') ? 'Y' : 'N';
  var new_comment_reply = $("#"+new_comment_reply_id+"").val(); // 댓글 수정하기 textarea 요소 값을 가져온다.
  // new_comment = new_comment.replace(/(?:\r\n|\r|\n)/g,'<br/>');
  var new_comment_reply_passwd = $("#"+new_comment_reply_passwd_id+"").val();
  console.log(new_comment_reply+', 비밀글 : '+new_reply_is_secret+', 비번 : '+new_comment_reply_passwd);

  // 댓글 정보를 업데이트 한다.
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/updatecommentreply',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      idx : idx,
      new_comment_reply : new_comment_reply,
      new_comment_reply_passwd : new_comment_reply_passwd,
      new_reply_is_secret : new_reply_is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment_reply = data.extra;
      var updated_comment_reply = comment_reply.post_comment_reply;
      console.log(updated_comment_reply);

      if(comment_reply != null) {
        alert('댓글에 대한 댓글이 수정 되었습니다.');
        // 수정된 댓글을 화면에 반영한다.
        $("#"+long_comment_reply_id+"").empty().html(nl2br(updated_comment_reply));

        // 엔터문자 개수를 센다.
        var count = count_str(updated_comment_reply,'\n')

        // 코멘트 텍스트 단어 요소로된 배열로 만든다.
        var comment_text = text_to_word(updated_comment_reply);

        // 단어의 개수가 4개 초과이거나 엔터문자가 1개 초과인 경우 요소를 넣어준다.
        if(comment_text.length > 4 || count > 0) {
          $("#"+long_comment_reply_id+"").append('<div><span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+ idx +'" onclick="fold_comment_reply(this)">간략히 보기</span></div>');
        }

        unfold_comment_reply(obj);
        console.log(comment_reply);
        // $("#"+new_comment_reply_passwd_id+"").val(''); // 수정 댓글 비밀번호를 초기화 한다.
        // $("#reply_modify_button_"+idx+"").click();

      } else {
        alert('댓글 수정에 실패하였습니다.')
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 문장을 단어 단위로 자른다.
function truncate(str, no_words) {
  return str.split(" ").splice(0,no_words).join(" ");
}

// 텍스트의 엔터기호를 삭제하고 각 요소를 '.'요소로 잘라 단어 단위 요소로 구성된 배열을 만든다.
function text_to_word(text) {
  var word_array = text.replace(/\n/g,'').split(' '); // 엔터기호를 삭제하고 공백문자 기준으로 자른다.
  console.log(word_array);
  $.each(word_array,function(index,item){
    var word = item.split('.'); // 각 요소를 '.' 기준으로 자르고
    console.log(word);
    if(word.length > 1) {
      $.each(word,function(index_2,item_2){
        word_array.splice(index,0,item_2) ; // 배열에 다시 넣어 준다.
        index++; // 다음 위치로 index를 변경해준다.
      });
      word_array.splice(index,1); // 원본 요소를 삭제한다.
    }
    
  }); // --end of each
  console.log(word_array);
  return word_array;
}

// 특정문자의 개수를 센다.
function count_str(text,str){
  var count = 0;
  var searchChar = str; // 찾으려는 문자
  var pos = text.indexOf(searchChar); //pos는 str을 처음 만나는 위치의 index

  while (pos !== -1) {
    count++;
    pos = text.indexOf(searchChar, pos + 1); // 첫 번째 인덱스부터 str를 찾습니다.
  }

  console.log('"\"'+ str + '"'+'의 개수 : '+ count);
  return count;
}

// arg 천단위 컴마 제거
function trd_comma_add(arg){
  if(arg){
      arg = arg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  return arg;
}

// arg 천단위 컴마 제거
function trd_comma_remove(arg){
  if(arg){
      arg = parseInt(arg.replace(/,/g,""));
  }
  return arg;
}

// input 요소 숫자 입력시 천단위 컴마 추가
function comma_check(obj){

  // console.log($(obj).val());

  comma = $(obj).val();
  if(comma ==''){  // 공백이면 함수 종료 (Nan 표시 방지)
      return;
  }
  comma = parseInt(comma.replace(/,/g,""));  // 천단위 콤마 제거
  comma = comma.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); // 천단위 콤마 추가
  // console.log(comma);

  $(obj).val(comma);
}

// 댓글을 삭제한다.
function delete_comment(idx,post_slug) {
  // 댓글에 대한 댓글 개수를 확인한다.
  var comment_reply_count = $("#comment_reply_count_"+idx+"").val();

  
  if(Number(comment_reply_count) == 0){
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/postmanage/deletecomment',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        idx : idx

      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);

        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var comment_reply_count = $("#comment_reply_count_"+idx+"").val();
        console.log(result_array.is_del+','+comment_reply_count);

        if(result_array != null) {
            alert('댓글이 삭제 되었습니다.');
            $("#comment_"+idx+"").remove();
            var new_comment_count = Number($('#comment_count_'+post_slug).text()) - 1; // 기존의 댓글 개수에서 삭제된 댓글 개수를 빼 새로운 개수를 구한다.
            $('#comment_count_'+post_slug).text(new_comment_count); // 코멘트 개수를 업데이트 해준다.
          } else {
          alert('댓글 삭제에 실패하였습니다.');
        }
      },
      error: function(err) {
        // $('#loading').fadeOut(200);

        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
  }else {
    // console.log(content);
    console.log(comment_reply_count);
    alert('댓글이 있는 댓글은 삭제할 수 없습니다.');
  }
}

// 댓글에 대한 댓글을 삭제한다.
function delete_comment_reply(idx) {
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/deletecommentreply',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      idx : idx

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      
      console.log(result_array.is_del);

      if(result_array != null) {
        alert('댓글이 삭제 되었습니다.');
        $("#comment_reply_"+idx+"").remove();
      } else {
        alert('댓글 삭제에 실패하였습니다.')
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 대댓글 등록 관련 요소를 활성화/비활성화 한다.
function comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var comment_reply_reg_id = 'comment_reply_reg_'+idx;
  console.log(idx+'번 댓글에 댓글을 남김니다.');
  $("#"+comment_reply_reg_id+"").slideToggle();
}

// 저장된 댓글 요소를 추한다.
function input_comment(comment,post_slug) {
  var comment_list_item_id = 'comment_list_item_'+post_slug;
  var word_count = text_to_word(comment.post_comment).length; // 단어의 개수를 센다.
  var br_count = count_str(comment.post_comment,'\n'); // 엔터문자의 개수를 센다.
  var mark = 'comment';
  console.log(comment_list_item_id);
  // $("#"+comment_list_id+"").empty();
  $('#toast_message').remove(); // 기존의 토스트 메세지 요소를 제거한다.
  if(word_count > 4 || br_count > 0 ) {
    console.log('단어수가 5개 이상이거나 엔터문자가 1개 이상인 경우');
    $("#"+comment_list_item_id+"").append(
      '<!-- 코멘트 내용 -->'+
      '<div class="border-top" id="comment_'+comment.idx+'">'+
        '<!-- 토스트 메세지 요소 -->'+
        '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
          '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
            '<div class="d-flex justify-content-center">'+
              '<div class="toast-body">'+
                '댓글이 추가 되었습니다.'+
              '</div>'+
              '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
            '</div>'+
          '</div>'+
        '</div>'+              
        '<div class="row no-gutters mt-2 mx-0 px-2">'+
          '<div class="col-md-12">'+
            '<div class="d-flex align-items-center mx-0 p-0">'+
              '<p class="text-center">'+
                // '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                //   '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                //   '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                // '</svg>'+
                '<img src="/img/logo.png" width="16" height="16">'+
                '<span class="ms-2 text-primary" style="font-size:.75rem; font-weight:bolder" title="운영자">'+
                  '운영자'+
                '</span>'+
                '&nbsp;&nbsp;'+
                '<small class="ms-2 text-dark" style="font-size:.5rem;">'+
                comment.reg_date+
                '</small>'+
              '</p>'+
            '</div>'+
            '<div class="row">'+

              '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_'+comment.idx+'">'+
                nl2br(word_limit(comment.post_comment,4)) +
                
                '<div>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="unfold_comment(this)">자세히 보기</span>'+
                '</div>'+
              
              '</div>'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem; display:none;" id="long_comment_'+comment.idx+'">'+
                nl2br(comment.post_comment)+
                
                '<div>'+						
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="fold_comment(this)">간략히 보기</span>'+
                '</div>'+
                
              '</div>'+
              '<!-- 댓글 수정을 위한 textarea hidden 요소 -->'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_'+comment.idx+'">'+
                '<div class="col-12 form-check d-flex align-items-center">'+
                  '<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_'+comment.idx+'">'+
                  '<label class="form-check-label me-3" for="is_secret_'+comment.idx+'">'+
                    '<span style="font-size:.75rem;">비밀글</span>'+
                  '</label>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="fold_comment(this)">취소</span>'+
                  
                '</div>'+
                '<div class="form-floating">'+
                  '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_modify_'+comment.idx+'" name="comment_modify_'+comment.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment.post_comment+'</textarea>'+
                  '<label for="comment_modify_'+comment.idx+'">댓글 수정하기</label>'+
                '</div>'+
                '<br>'+									
              '</div>'+
              
              '<!-- 댓글 수정,삭제 관련 요소 -->'+
              '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                '<div class="row d-flex justify-content-center px-0">'+
                  '<div class="d-flex justify-content-center" style="height:80%;" >'+
                    '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment.idx+'" onclick="update_comment(this)" style="width:100%;" id="comment_modify_save_btn_'+comment.idx+'">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                  '<div class="comment_pw_check mb-1" id="passwd_check_'+comment.idx+'">'+
                    '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value(this,undo_input,unfold_hidden_comment,delete_comment)" id="modify_passwd_'+comment.idx+'" idx="'+comment.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                  '</div>'+
                '</div>'+
                '<div class="d-flex justify-content-center align-items-end">'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="reply_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="comment_reply(this)" idx="'+comment.idx+'" title="댓글 남기기">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                        '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="modify_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment(this)" idx="'+comment.idx+'" title="수정">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                        '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                        '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div del_btn" id="delete_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" post_slug ="'+post_slug+'" mark="comment" title="삭제">'+
                    '<p class="trash_cap_wrap text-center">'+
                      '<span class="trash_cap py-0 my-0"></span>'+
                    '</p>'+
                    '<span class="trash comment_icon">'+
                    '</span>'+
                  '</div>'+
                '</div>'+
              '</div>'+
              '<!-- 댓글에 대한 댓글 등록 요소  -->'+
              '<div class="comment_reply mt-2 p-1 border rounded" id="comment_reply_reg_'+comment.idx+'">'+
                '<div class="row mb-2">'+
                  '<div class="col-3 pe-0">'+
                    '<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_reply_nickname_'+comment.idx+'" name="comment_reply_nickname_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 pe-0">'+
                    '<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_reply_passwd_'+comment.idx+'" name="comment_reply_passwd_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">'+
                    '<input class="form-check-input me-2" type="checkbox" value="" id="comment_reply_is_secret_'+comment.idx+'" name="comment_reply_is_secret_'+comment.idx+'">'+
                    '<label class="form-check-label" for="comment_reply_is_secret_'+comment.idx+'">'+
                      '<span style="font-size:.75rem;">비밀글</span>'+
                    '</label>'+
                  '</div>'+
                '</div>'+
                '<div class="row">'+
                  '<div class="col-9 form-floating d-flex justify-content-center pe-0">'+
                    '<textarea class="form-control" placeholder="댓글에 댓글 남기기" id="post_comment_reply_'+comment.idx+'" name="post_comment_reply_'+comment.idx+'" style="height: 100px; width:98%;" ></textarea>'+
                    '<label class="ps-4" for="post_comment_reply_'+comment.idx+'">댓글에 댓글 남기기</label>'+
                  '</div>'+
                  '<div class="col-3 d-grid ps-0">'+
                    '<button type="button" class="btn btn-outline-primary" slug="'+post_slug+'" org_idx="'+comment.idx+'" onclick="save_comment_reply(this)" style="width:95%;">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+												
          '</div>'+
        '</div>'+
        '<!-- end of comment -->'+
        '<!-- 댓글에 대한 댓글 내용 -->'+
        '<div id="comment_reply_list_'+comment.idx+'">'+
        '</div>'+
        '<!-- end of comment_reply_list -->'+
        '<input type="hidden" id="comment_reply_count_'+comment.idx+'" value="0" >'+
      '</div>'+
      '<!-- end of comment -->'

    );
  } else {
    console.log('단어수가 5개 미만이거나 엔터문자가 없는 경우');
    $("#"+comment_list_item_id+"").append(
      '<!-- 코멘트 내용 -->'+
      '<div class="border-top" id="comment_'+comment.idx+'">'+
        '<!-- 토스트 메세지 요소 -->'+
        '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
          '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
            '<div class="d-flex justify-content-center">'+
              '<div class="toast-body">'+
                '댓글이 추가 되었습니다.'+
              '</div>'+
              '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
            '</div>'+
          '</div>'+
        '</div>'+    
        '<div class="row no-gutters mt-2 mx-0 px-2">'+
          '<div class="col-md-12">'+
            '<div class="d-flex align-items-center mx-0 p-0">'+
              '<p class="text-center">'+
                // '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                //   '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                //   '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                // '</svg>'+
                '<img src="/img/logo.png" width="16" height="16">'+
                '<span class="ms-2 text-primary" style="font-size:.75rem; font-weight:bolder" title="운영자">'+
                  '운영자'+
                '</span>'+
                '&nbsp;&nbsp;'+
                '<small class="ms-2 text-dark" style="font-size:.5rem;">'+
                comment.reg_date+
                '</small>'+
              '</p>'+
            '</div>'+
            '<div class="row">'+

              '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_'+comment.idx+'">'+
                nl2br(word_limit(comment.post_comment,4)) +
                
              '</div>'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem; display:none;" id="long_comment_'+comment.idx+'">'+
                nl2br(comment.post_comment)+
                
              '</div>'+
              '<!-- 댓글 수정을 위한 textarea hidden 요소 -->'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_'+comment.idx+'">'+
                '<div class="col-12 form-check d-flex align-items-center">'+
                  '<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_'+comment.idx+'">'+
                  '<label class="form-check-label me-3" for="is_secret_'+comment.idx+'">'+
                    '<span style="font-size:.75rem;">비밀글</span>'+
                  '</label>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="fold_comment(this)">취소</span>'+
                  
                '</div>'+
                '<div class="form-floating">'+
                  '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_modify_'+comment.idx+'" name="comment_modify_'+comment.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment.post_comment+'</textarea>'+
                  '<label for="comment_modify_'+comment.idx+'">댓글 수정하기</label>'+
                '</div>'+
                '<br>'+									
              '</div>'+
              
              '<!-- 댓글 수정,삭제 관련 요소 -->'+
              '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                '<div class="row d-flex justify-content-center px-0">'+
                  '<div class="d-flex justify-content-center" style="height:80%;" >'+
                    '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment.idx+'" onclick="update_comment(this)" style="width:100%;" id="comment_modify_save_btn_'+comment.idx+'">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                  '<div class="comment_pw_check mb-1" id="passwd_check_'+comment.idx+'">'+
                    '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value(this,undo_input,unfold_hidden_comment,delete_comment)" id="modify_passwd_'+comment.idx+'" idx="'+comment.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                  '</div>'+
                '</div>'+
                '<div class="d-flex justify-content-center align-items-end">'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="reply_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="comment_reply(this)" idx="'+comment.idx+'" title="댓글 남기기">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                        '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="modify_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment(this)" idx="'+comment.idx+'" title="수정">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                        '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                        '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div del_btn" id="delete_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" post_slug ="'+post_slug+'" mark="comment" title="삭제">'+
                    '<p class="trash_cap_wrap text-center">'+
                      '<span class="trash_cap py-0 my-0"></span>'+
                    '</p>'+
                    '<span class="trash comment_icon">'+
                    '</span>'+
                  '</div>'+
                '</div>'+
              '</div>'+
              '<!-- 댓글에 대한 댓글 등록 요소  -->'+
              '<div class="comment_reply mt-2 p-1 border rounded" id="comment_reply_reg_'+comment.idx+'">'+
                '<div class="row mb-2">'+
                  '<div class="col-3 pe-0">'+
                    '<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_reply_nickname_'+comment.idx+'" name="comment_reply_nickname_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 pe-0">'+
                    '<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_reply_passwd_'+comment.idx+'" name="comment_reply_passwd_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">'+
                    '<input class="form-check-input me-2" type="checkbox" value="" id="comment_reply_is_secret_'+comment.idx+'" name="comment_reply_is_secret_'+comment.idx+'">'+
                    '<label class="form-check-label" for="comment_reply_is_secret_'+comment.idx+'">'+
                      '<span style="font-size:.75rem;">비밀글</span>'+
                    '</label>'+
                  '</div>'+
                '</div>'+
                '<div class="row">'+
                  '<div class="col-9 form-floating d-flex justify-content-center pe-0">'+
                    '<textarea class="form-control" placeholder="댓글에 댓글 남기기" id="post_comment_reply_'+comment.idx+'" name="post_comment_reply_'+comment.idx+'" style="height: 100px; width:98%;" ></textarea>'+
                    '<label class="ps-4" for="post_comment_reply_'+comment.idx+'">댓글에 댓글 남기기</label>'+
                  '</div>'+
                  '<div class="col-3 d-grid ps-0">'+
                    '<button type="button" class="btn btn-outline-primary" slug="'+post_slug+'" org_idx="'+comment.idx+'" onclick="save_comment_reply(this)" style="width:95%;">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+												
          '</div>'+
        '</div>'+
        '<!-- end of comment -->'+
        '<!-- 댓글에 대한 댓글 내용 -->'+
        '<div id="comment_reply_list_'+comment.idx+'">'+
        '</div>'+
        '<!-- end of comment_reply_list -->'+
        '<input type="hidden" id="comment_reply_count_'+comment.idx+'" value="0" >'+
      '</div>'+
      '<!-- end of comment -->'
    );
  }
  trash_init_each(mark,comment.idx);
}

// 저장된 댓글에 대한 댓글 요소를 추가한다.
function input_comment_reply(comment_reply,org_idx) {
  var comment_reply_list_id = 'comment_reply_list_'+org_idx;
  var word_count = text_to_word(comment_reply.post_comment_reply).length; // 단어의 개수를 센다.
  var br_count = count_str(comment_reply.post_comment_reply,'\n'); // 엔터문자의 개수를 센다.
  var mark = 'comment_reply';
  console.log(comment_reply_list_id);
  // $("#"+comment_reply_list_id+"").empty();
  $('#toast_message').remove(); // 기존의 토스트 메세지 요소를 제거한다.
  if(word_count > 4 || br_count > 0 ) {
    console.log('단어수가 5개 이상이거나 엔터문자가 1개 이상인 경우');
    $("#"+comment_reply_list_id+"").append(
      '<div class="border-top comment_reply_content" id="comment_reply_'+comment_reply.idx+'">'+
        '<!-- 토스트 메세지 요소 -->'+
        '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
          '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
            '<div class="d-flex justify-content-center">'+
              '<div class="toast-body">'+
                '댓글에 대한 댓글이 추가 되었습니다.'+
              '</div>'+
              '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
            '</div>'+
          '</div>'+
        '</div>'+                                            
        '<div class="row no-gutters mt-2 mx-0 px-2">'+
          '<div class="col-md-12">'+
            '<div class="d-flex align-items-center mx-0 p-0">'+
              '<p class="text-center">'+                                                    
                '<span class="text-primary me-1">'+
                  '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-right" viewBox="0 0 16 16">'+
                    '<path fill-rule="evenodd" d="M1.5 1.5A.5.5 0 0 0 1 2v4.8a2.5 2.5 0 0 0 2.5 2.5h9.793l-3.347 3.346a.5.5 0 0 0 .708.708l4.2-4.2a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 8.3H3.5A1.5 1.5 0 0 1 2 6.8V2a.5.5 0 0 0-.5-.5z"/>'+
                  '</svg>'+
                '</span>'+  
                // '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                //   '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                //   '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                // '</svg>'+
                '<img src="/img/logo.png" width="16" height="16">'+
                '<span class="ms-2 text-primary" style="font-size:.75rem; font-weight:bolder" title="운영자">'+
                  '운영자'+
                '</span>'+
                '&nbsp;&nbsp;'+
                '<small class="mr-2 text-dark" style="font-size:.5rem;">'+
                comment_reply.reg_date+
                '</small>'+
              '</p>'+
            '</div>'+
            '<div class="row">'+

              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem;" id="short_comment_reply_'+comment_reply.idx+'">'+
                nl2br(word_limit(comment_reply.post_comment_reply,4)) +
                '<div>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="unfold_comment_reply(this)">자세히 보기</span>'+
                '</div>'+
              
              '</div>'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem; display:none;" id="long_comment_reply_'+comment_reply.idx+'">'+
                nl2br(comment_reply.post_comment_reply)+
                
                '<div>'+						
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="fold_comment_reply(this)">간략히 보기</span>'+
                '</div>'+
                
              '</div>'+
              '<!-- 댓글에 대한 댓글 수정을 위한 textarea hidden 요소 -->'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_reply_'+comment_reply.idx+'">'+
                '<div class="col-12 form-check d-flex align-items-center">'+
                  '<input class="form-check-input me-2" type="checkbox" value="" id="reply_is_secret_'+comment_reply.idx+'">'+
                  '<label class="form-check-label me-3" for="reply_is_secret_'+comment_reply.idx+'">'+
                    '<span style="font-size:.75rem;">비밀글</span>'+
                  '</label>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="fold_comment_reply(this)">취소</span>'+
                  
                '</div>'+
                '<div class="form-floating">'+
                  '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_reply_modify_'+comment_reply.idx+'" name="comment_reply_modify_'+comment_reply.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment_reply.post_comment_reply+'</textarea>'+
                  '<label for="comment_reply_modify_'+comment_reply.idx+'">댓글 수정하기</label>'+
                '</div>'+
                '<br>'+									
              '</div>'+
              
              '<!-- 댓글에 대한 댓글 수정,삭제 관련 요소 -->'+
              '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                '<div class="row d-flex justify-content-center px-0">	'+
                  '<div class="d-flex justify-content-center" style="height:80%;" >'+
                    '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment_reply.idx+'" onclick="update_comment_reply(this)" style="width:100%;" id="comment_reply_modify_save_btn_'+comment_reply.idx+'">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                  '<div class="comment_pw_check mb-1" id="reply_passwd_check_'+comment_reply.idx+'">'+
                    '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value_reply(this,undo_input,unfold_hidden_comment_reply,delete_comment_reply)" id="modify_reply_passwd_'+comment_reply.idx+'" idx="'+comment_reply.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                  '</div>'+
                '</div>'+
                '<div class="d-flex justify-content-center align-items-end">'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="reply_modify_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment_reply(this)" idx="'+comment_reply.idx+'" title="수정">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                        '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                        '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div del_btn" id="reply_delete_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" mark="comment_reply" title="삭제">'+									
                    '<p class="trash_cap_wrap text-center">'+
                      '<span class="trash_cap py-0 my-0"></span>'+	
                    '</p>'+
                    '<span class="trash comment_icon">'+
                    '</span>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<!-- end of comment_reply -->'
    );
  } else {
    console.log('단어수가 5개 미만이거나 엔터문자가 없는 경우');
    $("#"+comment_reply_list_id+"").append(
      '<div class="border-top comment_reply_content" id="comment_reply_'+comment_reply.idx+'">'+
        '<!-- 토스트 메세지 요소 -->'+
        '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
          '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
            '<div class="d-flex justify-content-center">'+
              '<div class="toast-body">'+
                '댓글에 대한 댓글이 추가 되었습니다.'+
              '</div>'+
              '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
            '</div>'+
          '</div>'+
        '</div>'+                                            
        '<div class="row no-gutters mt-2 mx-0 px-2">'+
          '<div class="col-md-12">'+
            '<div class="d-flex align-items-center mx-0 p-0">'+
              '<p class="text-center">'+
                '<span class="text-primary me-1">'+
                  '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-right" viewBox="0 0 16 16">'+
                    '<path fill-rule="evenodd" d="M1.5 1.5A.5.5 0 0 0 1 2v4.8a2.5 2.5 0 0 0 2.5 2.5h9.793l-3.347 3.346a.5.5 0 0 0 .708.708l4.2-4.2a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 8.3H3.5A1.5 1.5 0 0 1 2 6.8V2a.5.5 0 0 0-.5-.5z"/>'+
                  '</svg>'+
                '</span>'+
                // '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                //   '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                //   '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                // '</svg>'+
                '<img src="/img/logo.png" width="16" height="16">'+
                '<span class="ms-2 text-primary" style="font-size:.75rem; font-weight:bolder" title="운영자">'+
                  '운영자'+
                '</span>'+
                '&nbsp;&nbsp;'+
                '<small class="mr-2 text-dark" style="font-size:.5rem;">'+
                comment_reply.reg_date+
                '</small>'+
              '</p>'+
            '</div>'+
            '<div class="row">'+

              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem;" id="short_comment_reply_'+comment_reply.idx+'">'+
                nl2br(word_limit(comment_reply.post_comment_reply,4)) +
              
              '</div>'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem; display:none;" id="long_comment_reply_'+comment_reply.idx+'">'+
                nl2br(comment_reply.post_comment_reply)+
                
              '</div>'+
              '<!-- 댓글 수정을 위한 textarea hidden 요소 -->'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_reply_'+comment_reply.idx+'">'+
                '<div class="col-12 form-check d-flex align-items-center">'+
                  '<input class="form-check-input me-2" type="checkbox" value="" id="reply_is_secret_'+comment_reply.idx+'">'+
                  '<label class="form-check-label me-3" for="reply_is_secret_'+comment_reply.idx+'">'+
                    '<span style="font-size:.75rem;">비밀글</span>'+
                  '</label>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="fold_comment_reply(this)">취소</span>'+
                  
                '</div>'+
                '<div class="form-floating">'+
                  '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_reply_modify_'+comment_reply.idx+'" name="comment_reply_modify_'+comment_reply.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment_reply.post_comment_reply+'</textarea>'+
                  '<label for="comment_reply_modify_'+comment_reply.idx+'">댓글 수정하기</label>'+
                '</div>'+
                '<br>'+									
              '</div>'+
              
              '<!-- 댓글에 대한 댓글 수정,삭제 관련 요소 -->'+
              '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                '<div class="row d-flex justify-content-center px-0">	'+
                  '<div class="d-flex justify-content-center" style="height:80%;" >'+
                    '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment_reply.idx+'" onclick="update_comment_reply(this)" style="width:100%;" id="comment_reply_modify_save_btn_'+comment_reply.idx+'">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                  '<div class="comment_pw_check mb-1" id="reply_passwd_check_'+comment_reply.idx+'">'+
                    '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value_reply(this,undo_input,unfold_hidden_comment_reply,delete_comment_reply)" id="modify_reply_passwd_'+comment_reply.idx+'" idx="'+comment_reply.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                  '</div>'+
                '</div>'+
                '<div class="d-flex justify-content-center align-items-end">'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="reply_modify_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment_reply(this)" idx="'+comment_reply.idx+'" title="수정">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                        '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                        '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div del_btn" id="reply_delete_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" mark="comment_reply" title="삭제">'+									
                    '<p class="trash_cap_wrap text-center">'+
                      '<span class="trash_cap py-0 my-0"></span>'+	
                    '</p>'+
                    '<span class="trash comment_icon">'+
                    '</span>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<!-- end of comment_reply -->'
    );
  }
  trash_init_each(mark,comment_reply.idx);
}

// 단어수를 제한하여 보여준다.
function word_limit(content,count) {
  var text_array = content.split(' '); // 문자열을 공백(단어) 단위로 자른다.
  var word_count = text_to_word(content).length;
  var new_text = ''; // 새로 생성할 문자열을 선언한다.
  if(word_count > count) { // 단어수가 count를 초과할 경우
    for(i=0; i < count; i++) {
      if(i < (count-1)) {
        new_text += text_array[i] + ' '; // 단어 배열의 각 요소를 새로 생성한 문자열에 담는다.
      } else {
        new_text += text_array[i];
      }
    }
    new_text += '...';
    console.log(new_text+' , '+word_count);

    return new_text;
  } else {
    console.log(content+' , '+word_count);

    return content;
  }
}

// 현재 브라우저 높이를 구한다.
function get_browser_height() {
  var userAgent = navigator.userAgent.toLowerCase();
 
  var browser = {
    msie    : /msie/.test( userAgent ) && !/opera/.test( userAgent ),
    safari  : /webkit/.test( userAgent ),
    firefox : /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent ),
    opera   : /opera/.test( userAgent )
  };   
 
  var nFinalHeight = 0;
 
  if( browser.msie ){ // 인터넷 익스플로러
    var scrollHeight = document.documentElement.scrollHeight;
    var browserHeight = document.documentElement.clientHeight;
  
    nFinalHeight  = scrollHeight < browserHeight ? browserHeight : scrollHeight;
 
  } else if ( browser.safari ){ //Chrome 또는 Safari, 같은 엔진을 사용합니다
    nFinalHeight   = document.body.scrollHeight;
  
  } else if ( browser.firefox ){ // Firefox 또는 NS
    var bodyHeight = document.body.clientHeight;
 
    nFinalHeight  = window.innerHeight < bodyHeight ? bodyHeight : window.innerHeight;
 
  } else if ( browser.opera ){ // Opera
    var bodyHeight = document.body.clientHeight;
 
    nFinalHeight = window.innerHeight < bodyHeight ? bodyHeight : window.innerHeight;
 
  } else { 
    alert("지원하지 않는 브라우저입니다.");
  }
   
  return nFinalHeight;
  // return window.innerHeight;

}

// 토스트 메세지 초기화 및 화면에 표시하기
function toast_init() {
  var toastElList = [].slice.call(document.querySelectorAll('.toast'))
  var toastList = toastElList.map(function (toastEl) {
    return new bootstrap.Toast(toastEl, {
      animation : true,
      autohide : true,
      delay : 5000 // 5초 후 사라짐
    })
  })
  console.log(toastList);
  toastList[0].show();
}

// 전체 포스트 순위 데이터를 가져와 표시한다.
function post_ranking_list_total(){

  var start_order_total = $("#start_order_total").val();

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getpostrankinglist',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      start_order_total : start_order_total

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      post_ranking_list_total_data = result_array.post_ranking_list_total;
      limit = result_array.limit;
      
      console.log(post_ranking_list_total_data);
      console.log('start_order_total : '+start_order_total +', new_start_order_total : '+ result_array.new_start_order_total);
      $("#loading_total").remove();
      for(i=0; i < limit; i++) {
        var index = Number(start_order_total-1) + i;
        // index가 배열의 길와 같은 경우 반복문을 종료한다.
        if(index == post_ranking_list_total_data.length){
          break;
        }
        var post = post_ranking_list_total_data[index];
        var ranking = index+1;
        $("#post_ranking_list_item_total").append(
          '<a href="/postmanage/detail/'+ post.slug +'?page=&filter=&search=" class="list-group-item list-group-item-action list-group-item-light py-1">'+
            '<div class="row">'+
              '<div class="col-2 text-center">'+
                '<span class="info_text">'+ranking+'.</span>'+
              '</div>'+
              '<div class="col-6 text-ellipsis">'+
                '<span class="info_text">'+post.title+'</span>'+
              '</div>'+
              '<div class="col-4 text-ellipsis">'+
                '<span class="info_text">'+post.main_subject_name+'>'+post.sub_subject_name+'</span>'+
              '</div>'+
            '</div>'+
          '</a>'
        );
      }

      // 포스트 랭킹 데이터의 개수가 limit보다 큰 경우 '더보기' 버튼 요소를 넣는다. 
      if( post_ranking_list_total_data.length > limit ) {
        $("#post_ranking_list_item_total_wrap").append(
          '<div class="d-flex justify-content-center w-100">'+
            '<button type="button" class="btn btn-outline-secondary btn-sm py-0 mt-2 mb-1" mark="total" onclick="add_post_list(this)" id="add_total_ranking_post"><span class="info_text">더보기</span></button>'+
          '</div>'
        );
      }
      
      // 데이터를 읽기 시작할 순서를 업데이트 해준다.
      $("#start_order_total").val(result_array.new_start_order_total);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 선택한 기간(일별)내 포스트 순위 데이터를 가져와 표시한다.
function post_ranking_list_daily_period(){

  var start_order_daily_period = $("#start_order_daily_period").val();
  var start_day = $("#start_day").val();
  var end_day = $("#end_day").val();

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getpostrankinglistbydaily',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      start_order_daily_period : start_order_daily_period,
      start_day : start_day,
      end_day : end_day

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      post_ranking_list_by_daily_data = result_array.post_ranking_list_by_daily;
      limit = result_array.limit;
      
      console.log(post_ranking_list_by_daily_data);
      console.log('start_order_daily_period : '+start_order_daily_period +', new_start_order_daily_period : '+ result_array.new_start_order_daily_period);
      // $("#post_ranking_list_item_daily_period").empty();
      $("#loading_daily").remove();
      for(i=0; i < limit; i++) {
        var index = Number(start_order_daily_period-1) + i;
        // index가 배열의 길와 같은 경우 반복문을 종료한다.
        if(index == post_ranking_list_by_daily_data.length){
          break;
        }
        var post = post_ranking_list_by_daily_data[index];
        var ranking = index+1;
        $("#post_ranking_list_item_daily_period").append(
          '<a href="/postmanage/detail/'+ post.slug +'?page=&filter=&search=" class="list-group-item list-group-item-action list-group-item-light py-1">'+
            '<div class="row">'+
              '<div class="col-2 text-center">'+
                '<span class="info_text">'+ranking+'.</span>'+
              '</div>'+
              '<div class="col-6 text-ellipsis">'+
                '<span class="info_text">'+post.title+'</span>'+
              '</div>'+
              '<div class="col-4 text-ellipsis">'+
                '<span class="info_text">'+post.main_subject_name+'>'+post.sub_subject_name+'</span>'+
              '</div>'+
            '</div>'+
          '</a>'
        );
      }

      // 포스트 랭킹 데이터의 개수가 limit보다 큰 경우 '더보기' 버튼 요소를 넣는다. 
      if( post_ranking_list_by_daily_data.length > limit ) {
        $("#post_ranking_list_item_daily_period_wrap").append(
          '<div class="d-flex justify-content-center w-100">'+
            '<button type="button" class="btn btn-outline-secondary btn-sm py-0 mt-2 mb-1" mark="daily_period" onclick="add_post_list(this)" id="add_daily_ranking_post"><span class="info_text">더보기</span></button>'+
          '</div>'
        );
      }

      // 데이터를 읽기 시작할 순서를 업데이트 해준다.
      $("#start_order_daily_period").val(result_array.new_start_order_daily_period);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 선택한 기간(월별)내 포스트 순위 데이터를 가져와 표시한다.
function post_ranking_list_monthly_period(){

  var start_order_monthly_period = $("#start_order_monthly_period").val();
  var start_month = $('#start_month').val();
  var start_day = start_month+'-01';
  var end_month = $('#end_month').val();
  var end_day_array = end_month.split('-');
  // 해당월의 마지막 날을 구한다.
  var last  = new Date(end_day_array[0], end_day_array[1],0).getDate(); 
  // last = new Date( last - 1 ); 
  var end_day = end_month+'-'+last;
  console.log('start_order_monthly_period : '+start_order_monthly_period+'start_day : '+start_day+'end_day : '+end_day);

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/getpostrankinglistbymonthly',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      start_order_monthly_period : start_order_monthly_period,
      start_day : start_day,
      end_day : end_day

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      post_ranking_list_by_monthly_data = result_array.post_ranking_list_by_monthly;
      limit = result_array.limit;
      
      console.log(post_ranking_list_by_monthly_data);
      console.log('start_order_monthly_period : '+start_order_monthly_period +', new_start_order_monthly_period : '+ result_array.new_start_order_monthly_period);
      // $("#post_ranking_list_item_daily_period").empty();
      $("#loading_monthly").remove();
      for(i=0; i < limit; i++) {
        var index = Number(start_order_monthly_period-1) + i;
        // index가 배열의 길와 같은 경우 반복문을 종료한다.
        if(index == post_ranking_list_by_monthly_data.length){
          break;
        }
        var post = post_ranking_list_by_monthly_data[index];
        var ranking = index+1;
        $("#post_ranking_list_item_monthly_period").append(
          '<a href="/postmanage/detail/'+ post.slug +'?page=&filter=&search=" class="list-group-item list-group-item-action list-group-item-light py-1">'+
            '<div class="row">'+
              '<div class="col-2 text-center">'+
                '<span class="info_text">'+ranking+'.</span>'+
              '</div>'+
              '<div class="col-6 text-ellipsis">'+
                '<span class="info_text">'+post.title+'</span>'+
              '</div>'+
              '<div class="col-4 text-ellipsis">'+
                '<span class="info_text">'+post.main_subject_name+'>'+post.sub_subject_name+'</span>'+
              '</div>'+
            '</div>'+
          '</a>'
        );
      }
      
      // 포스트 랭킹 데이터의 개수가 limit보다 큰 경우 '더보기' 버튼 요소를 넣는다. 
      if( post_ranking_list_by_monthly_data.length > limit ) {
        $("#post_ranking_list_item_monthly_period_wrap").append(
          '<div class="d-flex justify-content-center w-100">'+
            '<button type="button" class="btn btn-outline-secondary btn-sm py-0 mt-2 mb-1" mark="monthly_period" onclick="add_post_list(this)" id="add_monthly_ranking_post"><span class="info_text">더보기</span></button>'+
          '</div>'
        );
      }

      // 데이터를 읽기 시작할 순서를 업데이트 해준다.
      $("#start_order_monthly_period").val(result_array.new_start_order_monthly_period);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 랭킹 포인트를 업데이트 한다. (페이지 로딩 퍼포먼스를 고려해 사용 보류)
function update_ranking_point(callback) {

  // 로딩중 spinner 요소를 넣어준다.
  $('#post_ranking_list_item_total_wrap').append(
    '<div class="d-flex justify-content-center align-items-center my-5" id="loading_total">'+
      '<div class="spinner-border text-secondary" role="status">'+
        '<span class="visually-hidden">Loading...</span>'+
      '</div>'+
    '</div>'
  );
  
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/main/updaterankingpoint',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      
      console.log(result_array);

      if(result_array != null) {
        console.log(result_array.length+'개 포스트의 랭킹 포인트가 업데이트 되었습니다.');
      }

      if(callback !== undefined) {
        // 업데이트된 랭킹 포인트를 기반으로 순위 데이터를 가져와 표시한다.
        post_ranking_list_total();
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 포스트 순위 리스트 아이템을 추가한다.
function add_post_list(obj){
  var mark = $(obj).attr('mark');
  if(mark == 'total'){
    var start_order_total = $("#start_order_total").val();
    var new_start_order_total = Number(start_order_total)+limit;
    console.log(mark+' start_order_total : '+start_order_total+'\n');
    console.log(post_ranking_list_total_data);
    
    for(i=0; i < limit; i++) {
      var index = Number(start_order_total-1) + i;
      var post = post_ranking_list_total_data[index];
      var ranking = index+1;
      $("#post_ranking_list_item_total").append(
        '<a href="/postmanage/detail/'+ post.slug +'?page=&filter=&search=" class="list-group-item list-group-item-action list-group-item-light py-1">'+
          '<div class="row">'+
            '<div class="col-2 text-center">'+
              '<span class="info_text">'+ranking+'.</span>'+
            '</div>'+
            '<div class="col-6 text-ellipsis">'+
              '<span class="info_text">'+post.title+'</span>'+
            '</div>'+
            '<div class="col-4 text-ellipsis">'+
              '<span class="info_text">'+post.main_subject_name+'>'+post.sub_subject_name+'</span>'+
            '</div>'+
          '</div>'+
        '</a>'
      );
      // index가 배열의 길와 같은 경우 '더보기' 버튼을 제거하고 반복문을 종료한다.
      if((index + 1) == post_ranking_list_total_data.length){       
        $("#add_total_ranking_post").remove();
        break;
      }
    }



    // 다음에 읽기 시작할 데이터 순서를 업데이트 해준다. 
    $("#start_order_total").val(new_start_order_total);

  } else if( mark == 'daily_period') {
    var start_order_daily_period = $("#start_order_daily_period").val();
    var new_start_order_daily_period = Number(start_order_daily_period)+limit;
    console.log(mark+' start_order_daily_period : '+start_order_daily_period+'\n');
    console.log(post_ranking_list_by_daily_data);

    for(i=0; i < limit; i++) {
      var index = Number(start_order_daily_period-1) + i;
      var post = post_ranking_list_by_daily_data[index];
      var ranking = index+1;
      $("#post_ranking_list_item_daily_period").append(
        '<a href="/postmanage/detail/'+ post.slug +'?page=&filter=&search=" class="list-group-item list-group-item-action list-group-item-light py-1">'+
          '<div class="row">'+
            '<div class="col-2 text-center">'+
              '<span class="info_text">'+ranking+'.</span>'+
            '</div>'+
            '<div class="col-6 text-ellipsis">'+
              '<span class="info_text">'+post.title+'</span>'+
            '</div>'+
            '<div class="col-4 text-ellipsis">'+
              '<span class="info_text">'+post.main_subject_name+'>'+post.sub_subject_name+'</span>'+
            '</div>'+
          '</div>'+
        '</a>'
      );
      // index가 배열의 길와 같은 경우 반복문을 종료한다.
      if((index + 1)== post_ranking_list_by_daily_data.length){
        $("#add_daily_ranking_post").remove();
        break;
      }
    }
    
    // 다음에 읽기 시작할 데이터 순서를 업데이트 해준다. 
    $("#start_order_daily_period").val(new_start_order_daily_period);

  } else { // 월별 통계 요소일 경우
    var start_order_monthly_period = $("#start_order_monthly_period").val();
    var new_start_order_monthly_period = Number(start_order_monthly_period)+limit;
    console.log(mark+' start_order_monthly_period : '+start_order_monthly_period+'\n');
    console.log(post_ranking_list_by_monthly_data);
   
    for(i=0; i < limit; i++) {
      var index = Number(start_order_monthly_period-1) + i;
      var post = post_ranking_list_by_monthly_data[index];
      var ranking = index+1;
      $("#post_ranking_list_item_monthly_period").append(
        '<a href="/postmanage/detail/'+ post.slug +'?page=&filter=&search=" class="list-group-item list-group-item-action list-group-item-light py-1">'+
          '<div class="row">'+
            '<div class="col-2 text-center">'+
              '<span class="info_text">'+ranking+'.</span>'+
            '</div>'+
            '<div class="col-6 text-ellipsis">'+
              '<span class="info_text">'+post.title+'</span>'+
            '</div>'+
            '<div class="col-4 text-ellipsis">'+
              '<span class="info_text">'+post.main_subject_name+'>'+post.sub_subject_name+'</span>'+
            '</div>'+
          '</div>'+
        '</a>'
      );
      // index가 배열의 길이와 같은 경우 반복문을 종료한다.
      if((index + 1) == post_ranking_list_by_monthly_data.length){
        $("#add_monthly_ranking_post").remove();
        break;
      }
    }

    // 다음에 읽기 시작할 데이터 순서를 업데이트 해준다. 
    $("#start_order_monthly_period").val(new_start_order_monthly_period);
  }
}

// 모달 초기화 및 객체 반환
function modal_init(id) {
  var myModal = new bootstrap.Modal(document.getElementById(id), {
    keyboard: false
  });
  // var myInput = document.getElementById('myInput')
  var myModalEl = document.getElementById(id);
  myModalEl.addEventListener('show.bs.modal', function (event) {  // 모달을 띄울 때 후 실행
    console.log('모달을 띄웁니다.');
    // myInput.focus()
  });

  myModalEl.addEventListener('shown.bs.modal', function (event) {  // 모달을 띄운 후 실행
    console.log('모달을 띄웠습니다.');
    // myInput.focus()
  });

  myModalEl.addEventListener('hide.bs.modal', function (event) {  // 모달을 닫을 때 실행
    console.log('모달을 닫습니다.');
    // myInput.focus()
  });
  
  myModalEl.addEventListener('hidden.bs.modal', function (event) {  // 모달을 닫은 후 실행
    console.log('모달을 닫았습니다.');
    // myInput.focus()
  });

  myModalEl.addEventListener('hidePrevented.bs.modal', function (event) {  // backdrop 옵션이 'static'이거나 keyboard 옵션이 'false' 일때 backdrop 클릭 또는 esc 입력 시 실행
    console.log("모달을 닫으려면 '닫기' 버튼을 누르세요.");
    // myInput.focus()
  });

  return myModal;
}

// popovers 초기화 
function popovers_init() {
  var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
  var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    var el_id = popoverTriggerEl.id;
    // popoverTriggerEl에 이벤트를 추가한다.
    var el_instance = document.getElementById(el_id); // 개별 popoverTrigger요소의 인스턴스를 생성한다.
    el_instance.addEventListener('show.bs.popover', function () { // popover를 나타내는 메서드가 호출될때 실행
      console.log(el_id+'의 popover가 열립니다.');
    });

    el_instance.addEventListener('inserted.bs.popover', function () { // popover를 나타내기 위한 요소가 DOM에 추가됐을때 실행
      console.log(el_id+'의 popover 요소가 추가되었습니다.');
    });

    el_instance.addEventListener('shown.bs.popover', function () { // popover가 나타난 이후 실행
      console.log(el_id+'의 popover가 열렸습니다.');
    });

    el_instance.addEventListener('hide.bs.popover', function () { // popover가 사라지기 시작할 때 실행
      console.log(el_id+'의 popover가 닫힙니다.');
    });

    el_instance.addEventListener('hidden.bs.popover', function () { // popover가 사라진 후 이후 실행
      console.log(el_id+'의 popover가 닫혔습니다.');
    });
    
    var popover = new bootstrap.Popover(popoverTriggerEl, {
      // template : '<div class="popover" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-header"></h3><div class="popover-body info_text"></div></div>',
      customClass : 'info_text',  // 로딩된 *.css 파일의 클래스 이름을 추가하면 popover 요소 전체에 해당 스타일이 적용된다.
      popperConfig: function (defaultBsPopperConfig) {
        var newPopperConfig = {
          // popper 설정값을 넣는다.
        }
        // use defaultBsPopperConfig if needed...
        // console.log(defaultBsPopperConfig);
        return newPopperConfig;
      }
    });
    return popover;
  });

  console.log(popoverList);
}

// viewer 객체 생성
function viewer_init() {
  var viewer_item_arr = document.querySelectorAll('.img-fluid');
  console.log(viewer_item_arr);
  if(viewer_item_arr.length > 0){ // 콘텐츠 내에 이미지 요소가 있는 경우
    // jquery
    // $.each(viewer_item_arr,function(index,item){
    //   $(item).closest('div').addClass('viewer-item');
    // });
    // pure javascript
    viewer_item_arr.forEach(function(item){
      var parent = item.parentNode;
      parent.classList.add('viewer-item');
    });
    var viewer_items = document.querySelector('.viewer-items');
    viewer = new Viewer(viewer_items, {
      title : 0,           // 이미지 제목(파일명) 표시하기 않도록 설정.
      backdrop : 'static', // 모달 배경 영역을 클릭해도 viewer가 종료 되지 않도록 설정.
      // movable : false,
      toolbar: {
        zoomIn: 0,
        zoomOut: 0,
        oneToOne: 0,
        reset: {
          show: 1,
          size: 'large',
        },
        prev: {
          show: 1,
          size: 'large',
        },
        play: {
          show: 1,
          size: 'large',
        },
        next: {
          show: 1,
          size: 'large',
        },
        rotateLeft: 0,
        rotateRight: 0,
        flipHorizontal: 0,
        flipVertical: 0      
      },
      // backdrop : false
      view(event) {
        console.log(event.detail.index);
      },
      viewed(event) {
        console.log('이미지를 로딩했습니다.');
      },
    });
    console.log('viewer가 생성되었습니다.');
  }
}