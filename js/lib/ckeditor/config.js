/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'ko';
	// config.uiColor = '#AADC6E';
	config.enterMode = CKEDITOR.ENTER_BR; // enter 시 <br> 태그 추가
	config.fillEmptyBlocks = false; // 비어있는 <p> 태그에 공백문자 추가하지 않음
	CKEDITOR.dtd.$removeEmpty['i'] = false; // 비어있는 <i> 태그 삭제하지 않음
	config.height = 350;
	config.resize_enabled = false; // 에디터 사이즈 변경 허용 안함
	config.toolbarCanCollapse = true; 
	config.font_names = '맑은 고딕/Malgun Gothic;굴림/Gulim;돋움/Dotum;바탕/Batang;궁서/Gungsuh;' + config.font_names;
	config.filebrowserBrowseUrl = '/js/lib/ckfinder/ckfinder.html';
	config.filebrowserUploadUrl = '/js/lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	//툴바 커스텀 설정값
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Blockquote,BidiLtr,BidiRtl,Language,Flash,Outdent,Indent,PageBreak,Anchor';
	
	// youtube 영상 삽입
	config.extraPlugins = 'youtube';
	config.youtube_width = '640'; // 기본 넓이 설정
	config.youtube_height = '480'; // 기본 높이 설정
	config.youtube_responsive = true;
	config.youtube_related = true;
	config.youtube_older = false;
	config.youtube_privacy = false;
	config.youtube_autoplay = false;
	config.youtube_controls = true;
	config.youtube_disabled_fields = ['txtEmbed', 'chkAutoplay'];

};
