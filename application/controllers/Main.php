<?php defined('BASEPATH') OR exit('No direct script access allowed');

//컨트롤러의 파일생성은 기본적으로 주소창의 주소 확장입니다.
//간단하게 말해 Main.php를 만들고 class를 설정하였다면 "URL/index.php/main"으로 접속 가능합니다.
//내부 function도 주소 확장입니다. "URL/index.php/main"로 접속하였다면 function index()가 기본적으로 실행됩니다.
//내부에 public function good() 함수를 추가하였다면 "URL/index.php/main/good"으로 실행됩니다.

class Main extends CI_Controller {

	function __construct() {       
      parent::__construct();
      $this->load->model('Main_model', 'main');
      $this->load->helper(array('form', 'url','alert','text'));
      $this->load->library(array('form_validation', 'upload'));

    }

    //index 함수 설정은 "URL/index.php/main" 또는 "URL/index.php/main/index"로 접속가능하게 함
	public function index() {
      // $this->load->model 명령어는 /application/models 폴더를 탐색한다고 이해하셔도 됩니다.
      // model('Main_model')에서 Main_model은 Main_model.php 파일을 찾겠다는 겁니다.
      // $this->Main_model->test();는 로드된 Main_model.php에서 test 함수를 실행하겠다는 뜻입니다.
	  // $this->load->model('Main_model');
	  // $data['admin'] = $this->Main_model->test();
    
    $data['title'] = 'cheoleeblog login';
    $this->load->view('login_form',$data);

  }

  // 입력 데이터 유효성 검증 및 로그인 시도
  public function login_validation() {
    $home = $_SERVER['HTTP_HOST']; // 사이트 도메인을 변수에 담아준다.
    $this->load->library('form_validation');
    
    $this->form_validation->set_rules('admin_id','아이디','required'); // 필드명, 이용자가 읽기쉬운 이름, 검사규칙
    $this->form_validation->set_rules('passwd','비밀번호','required');

    if($this->form_validation->run()){ //입력 데이터 유효성 검사 통과
      $admin_id = $this->input->post('admin_id', TRUE);
      $passwd = $this->input->post('passwd', TRUE);
      
      //model function
      $this->load->model('Main_model');
      if($this->Main_model->login($admin_id,$passwd)){ // 로그인 성공
        $session_data = array(
          'admin_id'=> $admin_id
        );
        $this->session->set_userdata($session_data);
        
        $last_login_date = date('Y-m-d H:i:s',time());
        $table = 'admin';
        $data = array(
          'last_login_date' => $last_login_date
        );
        
        // 마지막 로그인 일자를 업데이트 해준다.
        $this->Main_model->update_last_login_date($table,$data,$session_data);

        // 로그인 횟수를 업데이트 해준다.
        $this->Main_model->update_login_count($table,$session_data);

        alert_move('로그인 되었습니다.','/main/enter');
        // redirect('/main/enter'); //enter 함수로 이동
      } else { // 로그인 실패
        // $this->session->set_flashdata('error','로그인에 실패했습니다');
        
        alert_move('로그인 정보를 정확히 입력해주세요.','/main/index');
        
        redirect('/main/index');
      }

    } else { //입력 데이터 유효성 검사 실패
      // $this->session->set_flashdata('validation_error','아이디와 비밀번호를 입력해주세요');
      alert_move('아이디와 비밀번호를 입력해주세요.','/main/index');
      $this->index(); //로그인 화면으로 이동
    }
  }

  public function enter() { //로그인 성공시
    if($this->session->userdata('admin_id') != '') {
      
      $main_subject = $this->main->get_all_main_subject(); // 메인 주제 데이터를 가져온다.
      $sub_subject = $this->main->get_all_sub_subject_data(); // 하위 주제 데이터를 가져온다.
      $data = array(
        'main_subject'   => $main_subject,
        'sub_subject'    => $sub_subject
      );
      
      $this->load->view('dashboard', $data);

    } else {
      redirect('/main/index'); //로그인 화면으로 이동
    }
  }

  public function logout() {
    $this->session->unset_userdata('admin_id');
    alert_move('로그아웃 되었습니다.','/main/index');
    // redirect('/main/index'); //로그인 화면으로 이동
  }

  // 주재별 색상 정보를 가져온다.
  public function getSubjectColor() {
    $main_subject_colors = '';
    $sub_subject_colors = '';
    
    $main_subject_data =  $this->db->query("Select main_subject_name, color From main_subject")->result_array();
    for ( $i=0; $i < count($main_subject_data); $i++){
      $main_subject = $main_subject_data[$i];
      $main_subject_colors[] = array(
        $main_subject['main_subject_name'] => $main_subject['color'] , // key : value
      );
    }
    $sub_subject_data = $this->db->query("Select sub_subject_name, color From sub_subject")->result_array();
    for ( $i=0; $i < count($sub_subject_data); $i++){
      $sub_subject = $sub_subject_data[$i];
      $sub_subject_colors[] = array(
        $sub_subject['sub_subject_name'] => $sub_subject['color'], // key : value
      );
    }

    $result_array = array(
      'main_subject_colors' => $main_subject_colors,
      'sub_subject_colors' => $sub_subject_colors
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 일별 현황 요소 로드
  public function dailyStatics(){
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');

    $main_subject = $this->main->get_all_main_subject(); // 메인 주제 데이터를 가져온다.

    $data = array(
      'main_subject'  => $main_subject,
      'start_day'     => $start_day,
      'end_day'       => $end_day
    );

    $this->load->view('daily_statics',$data);
  }

  // 월별 현황 요소 로드
  public function monthlyStatics(){
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');

    $main_subject = $this->main->get_all_main_subject(); // 메인 주제 데이터를 가져온다.

    $data = array(
      'main_subject'  => $main_subject,
      'start_day'     => $start_day,
      'end_day'       => $end_day
    );

    $this->load->view('monthly_statics',$data);
  }

  // 일별 현황 통계 데이터 선택
  public function getDailyStatics(){
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');
    
    // 각 배열 변수 초기화
    $yoil = array('일','월','화','수','목','금','토');
    $each_day_yoil = array(); // 요일 표시 추가된 날짜 배열 초기화
    $each_day = array($start_day); // 설정된 기간내 각 날짜를 담을 배열을 초기화 한다. category
    $each_day_total_post = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_day_total_comment = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_day_total_comment_reply = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $series_name_arr = array(); // seriesName 배열을 초기화 한다. seriese-name
    $post_statics_data = array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_statics_data = array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_reply_statics_data = array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data
    
    try {
      $start = new DateTime($start_day);
      $end = new DateTime($end_day);
    } catch (Exception $e){
      echo json_encode(array('code' => 300, 'message' => $e->getMessage(), 'extra' => null, 'debug' => null)); exit;
    }

    $interval = $start->diff($end); //기간의 시간 차이를 계산
    
    $diff_day = $interval->days;  //기간 차이를 일수로 환산
    // echo json_encode(array('code' => 400, 'message' => $diff_day, 'extra' => null, 'debug' => null)); exit;
  
    // 기간 사이의 각 날짜를 배열에 넣는다.
    for($i=1; $i < $diff_day+1; $i++){
      $timestamp = strtotime($start_day.'+'.$i.' days');
      $each_day[] = date("Y-m-d", $timestamp);
    }

    //각 날짜의 요일을 구해 넣는다. category
    for($i=0; $i < count($each_day); $i++){
      $day = $each_day[$i];
      $weekday = $yoil[date('w',strtotime($day))]; // 요일을 구한다.
      $day_yoil = $day.'('.$weekday.')'; // 날짜와 요일을 병합한다.
      $each_day_yoil[] = $day_yoil;
    }
   
    // 각 주제의 이름을 배열에 담는다. series-name
    $main_subject = $this->main->get_all_main_subject(); // 메인 주제 데이터를 가져온다.
    for($i=0;$i < count($main_subject);$i++){
      $series_name_arr[] = $main_subject[$i]['main_subject_name']; //seriesName 배열에 담는다. series-name
    }
    
    // 각 주제별 기간내 통계 데이터를 담는다. series-data
    for($i=0;$i < count($series_name_arr); $i++){
      $series_name = $series_name_arr[$i];
      $post_statics = array(); // 주제별 기간내 통계 데이터 배열을 초기화 한다.
      $comment_statics = array();
      $comment_reply_statics = array();

      for($j=0; $j < count($each_day); $j++){
        $day_temp = $each_day[$j];
        $post_statics[] = $this->main->count_daily_subject_post($series_name,$day_temp); // 각 날짜의 포스트 등록 개수를 가져온다.
        $comment_statics[] = $this->main->count_daily_subject_comment($series_name,$day_temp); // 각 날짜의 댓글 등록 개수를 가져온다.
        $comment_reply_statics[] = $this->main->count_daily_subject_comment_reply($series_name,$day_temp); // 각 날짜의 대댓글 등록 개수를 가져온다.
        
      }
      
      $post_statics_data[] =  $post_statics; // 날짜별 현황 데이터를 주제별 포스트 등록 현황 배열에 담는다.
      $comment_statics_data[] = $comment_statics; // 날짜별 현황 데이터를 주제별 댓글 등록 현황 배열에 담는다.
      $comment_reply_statics_data[] = $comment_reply_statics; // 날짜별 현황 데이터를 주제별 대댓글 등록 현황 배열에 담는다.
    }

    // 각 주제별 기간내 통계의 합계 데이터를 담는다. series-data
    for($i=0; $i < count($each_day); $i++) {
      $day_temp = $each_day[$i];
      $each_day_total_post[] = $this->main->count_daily_post($day_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_day_total_comment[] = $this->main->count_daily_comment($day_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_day_total_comment_reply[] = $this->main->count_daily_comment_reply($day_temp); // 각 날짜별 통계의 합을 배열에 담는다.
    }

    // echo json_encode(array('code' => 500, 'message' => $each_day, 'extra' => null, 'debug' => null)); exit;
    $result_array = array(
      'each_day'                      => $each_day_yoil,
      'series_name_arr'               => $series_name_arr,
      'post_statics_data'             => $post_statics_data,
      'comment_statics_data'          => $comment_statics_data,
      'comment_reply_statics_data'    => $comment_reply_statics_data,
      'each_day_total_post'           => $each_day_total_post,
      'each_day_total_comment'        => $each_day_total_comment,
      'each_day_total_comment_reply'  => $each_day_total_comment_reply
    );
    // echo json_encode(array('code' => 500, 'message' => $each_day_yoil, 'extra' => $result_array, 'debug' => null)); exit;

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 월별 현황 통계 데이터 선택
  public function getMonthlyStatics(){
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');
    $start_month = date('Y-m',strtotime($start_day));
    // 각 배열 변수 초기화
    $each_month_k = array(); // 년, 월 한글 표시 추가된 날짜 배열 초기화
    $each_month = array($start_month); // 설정된 기간내 각 날짜를 담을 배열을 초기화 한다. category
    $each_month_total_post = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_month_total_comment = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_month_total_comment_reply = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $series_name_arr = array(); // seriesName 배열을 초기화 한다. seriese-name
    $post_statics_data = array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_statics_data = array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_reply_statics_data = array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data
    
    try {
      $start = new DateTime($start_day);
      $end = new DateTime($end_day);
    } catch (Exception $e){
      echo json_encode(array('code' => 300, 'message' => $e->getMessage(), 'extra' => null, 'debug' => null)); exit;
    }
    
    $interval = $start->diff($end); //기간의 시간 차이를 계산
    
    // $diff_day = $interval->day;  //기간 차이를 일수로 환산
    $diff_month = ($interval->format('%y') * 12) + $interval->format('%m');  //기간 차이를 개월수로 환산
  
    // echo json_encode(array('code' => 400, 'message' => $diff_month, 'extra' => null, 'debug' => null)); exit;
    // 기간 사이의 각 날짜를 배열에 넣는다.
    for($i=1; $i < $diff_month; $i++){
      $timestamp = strtotime($start_day.'+'.$i.' months');
      $each_month[] = date("Y-m", $timestamp);
    }
    
    //각 날짜를 한글 표기를 추가해 넣는다. category
    for($i=0; $i < count($each_month); $i++){
      $month = $each_month[$i];
      $month_array = explode('-',$month);
      $month_k = $month_array[0].'년'.$month_array[1].'월'; // 날짜와 요일을 병합한다.
      $each_month_k[] = $month_k;
    }
   
    // 각 주제의 이름을 배열에 담는다. series-name
    $main_subject = $this->main->get_all_main_subject(); // 메인 주제 데이터를 가져온다.
    for($i=0;$i < count($main_subject);$i++){
      $series_name_arr[] = $main_subject[$i]['main_subject_name']; //seriesName 배열에 담는다. series-name
    }
    
    // 각 주제별 기간내 통계 데이터를 담는다. series-data
    for($i=0;$i < count($series_name_arr); $i++){
      $series_name = $series_name_arr[$i];
      $post_statics = array(); // 주제별 기간내 통계 데이터 배열을 초기화 한다.
      $comment_statics = array();
      $comment_reply_statics = array();

      for($j=0; $j < count($each_month); $j++){
        $month_temp = $each_month[$j];
        $post_statics[] = $this->main->count_monthly_subject_post($series_name,$month_temp); // 각 월의 포스트 등록 개수를 가져온다.
        $comment_statics[] = $this->main->count_monthly_subject_comment($series_name,$month_temp); // 각 월의 댓글 등록 개수를 가져온다.
        $comment_reply_statics[] = $this->main->count_monthly_subject_comment_reply($series_name,$month_temp); // 각 날짜의 대댓글 개수를 가져온다.
        
      }
      
      $post_statics_data[] =  $post_statics; // 날짜별 현황 데이터를 주제별 포스트 등록 현황 배열에 담는다.
      $comment_statics_data[] = $comment_statics; // 날짜별 현황 데이터를 주제별 댓글 등록 현황 배열에 담는다.
      $comment_reply_statics_data[] = $comment_reply_statics; // 날짜별 현황 데이터를 주제별 대댓글 등록 현황 배열에 담는다.
    }
    
    // 각 주제별 기간내 통계의 합계 데이터를 담는다. series-data
    for($i=0;$i < count($each_month); $i++) {
      $month_temp = $each_month[$i];
      $each_month_total_post[] = $this->main->count_monthly_post($month_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_month_total_comment[] = $this->main->count_monthly_comment($month_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_month_total_comment_reply[] = $this->main->count_monthly_comment_reply($month_temp); // 각 날짜별 통계의 합을 배열에 담는다.
    }
    
    $result_array = array(
      'each_month'                      => $each_month_k,
      'series_name_arr'                 => $series_name_arr,
      'post_statics_data'               => $post_statics_data,
      'comment_statics_data'            => $comment_statics_data,
      'comment_reply_statics_data'      => $comment_reply_statics_data,
      'each_month_total_post'           => $each_month_total_post,
      'each_month_total_comment'        => $each_month_total_comment,
      'each_month_total_comment_reply'  => $each_month_total_comment_reply
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 선택한 메인 주제의 일별 현황 통계 데이터 선택
  public function getDailyStaticsByMainSubject(){
    $subject_name = $this->input->post_get('subject_name');
    $subject_code = $this->input->post_get('subject_code');
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');
    
    // 각 배열 변수 초기화
    $yoil = array('일','월','화','수','목','금','토');
    $each_day_yoil = array(); // 요일 표시 추가된 날짜 배열 초기화
    $each_day = array($start_day); // 설정된 기간내 각 날짜를 담을 배열을 초기화 한다. category
    $each_day_total_post = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_day_total_comment = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_day_total_comment_reply = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $series_name_arr = array(); // seriesName 배열을 초기화 한다. seriese-name
    $post_statics_data = array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_statics_data = array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_reply_statics_data = array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data

    try {
      $start = new DateTime($start_day);
      $end = new DateTime($end_day);
    } catch (Exception $e){
      echo json_encode(array('code' => 300, 'message' => $e->getMessage(), 'extra' => null, 'debug' => null)); exit;
    }

    $interval = $start->diff($end); //기간의 시간 차이를 계산
     
    $diff_day = $interval->days;  //기간 차이를 일수로 환산
  
    // echo json_encode(array('code' => 400, 'message' => $diff_day, 'extra' => null, 'debug' => null)); exit;
    
    // 기간 사이의 각 날짜를 배열에 넣는다.
    for($i=1; $i < $diff_day+1; $i++){
      $timestamp = strtotime($start_day.'+'.$i.' days');
      $each_day[] = date("Y-m-d", $timestamp);
    }

    //각 날짜의 요일을 구해 넣는다. category
    for($i=0; $i < count($each_day); $i++){
      $day = $each_day[$i];
      $weekday = $yoil[date('w',strtotime($day))]; // 요일을 구한다.
      $day_yoil = $day.'('.$weekday.')'; // 날짜와 요일을 병합한다.
      $each_day_yoil[] = $day_yoil;
    }
    
    // subject_title 하위 주제의 이름을 배열에 담는다. series-name
    $sub_subject = $this->main->get_all_sub_subject($subject_code); // 메인 주제 데이터를 가져온다.
    for($i=0;$i < count($sub_subject);$i++){
      $series_name_arr[] = $sub_subject[$i]['sub_subject_name']; //seriesName 배열에 담는다. series-name
    }
   
    // 각 주제별 기간내 통계 데이터를 담는다. series-data
    for($i=0;$i < count($series_name_arr); $i++){
      $series_name = $series_name_arr[$i];
      $post_statics = array(); // 주제별 기간내 통계 데이터 배열을 초기화 한다.
      $comment_statics = array();
      $comment_reply_statics = array();

      for($j=0; $j < count($each_day); $j++){
        $day_temp = $each_day[$j];
        $post_statics[] = $this->main->count_daily_sub_subject_post($subject_name,$series_name,$day_temp); // 각 날짜의 포스트 등록 개수를 가져온다.
        $comment_statics[] = $this->main->count_daily_sub_subject_comment($subject_name,$series_name,$day_temp); // 각 날짜의 포스트 등록 개수를 가져온다.
        $comment_reply_statics[] = $this->main->count_daily_sub_subject_comment_reply($subject_name,$series_name,$day_temp); // 각 날짜의 포스트 등록 개수를 가져온다.
        
      }
      
      $post_statics_data[] =  $post_statics; // 날짜별 현황 데이터를 주제별 포스트 등록 현황 배열에 담는다.
      $comment_statics_data[] = $comment_statics; // 날짜별 현황 데이터를 주제별 댓글 등록 현황 배열에 담는다.
      $comment_reply_statics_data[] = $comment_reply_statics; // 날짜별 현황 데이터를 주제별 대댓글 등록 현황 배열에 담는다.
    }
    
    // 각 주제별 기간내 통계의 합계 데이터를 담는다. series-data
    for($i=0;$i < count($each_day); $i++) {
      $day_temp = $each_day[$i];
      $each_day_total_post[] = $this->main->count_daily_sub_post($subject_name,$day_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_day_total_comment[] = $this->main->count_daily_sub_comment($subject_name,$day_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_day_total_comment_reply[] = $this->main->count_daily_sub_comment_reply($subject_name,$day_temp); // 각 날짜별 통계의 합을 배열에 담는다.
    }
    // echo json_encode(array('code' => 500, 'message' => '살살해', 'extra' => null, 'debug' => null)); exit;
    $result_array = array(
      'each_day'                      => $each_day_yoil,
      'series_name_arr'               => $series_name_arr,
      'post_statics_data'             => $post_statics_data,
      'comment_statics_data'          => $comment_statics_data,
      'comment_reply_statics_data'    => $comment_reply_statics_data,
      'each_day_total_post'           => $each_day_total_post,
      'each_day_total_comment'        => $each_day_total_comment,
      'each_day_total_comment_reply'  => $each_day_total_comment_reply
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 선택한 메인 주제의 월별 현황 통계 데이터 선택
  public function getMonthlyStaticsByMainSubject(){
    $subject_name = $this->input->post_get('subject_name');
    $subject_code = $this->input->post_get('subject_code');
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');
    $start_month = date('Y-m',strtotime($start_day));

    // 각 배열 변수 초기화
    $each_month_k = array(); // 년, 월 한글 표시 추가된 날짜 배열 초기화
    $each_month = array($start_month); // 설정된 기간내 각 날짜를 담을 배열을 초기화 한다. category
    $each_month_total_post = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_month_total_comment = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $each_month_total_comment_reply = array(); // 설정된 기간내 각 날짜 통계 합계를 담을 배열을 초기화 한다.
    $series_name_arr = array(); // seriesName 배열을 초기화 한다. seriese-name
    $post_statics_data = array(); // 각 주제별 포스트 등록 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_statics_data = array(); // 포스트 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data
    $comment_reply_statics_data = array(); // 포스트 댓글에 대한 댓글 현황 데이터를 담을 배열을 초기화 한다. series-data


    try {
      $start = new DateTime($start_day);
      $end = new DateTime($end_day);
    } catch (Exception $e){
      echo json_encode(array('code' => 300, 'message' => $e->getMessage(), 'extra' => null, 'debug' => null)); exit;
    }

    $interval = $start->diff($end); //기간의 시간 차이를 계산
    
    // $diff_day = $interval->days;  //기간 차이를 일수로 환산
    $diff_month = ($interval->format('%y') * 12) + $interval->format('%m');  //기간 차이를 개월수로 환산
  
    // 기간 사이의 각 날짜를 배열에 넣는다.
    for($i=1; $i < $diff_month; $i++){
      $timestamp = strtotime($start_day.'+'.$i.' months');
      $each_month[] = date("Y-m", $timestamp);
    }
    
    //각 날짜를 한글 표기를 추가해 넣는다. category
    for($i=0; $i < count($each_month); $i++){
      $month = $each_month[$i];
      $month_array = explode('-',$month);
      $month_k = $month_array[0].'년'.$month_array[1].'월'; // 날짜와 요일을 병합한다.
      $each_month_k[] = $month_k;
    }
    
    // subject_title 하위 주제의 이름을 배열에 담는다. series-name
    $sub_subject = $this->main->get_all_sub_subject($subject_code); // 메인 주제 데이터를 가져온다.
    for($i=0;$i < count($sub_subject);$i++){
      $series_name_arr[] = $sub_subject[$i]['sub_subject_name']; //seriesName 배열에 담는다. series-name
    }
    
    // 각 주제별 기간내 통계 데이터를 담는다. series-data
    for($i=0;$i < count($series_name_arr); $i++){
      $series_name = $series_name_arr[$i];
      $post_statics = array(); // 주제별 기간내 통계 데이터 배열을 초기화 한다.
      $comment_statics = array();
      $comment_reply_statics = array();

      for($j=0; $j < count($each_month); $j++){
        $month_temp = $each_month[$j];
        $post_statics[] = $this->main->count_monthly_sub_subject_post($subject_name,$series_name,$month_temp); // 각 월의 포스트 등록 개수를 가져온다.
        $comment_statics[] = $this->main->count_monthly_sub_subject_comment($subject_name,$series_name,$month_temp); // 각 월의 포스트 등록 개수를 가져온다.
        $comment_reply_statics[] = $this->main->count_monthly_sub_subject_comment_reply($subject_name,$series_name,$month_temp); // 각 월의 포스트 등록 개수를 가져온다.
        
      }
      
      $post_statics_data[] =  $post_statics; // 날짜별 현황 데이터를 주제별 포스트 등록 현황 배열에 담는다.
      $comment_statics_data[] = $comment_statics; // 날짜별 현황 데이터를 주제별 댓글 등록 현황 배열에 담는다.
      $comment_reply_statics_data[] = $comment_reply_statics; // 날짜별 현황 데이터를 주제별 대댓글 등록 현황 배열에 담는다.
    }

    // 각 주제별 기간내 통계의 합계 데이터를 담는다. series-data
    for($i=0;$i < count($each_month); $i++) {
      $month_temp = $each_month[$i];
      $each_month_total_post[] = $this->main->count_monthly_sub_post($subject_name,$month_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_month_total_comment[] = $this->main->count_monthly_sub_comment($subject_name,$month_temp); // 각 날짜별 통계의 합을 배열에 담는다.
      $each_month_total_comment_reply[] = $this->main->count_monthly_sub_comment_reply($subject_name,$month_temp); // 각 날짜별 통계의 합을 배열에 담는다.
    }
    // echo json_encode(array('code' => 500, 'message' => '살살해', 'extra' => null, 'debug' => null)); exit;
    $result_array = array(
      'each_month'                      => $each_month_k,
      'series_name_arr'                 => $series_name_arr,
      'post_statics_data'               => $post_statics_data,
      'comment_statics_data'            => $comment_statics_data,
      'comment_reply_statics_data'      => $comment_reply_statics_data,
      'each_month_total_post'           => $each_month_total_post,
      'each_month_total_comment'        => $each_month_total_comment,
      'each_month_total_comment_reply'  => $each_month_total_comment_reply
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 전체 포스트 순위 데이터를 가져온다.
  public function getPostRankingList(){
    $start_order_total = $this->input->post_get('start_order_total');
    $limit = 10;

    $post_ranking_list_total = $this->main->get_post_ranking_data();
    $new_start_order_total = $start_order_total + $limit;

    $result_array = array(
      'post_ranking_list_total'         =>  $post_ranking_list_total,
      'new_start_order_total'           =>  $new_start_order_total,
      'limit'                           =>  $limit
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 선택한 기간(일별)의 포스트 순위 데이터를 가져온다.
  public function getPostRankingListByDaily(){
    
    $start_order_daily_period = $this->input->post_get('start_order_daily_period');
    
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');
    $limit = 10;
    
    $post_ranking_list_by_daily = $this->main->get_post_ranking_data_by_daily($start_day,$end_day);
    $new_start_order_daily_period = $start_order_daily_period + $limit;

    $result_array = array(
      'post_ranking_list_by_daily'        =>  $post_ranking_list_by_daily,
      'new_start_order_daily_period'      =>  $new_start_order_daily_period,
      'limit'                             =>  $limit
    );
    // echo json_encode(array('code' => 500, 'message' => '이찌도 니도와', 'extra' => null, 'debug' => null)); exit;
    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 선택한 기간(월별)의 포스트 순위 데이터를 가져온다.
  public function getPostRankingListByMonthly(){
  
    $start_order_monthly_period = $this->input->post_get('start_order_monthly_period');
    
    $start_day = $this->input->post_get('start_day');
    $end_day = $this->input->post_get('end_day');
    $limit = 10;
    
    $post_ranking_list_by_monthly = $this->main->get_post_ranking_data_by_monthly($start_day,$end_day);
    $new_start_order_monthly_period = $start_order_monthly_period + $limit;

    $result_array = array(
      'post_ranking_list_by_monthly'        =>  $post_ranking_list_by_monthly,
      'new_start_order_monthly_period'      =>  $new_start_order_monthly_period,
      'limit'                             =>  $limit
    );
    // echo json_encode(array('code' => 500, 'message' => '이찌도 니도와', 'extra' => null, 'debug' => null)); exit;
    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 랭킹 포인트를 업데이트 한다.
  public function updateRankingPoint(){
    $posts = $this->main->get_all_post();
    
    $result_array = array();
    // 모든 포스트의 랭킹 포인트를 업데이트 한다.
    for($i=0; $i < count($posts); $i++){
      $post = $posts[$i];
      $slug = $post['slug'];
      $result = '';
      $comments_count = $this->main->count_all_comment_by_slug($slug);
      $view_count = $post['view_count'];
      $recommanded = $post['recommanded'];
      //랭킹 포인트 = 조회수 + (댓글 개수*2) + (추천수*3)
      $ranking_point = $view_count + ($comments_count*2) + ($recommanded*3);

      // 랭킹 포인트를 업데이트 한다.
      $this->db->update('post_content',array('ranking_point' => $ranking_point),array('slug' => $slug));
      $result = array(
        'slug'          => $slug,
        'ranking_point' => $ranking_point
      );

      // 결과 배열에 개별 결과 배열을 담는다.
      $result_array[] = $result;
    }
    
    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));

  }

  

}
