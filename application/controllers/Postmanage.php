<?php defined('BASEPATH') OR exit('No direct script access allowed');

//컨트롤러의 파일생성은 기본적으로 주소창의 주소 확장입니다.
//간단하게 말해 Main.php를 만들고 class를 설정하였다면 "URL/index.php/main"으로 접속 가능합니다.
//내부 function도 주소 확장입니다. "URL/index.php/main"로 접속하였다면 function index()가 기본적으로 실행됩니다.
//내부에 public function good() 함수를 추가하였다면 "URL/index.php/main/good"으로 실행됩니다.

class Postmanage extends CI_Controller {

	function __construct() {       
      parent::__construct();
      $this->load->model('Postmanage_model', 'post');
      $this->load->helper(array('form', 'url','alert','text'));
      $this->load->library(array('form_validation', 'session', 'upload'));
      
    }

	// 선택된 포스트의 내용을 표시한다.
  	public function detail($slug)
	{	
		$filter = $this->input->post_get('filter');
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = $this->input->post_get('search');
		if($search == null) {
			$search = '';
		}
		$cur_page = $this->input->get('page');
		if($cur_page == null) {
			$cur_page = 1;
		}
		// config 정보를 가져온다.
		$config = $this->post->get_config();
		$posts_per_page = $config->posts_per_page; // 페이지 당 포스트 개수

		$postDetail = $this->post->getDetailPost($slug);
		$where = array(
			'main_subject_name' => $postDetail['main_subject_name'],
			'sub_subject_name'  => $postDetail['sub_subject_name']
		);
		// 같은 주제의 포스트 정보를 가져온다.
		$posts = $this->db->get_where('post_content',$where)->result_array();
    	// var_dump($slug);
		// var_dump($postDetail);
		$data = array(
			'posts'			    => $posts,
			'main_subject_name' => $postDetail['main_subject_name'],
			'sub_subject_name'  => $postDetail['sub_subject_name'],
			'foreword'    		=> $postDetail['foreword'],
			'writer_id'    		=> $postDetail['writer_id'],
			'title'				=> $postDetail['title'],
			'content' 			=> $postDetail['content'],
			'slug' 				=> $postDetail['slug'],
			'view_count' 		=> $postDetail['view_count'],
			'reg_date'			=> $postDetail['reg_date'],
			'thumbnail'			=> $postDetail['thumbnail'],
			'filter'			=> $filter,
			'search'			=> $search,
			'cur_page'			=> $cur_page,
			'posts_per_page'	=> $posts_per_page

		);

		$this->load->view('post_detail', $data);
	}

	// 포스트 전체 목록을 표시한다.
	public function manage()
	{	
		if($this->session->userdata('admin_id') != '') { // 세션 정보가 정상일 경우
			$filter = $this->input->post_get('filter');
			if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
				$filter = 'title';
			}
			$search = $this->input->post_get('search');
			if($search == null) {
				$search = '';
			}

			// var_dump($filter.','.$search);
			
			if(!empty($filter) && !empty($search)) {
				$all_post = $this->post->count_search_data($filter,$search); // 모든 검색 결과 포스트의 수
			} else {
				$all_post = $this->post->countAllData(); // 모든 포스트의 수
			}
			
			// config 정보를 가져온다.
			$config = $this->post->get_config();
			$posts_per_page = $config->posts_per_page; // 페이지 당 포스트 개수
			$page_item_count = $config->page_item_count; // 페이지 아이템 표시 개수

			// $page_item_count = 5; // 페이지 아이템 표시 개수
			// $posts_per_page = 5; // 한 페이지에 보여줄 포스트의 수
			$all_page = ceil($all_post/$posts_per_page); // 모든 페이지의 수
			// var_dump($slug);
			// var_dump($postDetail);
			$cur_page = $this->input->get('page');

			if($cur_page == null) {
				$cur_page = 1;
			}
			
			// 가져올 데이터의 시작 번호(index)
			$start_index = ($cur_page-1)*$posts_per_page;

			// var_dump($cur_page.','.$posts_per_page);
			
			$data = array(
				'all_posts'				=> $this->post->get_all_post_where($filter,$search),
				'posts'					=> $this->post->get_page_data($start_index,$posts_per_page,$filter,$search),
				'all_page'				=> $all_page,
				'page_item_count'		=> $page_item_count,
				'cur_page'				=> $cur_page,
				'filter'				=> $filter,
				'search'				=> $search
			);
			// var_dump($data);

			$this->load->view('post_manage', $data);
		} else { // 세션 정보가 없는 경우 로그인 화면으로 이동한다.
			$this->session->set_flashdata('message', '        
				<script>
					$(function(){
						alert("세션이 종료되었습니다. 로그인 후 이용해 주세요.");
					});			
				</script>
			');

			redirect('/main/index'); //로그인 화면으로 이동
		}
	}

	function _resizeImage($file_name)
	{
		// Image resizing config
		$config = array(
			'image_library' => 'GD2',
			'source_image'  => '/img/post_profile/' . $file_name,
			'maintain_ratio' => FALSE,
			'width'         => 600,
			'height'        => 400,
			'new_image'     => '/img/post_profile/resize/' . $file_name
		);

		// load config (built in liblary CI3)
		$this->load->library('image_lib', $config);

		$this->image_lib->initialize($config);
		if (!$this->image_lib->resize()) {
			return false;
		}
		$this->image_lib->clear();
			
	}

	// 선택한 포스트를 편집한다.
	public function edit($slug)
	{	
		$filter = $this->input->post_get('filter');
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = $this->input->post_get('search');
		if($search == null) {
			$search = '';
		}
		$cur_page = $this->input->get('page');
		if($cur_page == null) {
			$cur_page = 1;
		}
		$main_subject_data = $this->post->get_all_main_subject();
		// get data from model
		$postDetail = $this->post->getDetailPost($slug);

		$data = array(
			'main_subject_data' => $main_subject_data,
			'main_subject_name' => $postDetail['main_subject_name'],
			'sub_subject_name'  => $postDetail['sub_subject_name'],
			'foreword'			=> $postDetail['foreword'],
			'title'				=> $postDetail['title'],
			'content' 			=> $postDetail['content'],
			'reg_date'			=> $postDetail['reg_date'],
			'idx'				=> $postDetail['idx'],
			'slug'				=> $postDetail['slug'],
			'thumbnail'			=> $postDetail['thumbnail'],
			'filter'			=> $filter,
			'search'			=> $search,
			'cur_page'			=> $cur_page
		);

		$this->load->view('post_edit', $data);
	}

	// 편집한 포스트 내용을 업데이트 한다.
	public function update()
	{
		// 파라미터 값을 가져온다.
		$filter = $this->input->post_get('filter');
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = $this->input->post_get('search');
		if($search == null) {
			$search = '';
		}
		$cur_page = $this->input->get('page');
		if($cur_page == null) {
			$cur_page = 1;
		}
		/////////////////////
		$main_subject_code = $this->input->post('main_subject'); // 오류 처리용
		$main_subject_name = $this->input->post('main_subject_name');
		$sub_subject_name  = $this->input->post('sub_subject');
		$foreword    	   = $this->input->post('foreword');
		$title 			   = $this->input->post('title');
		$idx			   = $this->input->post('idx');
		$slug			   = $this->input->post('slug');
		$writer_id 		   = $this->input->post('writer_id');
		$contents 		   = $this->input->post('contents');
		$old_image		   = $this->input->post('old_image');
		$last_update_date  = date("Y-m-d H:i:s");

		// 하위 주제 정보를 가져온다.
		$sub_subject = $this->db->get_where('sub_subject',array('sub_subject_name' => $sub_subject_name))->result_array();
		$sub_subject_code = $sub_subject[0]['sub_subject_code'];

		$where = array('idx' => $idx);

		$no_upload_image = array(
			'main_subject_name'		=> $main_subject_name,
			'main_subject_code' 	=> $main_subject_code,
			'sub_subject_name'		=> $sub_subject_name,
			'sub_subject_code'		=> $sub_subject_code,
			'foreword'				=> $foreword,
			'title'					=> $title,
			'content'				=> $contents,
			'last_update_date'		=> $last_update_date,
		);

		$this->form_validation->set_rules('main_subject', 'Main_subject', 'required');
		// $this->form_validation->set_rules('sub_subject', 'Sub_subject_name', 'required');
		$this->form_validation->set_rules('foreword', 'Foreword', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('contents', 'Contents', 'required');

		// validate image
		// 편집내용 업데이트 시에는 썸네일 유효성 검증을 하지 않는다.
		// if (empty($_FILES['thumbnail']['name'])) {
		// 	$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
		// }

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', '        
				<script>
				$(function(){
					alert("누락된 항목이 있습니다.");
				});            
				</script>
			');
			redirect('postmanage/edit/' . $slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
		} else {

			$config['upload_path']		= '/var/data/img/post_profile'; //path folder
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['encrypt_name']		= TRUE;

			$this->upload->initialize($config);

			// if user upload new image
			if (!empty($_FILES['thumbnail']['name'])) {

				if ($this->upload->do_upload('thumbnail')) {
					$img = $this->upload->data();

					//Compress Image
					$this->_resizeImage($img['file_name']);

					$thumbnail = $img['file_name'];

					$data = array(
						'main_subject_name'		=> $main_subject_name,
						'main_subject_code' 	=> $main_subject_code,
						'sub_subject_name'		=> $sub_subject_name,
						'sub_subject_code'		=> $sub_subject_code,
						'foreword'				=> $foreword,
						'title'					=> $title,
						'content'				=> $contents,
						'last_update_date'		=> $last_update_date,
						'thumbnail'   			=> $thumbnail,
					);

					// var_dump($data);

					$this->db->update('post_content', $data, $where);

					// remove old image thumbnail
					$filename = explode(".", $old_image)[0];
					if($filename != 'default') {
						array_map('unlink', glob(FCPATH . "img/post_profile/$filename.*"));
						// array_map('unlink', glob(FCPATH . "img/post_profile/resize/$filename.*"));
					}
					$this->session->set_flashdata('message', '
						<script>
							$(function(){
								alert("포스트 내용이 수정되었습니다.");
							});
						</script>
					');
					redirect('postmanage/detail/' . $slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
				} else {
					// if upload image fail
					echo $this->upload->display_errors();
				}
			} else {
				// var_dump($no_upload_image);
				// if user doesn't upload new image
				$this->db->update('post_content', $no_upload_image, $where);
				
				$this->session->set_flashdata('message', '
					<script>
						$(function(){
							alert("포스트 내용이 수정되었습니다.");
						});
					</script>
				');
				redirect('postmanage/detail/' . $slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
			}
		}
	}

	// 포스트 상태를 업데이트 한다.
	public function statusUpdate() {
		$slug = $this->input->post('slug');
		$status = $this->input->post('status');
	
		// 포스트 상태를 업데이트하고 업데이트된 포스트 정보를 가져온다.
		$this->post->status_update($slug,$status);
		
		$post_info = $this->post->getDetailPost($slug);
	 	
		// 계정 상태 확인을 위해 다시 클라이언트에 보낸다.
		$result_array = array(
		  'post_status' => $post_info['is_activate']
		);
	
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
	  }

	// 포스트를 삭제한다.
	public function delete($idx)
	{
		// 파라미터 값을 가져온다.
		$filter = $this->input->post_get('filter');
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = $this->input->post_get('search');
		if($search == null) {
			$search = '';
		}
		$cur_page = $this->input->get('page');
		if($cur_page == null) {
			$cur_page = 1;
		}
		/////////////////////
		$this->post->deletePost($idx, 'post_content');

		$this->session->set_flashdata('message', '
			<script>
				$(function(){
					alert("포스트가 삭제되었습니다.");
				});
			</script>
		');
		redirect('postmanage/manage'.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
	}

	// 코멘트를 저장한다.
	public function saveComment() {
		$post_slug = $this->input->post('post_slug');
		$main_subject_name = $this->input->post('main_subject_name');
		$sub_subject_name = $this->input->post('sub_subject_name');
		$comment_nickname = $this->input->post('comment_nickname');
		if($comment_nickname == null) { // 닉네임을 입력하지 않으면 '운영자'로 설정
			$comment_nickname = '운영자';
		}
		$comment_passwd = $this->input->post('comment_passwd');
		$post_comment = $this->input->post('post_comment');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$is_secret = $this->input->post('is_secret');
		// echo $post_slug.','.$comment_nickname.','.$comment_passwd.',\\r'.$post_comment;
		// echo json_encode(array('code' => 200, 'message' => $post_slug.','.$comment_nickname.','.$comment_passwd.',\\r'.$post_comment, 'extra' => null, 'debug' => null));
		// 	exit;
		// validate input
		$this->form_validation->set_rules('post_slug', 'Post_slug', 'required');
		// $this->form_validation->set_rules('sub_subject', 'Sub_subject', 'required');
		// $this->form_validation->set_rules('main_subject_name', 'Main_subject_name', 'required');       
		// $this->form_validation->set_rules('comment_nickname', 'Comment_nickname', 'required'); // 운영자 닉네임은 자동 설정된다.
		$this->form_validation->set_rules('comment_passwd', 'Comment_passwd', 'required');
		$this->form_validation->set_rules('post_comment', 'Post_comment', 'required');

		
		if ($this->form_validation->run() == FALSE) {
			echo json_encode(array('code' => 100, 'message' => '누락된 항목이 있습니다.', 'extra' => null, 'debug' => null));
			exit;
			
		} else {
			
			$now = date("Y-m-d H:i:s",time());
			$data = array(
				'post_slug'				=> $post_slug,  //주소창에 붙는 포스트 url						
				'main_subject_name'		=> $main_subject_name,
				'sub_subject_name'		=> $sub_subject_name,
				'comment_nickname'		=> $comment_nickname,
				'post_comment'			=> $post_comment,
				'ip_addr' 				=> $ip_addr,
				'is_secret' 			=> $is_secret,
				'comment_passwd'		=> $comment_passwd,
				'reg_date'				=> $now,
				'is_del'				=> 'N',
				'last_update_date'		=> $now
			);
			// var_dump($data);

			// insert data with model
			$this->post->insert_comment('post_comment', $data);

			// 해당 포스트의 모든 코멘트 정보를 가져온다.
			$comments = $this->post->get_all_comment($post_slug);
			// 추가된 댓글 정보를 가져온다.
			$where = array('reg_date' => $now);
			$result = $this->post->get_comment($where);
			$result_array = $result[0];
			$result_array['count_all_count'] = count($comments); // 코멘트의 개수를 결과 배열에 넣어준다.
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;	
		}
	}

	// 코멘트의 댓글을 저장한다.
	public function saveCommentReply() {

		$org_idx = $this->input->post('org_idx');
		$post_slug = $this->input->post('post_slug');
		$comment_reply_nickname = $this->input->post('comment_reply_nickname');
		if($comment_reply_nickname == null) { // 닉네임을 입력하지 않으면 '운영자'로 설정
			$comment_reply_nickname = '운영자';
		}
		$comment_reply_passwd = $this->input->post('comment_reply_passwd');
		$post_comment_reply = $this->input->post('post_comment_reply');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$comment_reply_is_secret = $this->input->post('comment_reply_is_secret');
		// echo $post_slug.','.$comment_reply_nickname.','.$comment_reply_passwd.',\n'.$post_comment_reply;
		// echo json_encode(array('code' => 300, 'message' => $post_slug.','.$comment_reply_nickname.','.$comment_passwd.',\n'.$comment_reply_passwd, 'extra' => null, 'debug' => null));
		// exit;
		// validate input
		$this->form_validation->set_rules('org_idx', 'Org_idx', 'required');
		$this->form_validation->set_rules('post_slug', 'Post_slug', 'required');

		// $this->form_validation->set_rules('comment_reply_nickname', 'Comment_reply_nickname', 'required'); // 운영자 닉네임은 자동 설정된다.
		$this->form_validation->set_rules('comment_reply_passwd', 'Comment_reply_passwd', 'required');
		$this->form_validation->set_rules('post_comment_reply', 'Post_comment_reply', 'required');

		if ($this->form_validation->run() == FALSE) {
			echo json_encode(array('code' => 100, 'message' => '누락된 항목이 있습니다.', 'extra' => null, 'debug' => null));
			exit;
			
		} else {
			$now = date("Y-m-d H:i:s",time());
			$data = array(
				'org_idx'					=> $org_idx,  // 원본 댓글 idx
				'post_slug'					=> $post_slug, // 원본 댓글 해당 포스트 url 식별번호			
				'comment_reply_nickname'	=> $comment_reply_nickname,
				'post_comment_reply'		=> $post_comment_reply,
				'ip_addr' 					=> $ip_addr,
				'is_secret' 				=> $comment_reply_is_secret,
				'comment_reply_passwd'		=> $comment_reply_passwd,
				'reg_date'					=> $now,
				'is_del'					=> 'N',
				'last_update_date'			=> $now
			);
			// var_dump($data);

			// insert data with model
			$this->post->insert_comment_reply('post_comment_reply', $data);

			// 추가된 댓글 정보를 가져온다.
			$where = array('reg_date' => $now);
			$result = $this->post->get_comment_reply($where);
			$result_array = $result[0];
			// 대댓글 정보를 가져온다.
			$comment_reply_list = $this->post->get_all_comment_reply_by_idx($org_idx);
			//대댓글 개수를 댓글 정보 배열에 추가한다.
			$result_array['comment_reply_count'] = count($comment_reply_list);
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;	
		}
	}

	// 코멘트를 수정한다.
	function updateComment() {
		$idx = $this->input->post('idx');
		$post_comment = $this->input->post('new_comment');
		// $comment_passwd = $this->input->post('new_comment_passwd');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$is_secret = $this->input->post('new_is_secret');
		$last_update_date = date('Y-m-d H:i:s',time());
		// echo json_encode(array('code' => 1, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
		// exit;
		// echo $comment_passwd;

		// 운영자가 댓글을 수정할 땐 비번을 확인하지 않는다.
		// if (strlen((string)$comment_passwd) < 4) { // 비밀번호 자리수가 4개인지 확인한다.
		// 	echo json_encode(array('code' => 1, 'message' => '비밀번호는 4자리 숫자로 입력해주세요.', 'extra' => null, 'debug' => null));
		// 	exit;
			
		// } else {
			
			$data = array(
				'post_comment'		=> $post_comment,
				// 'comment_passwd'	=> $comment_passwd,
				'ip_addr'			=> $ip_addr,
				'is_secret'			=> $is_secret,
				'last_update_date'	=> $last_update_date
			);
			// echo json_encode(array('code' => 2, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
			// exit;
			// 댓글 정보를 업데이트 한다.
			$this->db->update('post_comment',$data,array('idx' => $idx));

			// 업데이트된 댓글 정보를 가져온다.
			$where = array('idx' => $idx);
			$result = $this->post->get_comment($where);
			$result_array = $result[0];
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;
		// }
	}

	// 코멘트의 댓글을 수정한다.
	function updateCommentReply() {
		$idx = $this->input->post('idx');
		$post_comment_reply = $this->input->post('new_comment_reply');
		// $comment_reply_passwd = $this->input->post('new_comment_reply_passwd');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$is_secret = $this->input->post('new_reply_is_secret');
		$last_update_date = date('Y-m-d H:i:s',time());
		// echo json_encode(array('code' => 1, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
		// exit;
		// echo $comment_passwd;

		// 운영자가 댓글을 수정할 땐 비번을 확인하지 않는다.
		// if (strlen((string)$comment_reply_passwd) < 4) { // 비밀번호 자리수가 4개인지 확인한다.
		// 	echo json_encode(array('code' => 1, 'message' => '비밀번호는 4자리 숫자로 입력해주세요.', 'extra' => null, 'debug' => null));
		// 	exit;
			
		// } else {
			
			$data = array(
				'post_comment_reply'		=> $post_comment_reply,
				// 'comment_reply_passwd'		=> $comment_reply_passwd,
				'ip_addr'					=> $ip_addr,
				'is_secret'					=> $is_secret,
				'last_update_date'			=> $last_update_date
			);
			// echo json_encode(array('code' => 2, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
			// exit;
			// 댓글 정보를 업데이트 한다.
			$this->db->update('post_comment_reply',$data,array('idx' => $idx));

			// 업데이트된 댓글 정보를 가져온다.
			$where = array('idx' => $idx);
			$result = $this->post->get_comment_reply($where);
			$result_array = $result[0];
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;
		// }
	}

	// idx에 해당하는 코멘트 정보를 가져온다.
	function getComment() {
		$idx = $this->input->post('idx');
		
		// idx에 해당하는 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// idx에 해당하는 코멘트 정보를 가져온다.
	function getCommentReply() {
		$idx = $this->input->post('idx');
		
		// idx에 해당하는 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment_reply($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// idx에 해당하는 코멘트를 삭제 상태로 변경한다.
	function deleteComment() {
		$idx = $this->input->post('idx');

		$del_date = date('Y-m-d H:i:s',time());

		$data = array(
			'is_del'	=> 'Y',
			'del_date'	=> $del_date
		);

		// 댓글 정보를 업데이트 한다.
		$this->db->update('post_comment',$data,array('idx' => $idx));

		// 업데이트된 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// idx에 해당하는 코멘트를 삭제 상태로 변경한다.
	function deleteCommentReply() {
		$idx = $this->input->post('idx');

		$del_date = date('Y-m-d H:i:s',time());

		$data = array(
			'is_del'	=> 'Y',
			'del_date'	=> $del_date
		);

		// 댓글 정보를 업데이트 한다.
		$this->db->update('post_comment_reply',$data,array('idx' => $idx));

		// 업데이트된 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment_reply($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// 댓글 리스트를 새로고침 한다.
	function commentList($slug) {
		$comments = $this->post->get_all_comment($slug);
		$comments_reply = $this->post->get_all_comment_reply_by_slug($slug);
		$data = array(
			'slug'				=> $slug, // 포스트 url 식별 번호
			'comments'			=> $comments, // 해당 포스트 댓글
			'comments_reply' 	=> $comments_reply // 해당 포스트 댓글에 대한 댓글
		);
		$this->load->view('comment_list',$data);
	}
}
