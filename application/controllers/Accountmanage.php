<?php defined('BASEPATH') OR exit('No direct script access allowed');

//컨트롤러의 파일생성은 기본적으로 주소창의 주소 확장입니다.
//간단하게 말해 Main.php를 만들고 class를 설정하였다면 "URL/index.php/main"으로 접속 가능합니다.
//내부 function도 주소 확장입니다. "URL/index.php/main"로 접속하였다면 function index()가 기본적으로 실행됩니다.
//내부에 public function good() 함수를 추가하였다면 "URL/index.php/main/good"으로 실행됩니다.

class Accountmanage extends CI_Controller {

	function __construct() {       
      parent::__construct();
      $this->load->model('Accountmanage_model', 'account');
      $this->load->helper(array('form', 'url','alert','text'));
      $this->load->library(array('form_validation', 'session', 'upload'));
      
    }
  /**
     * 클라이언트 웹페이지의 jQuery $.ajax() 함수를 위한 반환용 JSON 코드를 생성한다.
     *
     * @param int    $code    결과 코드(기본값: 0)
     * @param string $message 결과 메시지(기본값: null)
     * @param mixed  $extra   추가로 전달할 데이터(기본값: null)
     * @param string $debug   디버그 메시지(기본값: null)
     */
  public  function make_ajax_json($code = 0, $message = null, $extra = null, $debug = null) {
      return json_encode(
          array('code' => $code, 'message' => $message, 'extra' => $extra, 'debug' => $debug)
      );
  }

  // 계정 정보를 화면에 보여준다.
	public function info() {
    if($this->session->userdata('admin_id') != '') { // 세션 정보가 정상일 경우
      $data = array(
        'title'             => 'cheoleeblog setting',
        'account_data'      => $this->account->get_all_account()
      );

      $this->load->view('account_manage', $data);

      // var_dump($data);
    } else { // 세션 정보가 없는 경우 로그인 화면으로 이동한다.
			$this->session->set_flashdata('message', '        
        <script>
          $(function(){
            alert("세션이 종료되었습니다. 로그인 후 이용해 주세요.");
          });        
        </script>
			');

			redirect('/main/index'); //로그인 화면으로 이동
		}
  }
  
  // 계정을 추가한다.
  public function accountAdd() {
    $admin_id = $this->input->post('admin_id');
    $passwd = $this->input->post('passwd');
    $nickname = $this->input->post('nickname');
    $email = $this->input->post('email');
    $nickname_check = $this->input->post('nickname_check');

    // validate input
    $this->form_validation->set_rules('admin_id', 'Admin_id', 'required|is_unique[admin.admin_id]');
    $this->form_validation->set_rules('passwd', 'Passwd', 'required');
    $this->form_validation->set_rules('nickname', 'Nickname', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
  

    // 유효성 검사 후 계정을 추가한다.
    if($this->form_validation->run() == FALSE) {
      $this->info();
    } else {
      if ( $nickname_check == 'Y') {
        $data = array(
            'admin_id'        => $admin_id,
            'passwd'          => $passwd,
            'nickname'        => $nickname,
            'level'           => 99,
            'profile_image'   => '',
            'email'           => $email,
            'admin_status'    => 'O',
            'login_count'     => 0,
            'last_login_date' => date('Y-m-d H:i:s', time()),
            'reg_date'        => date('Y-m-d H:i:s', time())

        );

        // 계정을 추가한다.
        $this->account->add_account('admin',$data);

        $this->session->set_flashdata('message', '        
          <script>
            $(function(){
              alert("새로운 계정 ['.$admin_id.']가 추가되었습니다.");
            });
          </script>
          ');
          redirect('accountmanage/info');
        } else {
          $this->session->set_flashdata('message', '        
          <script>
            $(function(){
              alert("닉네임 중복 여부를 확인해주세요.");
              $("#admin_id").val("'.$admin_id.'");
              $("#passwd").val("'.$passwd.'");
              $("#nickname").val("'.$nickname.'");
              $("#email").val("'.$email.'");
            });            
          </script>
          ');
          redirect('accountmanage/info');
        }
    }
  }

  // 계정 정보를 업데이트 한다.
  public function accountUpdate() {
    $admin_id = $this->input->post('admin_id');
    $nickname = $this->input->post('nickname');
    $level = $this->input->post('level');

    $data = array(
      'nickname'  => $nickname,
      'level'     => $level
    );

    // 계정을 업데이트하고 업데이트된 계정 정보를 가져온다.
    $this->account->update_account_info('admin',$data,$admin_id);
    $account_info = $this->account->get_detail_account($admin_id);

    // 계정 상태 확인을 위해 다시 클라이언트에 보낸다.
    $result_array = array(
      'nickname' => $account_info[0]['nickname'],
      'level' => $account_info[0]['level']
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }
  
  // 계정 정보를 삭제한다.
  public function accountDelete() {

  }

  // 계정 상태를 업데이트 한다.
  public function statusUpdate() {
    $admin_id = $this->input->post('id');
    $status = $this->input->post('status');

    // 계정 상태를 업데이트하고 업데이트된 계정 정보를 가져온다.
    $this->account->status_update($admin_id,$status);
    $account_info = $this->account->get_detail_account($admin_id);
     

    // 계정 상태 확인을 위해 다시 클라이언트에 보낸다.
    $result_array = array(
      'admin_status' => $account_info[0]['admin_status']
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

  // 선택한 프로필 이미지 파일을 서버에 업로드한다.
  Public function profileUpload() {
    $user_id = $this->input->post('user_id');
    $new_profile = $user_id.'_new_profile';
    
    // validate image
    if (empty($_FILES[$new_profile]['name'])) {
      $this->form_validation->set_rules($new_profile, 'New_profile', 'required');
    }
    // var_dump($user_id.' : '.$_FILES['new_profile']['name']);

    // var_dump($description.'.'.$_FILES['new_image']['name']);
    $config['upload_path']		= '/var/data/img/profile_image'; //path folder
    $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
    $config['encrypt_name']		= TRUE;

    $this->upload->initialize($config);
    // if user upload new image
    if (!empty($_FILES[$new_profile]['name'])) {

      if ($this->upload->do_upload($new_profile)) {
        $img = $this->upload->data();

        //Compress Image
        $this->_resizeImage($img['file_name']);

        $profile_image = $img['file_name'];
        $last_used_date = date('Y-m-d H:i:s',time());
        $reg_date = date('Y-m-d H:i:s',time());

        // 업로드한 이미지의 정보를 DB에 넣어준다.
        $data = array(
            'user_id'         => $user_id,
            'profile_image'   => $profile_image,
            'used_count'      => 0,
            'is_activate'     => 'Y',
            'last_used_date'  => $last_used_date,
            'reg_date'        => $reg_date
        );

        // admin 테이블 업데이트 데이터를 배열로 생성해준다.
        $data_2 = array(
            'profile_image'   => $profile_image
        );
        
        // 기존의 프로필 이미지 파일을 삭제한다. 
        $is_profile_delete = $this->account->delete_profile_file($user_id);

        // var_dump($is_profile_delete);

        // insert data with model
        $this->account->insert_profile_image('user_profile_image', $data);

        // 계정 데이터의 프로필 이미지 정보를 업데이트 한다.
        $this->account->update_account_info('admin',$data_2,$user_id);
        
        // alert_move('['.$user_id.']님의 프로필 이미지가 변경되었습니다.','info');
        $this->session->set_flashdata('message', '        
        <script>
          $(function(){
            alert("['.$user_id.']님의 프로필 이미지가 변경되었습니다.");
          });
        </script>
        ');
        redirect('accountmanage/info');
      
      } else {
        $this->session->set_flashdata('message', '        
        <script>
          $(function(){
            alert("['.$user_id.']님의 프로필 이미지가 변경에 실패하였습니다.");
          });
        </script>
      ');
      redirect('accountmanage/info');
      // alert_move('['.$user_id.']님의 프로필 이미지 변경에 실패하였습니다.','info');

      }
    } else {
      $this->session->set_flashdata('message', '        
      <script>
        $(function(){
          alert("업로드할 이미지 파일을 선택해주세요.");
        });
      </script>
      ');
      redirect('accountmanage/info');
      // alert_move('업로드할 이미지 파일을 선택해주세요.','info');
    }
  }

  // 업로드 파일 크기를 조정한다.
  function _resizeImage($file_name)
	{
		// Image resizing config
		$config = array(
			'image_library' => 'GD2',
			'source_image'  => '/var/data/img/profile_image/' . $file_name,
			'maintain_ratio' => FALSE,
			'width'         => 30,
			'height'        => 30,
			'new_image'     => '/var/data/img/profile_image/' . $file_name
		);

		// load config (built in liblary CI3)
		$this->load->library('image_lib', $config);

		$this->image_lib->initialize($config);
		if (!$this->image_lib->resize()) {
			return false;
		}
		$this->image_lib->clear();
	}

  // 닉네임 중복 여부를 확인한다.
  public function nicknameCheck() {
    $nickname = $this->input->post('nickname');

    $nickname_count = $this->account->count_nickname('admin',$nickname);

    // 계정 상태 확인을 위해 다시 클라이언트에 보낸다.
    $result_array = array(
      'nickname_count' => $nickname_count
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));

  }
}
