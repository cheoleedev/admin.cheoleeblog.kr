<?php defined('BASEPATH') OR exit('No direct script access allowed');

//컨트롤러의 파일생성은 기본적으로 주소창의 주소 확장입니다.
//간단하게 말해 Main.php를 만들고 class를 설정하였다면 "URL/index.php/main"으로 접속 가능합니다.
//내부 function도 주소 확장입니다. "URL/index.php/main"로 접속하였다면 function index()가 기본적으로 실행됩니다.
//내부에 public function good() 함수를 추가하였다면 "URL/index.php/main/good"으로 실행됩니다.

class Postwrite extends CI_Controller {

	function __construct() {       
      parent::__construct();
      $this->load->model('Postwrite_model', 'post');
      $this->load->helper(array('form', 'url','alert','text'));
      $this->load->library(array('form_validation', 'session', 'upload'));
      
    }

    //index 함수 설정은 "URL/index.php/main" 또는 "URL/index.php/main/index"로 접속가능하게 함
	public function write() {
		if($this->session->userdata('admin_id') != '') { // 세션 정보가 정상일 경우
      
			$main_subject_data = $this->post->get_all_main_subject();
			$data = array(
				
				'main_subject_data' 		 => $main_subject_data,
				'main_subject_code' 		 => '',
				'main_subject_name_selected' => '',
				'sub_subject_name' 			 => '',
				'foreword' 					 => '',
				'title' 					 => '',
				'content' 					 => ''
			);

			$this->load->view('post_write', $data);

    	// var_dump($data);
		} else { // 세션 정보가 없는 경우 로그인 화면으로 이동한다.
			$this->session->set_flashdata('message', '        
			<script>
			$(function(){
				alert("세션이 종료되었습니다. 로그인 후 이용해 주세요.");
			});
			
			</script>
			');

			redirect('/main/index'); //로그인 화면으로 이동
		}
  	}

	// 포스트의 상세 내용을 보여준다.
  	public function detail($slug)
	{	
		$filter = $this->input->post_get('filter');
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = $this->input->post_get('search');
		if($search == null) {
			$search = '';
		}
		$cur_page = $this->input->get('page');
		if($cur_page == null) {
			$cur_page = 1;
		}

		// config 정보를 가져온다.
		$config = $this->post->get_config();
		$posts_per_page = $config->posts_per_page; // 페이지 당 포스트 개수

		$postDetail = $this->post->getDetailPost($slug);
		$where = array(
			'main_subject_name' => $postDetail['main_subject_name'],
			'sub_subject_name'  => $postDetail['sub_subject_name']
		);
		// 같은 주제의 포스트 정보를 가져온다.
		$posts = $this->db->get_where('post_content',$where)->result_array();
    	// var_dump($slug);
		// var_dump($postDetail);
		// $postDetail = $this->post->getDetailPost(urldecode($slug));
		// $postDetail = $this->post->getDetailPost(urldecode($slug));
    	// var_dump($slug);
		// var_dump($postDetail);
		$data = array(
			'posts'			    => $posts,
			'main_subject_name' => $postDetail['main_subject_name'],
			'sub_subject_name'  => $postDetail['sub_subject_name'],
			'foreword'    		=> $postDetail['foreword'],
			'title'				=> $postDetail['title'],
			'content' 			=> $postDetail['content'],
			'slug' 				=> $postDetail['slug'],
			'view_count' 		=> $postDetail['view_count'],
			'reg_date'			=> $postDetail['reg_date'],
			'thumbnail'			=> $postDetail['thumbnail'],
			'filter'			=> $filter,
			'search'			=> $search,
			'cur_page'			=> $cur_page,
			'posts_per_page'	=> $posts_per_page
		);

		$this->load->view('post_detail', $data);
	}

	// 작성한 포스트를 저장한다.
	public function save()
	{
		$main_subject_code = $this->input->post('main_subject'); // 오류 처리용
		$main_subject_name = $this->input->post('main_subject_name');
		$sub_subject_name  = $this->input->post('sub_subject');
		$foreword    	   = $this->input->post('foreword');
		$title 			   = $this->input->post('title');
		$writer_id 		   = $this->input->post('writer_id');
		$contents          = $this->input->post('contents');
		$ip_addr           = $this->input->ip_address(); // 사용자의 ip를 가져온다. 

		// validate input
		$this->form_validation->set_rules('main_subject', 'Main_subject', 'required');
		// $this->form_validation->set_rules('sub_subject', 'Sub_subject', 'required');
		$this->form_validation->set_rules('foreword', 'Foreword', 'required');       
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('writer_id', 'Writer_id', 'required');
		$this->form_validation->set_rules('contents', 'Content', 'required');

		// validate image
		// if (empty($_FILES['thumbnail']['name'])) {
		// 	$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
		// }
    	// var_dump($title,$contents,$thumbnail);

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', '        
				<script>
				$(function(){
					alert("누락된 항목이 있습니다.");
				});            
				</script>
			');
			$main_subject_data = $this->post->get_all_main_subject();
			$data = array(
				'main_subject_data'	=> $main_subject_data,
				'main_subject_code' => $main_subject_code,
				'main_subject_name_selected' => $main_subject_name, // 저장 전 선택된 메인 주제의 이름
				'sub_subject_name' => $sub_subject_name,
				'foreword' => $foreword,
				'title' => $title,
				'content' => $contents
			);
			
			// redirect('postwrite/write');
			$this->load->view('post_write', $data);
			// $this->write();
		} else {

			//upload_path는 반드시 절대경로로 설정해주어야 한다.
			$config['upload_path']		= '/var/data/img/post_profile'; //path folder
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['encrypt_name']		= TRUE;

			$this->upload->initialize($config);

			if (!empty($_FILES['thumbnail']['name'])) {
				// if user  uploading image

				if ($this->upload->do_upload('thumbnail')) {
					$img = $this->upload->data();

					//compress Image
					$this->_resizeImage($img['file_name']);

					$thumbnail = $img['file_name'];

					// 하위 주제 정보를 가져온다.
					$sub_subject = $this->db->get_where('sub_subject',array('sub_subject_name' => $sub_subject_name))->result_array();
					$sub_subject_code = $sub_subject[0]['sub_subject_code'];

					$data = array(						
						'main_subject_name'		=> $main_subject_name,
						'main_subject_code'		=> $main_subject_code,
						'sub_subject_name'		=> $sub_subject_name,
						'sub_subject_code'		=> $sub_subject_code,
						'foreword'				=> $foreword,
						'writer_id'				=> $writer_id,
						// built in helper CI3
						'slug'					=> 0,  //주소창에 붙는 포스트 url
						'title'					=> $title,
						'content' 				=> $contents,
						'thumbnail' 			=> $thumbnail,
						'view_count'  			=> 0,
						'ip_addr' 				=> $ip_addr,
						'is_activate' 			=> 'N',
						'reg_date'				=> date("Y-m-d H:i:s"),
						'last_update_date'		=> date("Y-m-d H:i:s")
					);
          			// var_dump($data);

					// insert data with model
					$this->post->insertPost('post_content', $data);

					// 저장된 포스트의 idx를 가져와서
					$idx = $this->post->get_idx('post_content',$title);

					// 'slug'에 idx값을 업데이트 해준다.
					$this->post->update_slug('post_content',$idx,$title);

					// 업데이트된 'slug'값을 가져온다.
					$slug = $this->post->get_slug('post_content',$idx);

					$this->session->set_flashdata('message', '
						<script>
							$(function(){
								alert("저장이 완료되었습니다.");
							});
						</script>
					');
					redirect('postwrite/detail/' . $slug);
				} else {
					// if uploading image error
					// show error
					$this->session->set_flashdata('message', '
						<script>
							$(function(){
								alert("'. $this->upload->display_errors() .'");
							});						
						</script>
					');
					// redirect('postwrite/wirte');
				}
			} else {
				// if user not uploading image
				// $this->session->set_flashdata('message', '
				// 	<script>
				// 		$(function(){
				// 			alert("썸네일 이미지 파일을 선택해주세요.");
				// 		});
				// 	</script>
				// ');
				// redirect('postwrite/write');
				// 하위 주제 정보를 가져온다.
				$sub_subject = $this->db->get_where('sub_subject',array('sub_subject_name' => $sub_subject_name))->result_array();
				$sub_subject_code = $sub_subject[0]['sub_subject_code'];

				$data = array(						
					'main_subject_name'		=> $main_subject_name,
					'main_subject_code'		=> $main_subject_code,
					'sub_subject_name'		=> $sub_subject_name,
					'sub_subject_code'		=> $sub_subject_code,
					'foreword'				=> $foreword,
					'writer_id'				=> $writer_id,
					// built in helper CI3
					'slug'					=> 0,  //주소창에 붙는 포스트 url
					'title'					=> $title,
					'content' 				=> $contents,
					'thumbnail' 			=> 'default.png',
					'view_count'  			=> 0,
					'ip_addr' 				=> $ip_addr,
					'is_activate' 			=> 'N',
					'reg_date'				=> date("Y-m-d H:i:s"),
					'last_update_date'		=> date("Y-m-d H:i:s")
				);
				  // var_dump($data);

				// insert data with model
				$this->post->insertPost('post_content', $data);

				// 저장된 포스트의 idx를 가져와서
				$idx = $this->post->get_idx('post_content',$title);

				// 'slug'에 idx값을 업데이트 해준다.
				$this->post->update_slug('post_content',$idx,$title);

				// 업데이트된 'slug'값을 가져온다.
				$slug = $this->post->get_slug('post_content',$idx);

				$this->session->set_flashdata('message', '
					<script>
						$(function(){
							alert("저장이 완료되었습니다.");
						});
					</script>
				');
				redirect('postwrite/detail/' . $slug);
			}
		}
	}


	function _resizeImage($file_name)
	{
		// Image resizing config
		$config = array(
			'image_library' => 'GD2',
			'source_image'  => '/var/data/img/post_profile/' . $file_name,
			'maintain_ratio' => FALSE,
			'width'         => 600,
			'height'        => 400,
			'new_image'     => '/var/data/img/post_profile/resize/' . $file_name
		);

		// load config (built in liblary CI3)
		$this->load->library('image_lib', $config);

		$this->image_lib->initialize($config);
		if (!$this->image_lib->resize()) {
			return false;
		}
		$this->image_lib->clear();
	}

	// 선택한 메인 주제에 해당하는 하위 주제를 모두 가져온다
	public function getSubSubject() {

		$main_subject_code = $this->input->post('selected_main_subject');
	
		// 선택한 메인 주제의 이름을 가져온다.
		$selected_main_subject = $this->post->get_selected_main_subject($main_subject_code);
		$main_subject_name = $selected_main_subject[0]['main_subject_name'];
		
		$sub_subject = $this->post->get_sub_subject($main_subject_code);
	
		//extra 배열에 메인 주제의 이름을 연관 배열로 요소를 추가한다.
		$sub_subject[] = array(
		  'main_subject_name' => $main_subject_name
		);
		
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $sub_subject, 'debug' => null));
		// try{
		// echo make_ajax_json(0, null, array('sub_subject' => $sub_subject), null);
		// } catch(Execption $e) {
		//   echo make_ajax_json(10,$e->getMessage().$subject_code);
		// }
	}


	public function delete($idx)
	{
		$where = array('idx' => $idx);

		$this->post->deletePost($where, 'post_content');

		$this->session->set_flashdata('message', '
			<script>
				$(function(){
					alert("포스트가 삭제되었습니다.");
				});
			</script>
		');
		redirect('/');
	}
}
