
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-light" >
		<?= $this->session->flashdata('message') ?>
		<div class="col-sm-12 col-lg-12">
			<div class="pt-2 fs-5 fw-bold">
				<svg class="bi me-1" width="16" height="16"><use xlink:href="#post_manage"/></svg>
				<span class="">포스트 관리</span>
			</div>
			<hr>
			<?= form_open_multipart('postmanage/manage') ?>
			<div class="d-flex justify-content-end mb-3">
				<div id="post_info" class="col-5 ps-2">
						<span style="font-size:.75rem;">(총</span>
						<span class="fw-bold mx-1" style="font-size:.75rem; color:blue;"><?= count($all_posts) ?></span>
						<span class="me-1" style="font-size:.75rem;">개)</span>
						<span class="info_text fw-bold me-1">Page</span><span class="info_text fw-bold" style="color:blue;"><?= $cur_page ?></span>
						<span class="info_text me-2">of</span><span class="info_text fw-bold"><?= $all_page ?></span>
					</div>
				<div class="col-3 me-1">
					<select class="form-select form-select-sm" aria-label=".form-select-sm example" id="filter" name="filter">
					<option value="title" <?= $filter == 'title' ? 'selected' : '' ?>>제목</option>
					<option value="foreword" <?= $filter == 'foreword' ? 'selected' : '' ?>>머리말</option>
					<option value="writer_id" <?= $filter == 'writer_id' ? 'selected' : '' ?>>작성자</option>
					<option value="content" <?= $filter == 'content' ? 'selected' : '' ?>>내용</option>
					</select>
				</div>
				<div class="col-4 d-flex">
					<input class="form-control form-control-sm me-1" type="search" placeholder="검색어 입력" aria-label="Search" id="search" name="search" value="<?= $search ?>">
					<button class="btn btn-sm btn-outline-primary" type="submit"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#search"/></svg></button>
				</div>
			</div>
			<?= form_close() ?>
			<?php foreach ($posts as $post) : ?>

			<div class="card mb-2 shadow-sm">
				<div class="row no-gutters mt-2 mx-0 px-2">
					<div class="col-md-12">
						<a href="<?= '/postmanage/detail/' . $post->slug .'?page='.$cur_page.'&filter='.$filter.'&search='.$search ?>" class="text-dark text-decoration-none p-0 m-0">											
							<div class="d-flex align-items-center mx-0 p-0 text-truncate">
								<!-- <div class="col-12 col-sm-8 col-md-8 col-lg-8 inline text-truncate px-0">	 -->
									<img src="<?= '/img/post_profile/'.$post->thumbnail ?>" class="img-thumbnail" width="30" alt="포스트 썸네일">
									<span class="d-inline-block" title="<?= '['.$post->foreword.'] '.$post->title ?>" style="max-width: 440px;">						
										<?= 
											// word_limiter('['.$post->foreword.'] '.$post->title, 4)
											'['.$post->foreword.'] '.$post->title;
										?>
									</span>
								<!-- </div> -->
							</div>
							<div class="d-flex align-items-center justify-content-between mx-0 p-0">	
								<!-- <div class="col-12 col-sm-4 col-md-4 col-lg-4 inline px-0"> -->
									<small class="mr-2">
										<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">작성자</i>
										<?= ' : '.$post->writer_id ?>
									</small>

									<small class="mr-2 float-end">
										<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">조회수</i>
										<?= ' : '.$post->view_count ?>
									</small>
								<!-- </div> -->
							</div>
							<hr class="my-1">
							<div class="d-flex align-items-center justify-content-between mx-0 p-0">
								<small class="mr-2">
									<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">주제</i>
									<?= ' : '.$post->main_subject_name.' > '.$post->sub_subject_name  ?>
								</small>

								<small class="mr-2">
									<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">등록일</i>
									<?= ' : '.date('Y-m-d',strtotime($post->reg_date))  ?>
								</small>
							</div>					
							<hr class="mt-1">

							<?= word_limiter(strip_tags($post->content), 4) ?>

						</a>
						<div class="d-flex justify-content-end align-items-center py-1">
							<div class='icon-wrap me-2' style="display:inline-block;">
								<span class="badge rounded-pill bg-primary">
									<?php
										// 포스트의 댓글 정보를 가져와 개수를 출력한다.
										$comments = $this->post->get_all_comment($post->slug);
										echo count($comments);
									?>
								</span>
							</div>
							<div class='icon-wrap me-2' style="display:inline-block;">
								<div class="form-check form-switch d-flex justify-content-center align-items-center">
									<?php if($post->is_activate == 'Y') { ?>
										<span title="상태 변경"><input class="form-check-input" type="checkbox" slug="<?= $post->slug ?>" checked  onclick="post_status_change(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
									<?php } else { ?>
										<span title="상태 변경"><input class="form-check-input" type="checkbox" slug="<?= $post->slug ?>" onclick="post_status_change(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
									<?php } // --end of if ?>
								</div>		
							</div>
							<div class="icon-wrap" style="display:inline-block;">
								<a href="<?= '/postmanage/edit/' . $post->slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search ?>" class="text-dark text-decoration-none p-0 m-0">
									<img src="<?= '/img/pencil-square.svg' ?>" class="img-thumbnail me-1" width="40" alt="편집" title="편집">
								</a>
								<a href="<?= '/postmanage/delete/' . $post->slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search ?>" class="text-dark text-decoration-none p-0 m-0">
									<img src="<?= '/img/trash.svg' ?>" class="img-thumbnail" width="40" alt="삭제" title="삭제">
								</a>	
							</div>
						</div>
						
							
					</div>
				</div>
			</div>
			<?php endforeach ?>
		</div>
		<!-- pagination -->
		<div>
			<nav aria-label="Page navigation-sm">
				<ul class="pagination justify-content-center">
				<?php
					if($cur_page == 1){ 	// 현재 페이지 번호가 1이면 
						$pre = $cur_page;   // 이전 페이지 번호도 1
					} else {
						$pre = $cur_page - 1;
					}
					$next = $cur_page + 1;	// 다음 페이지 번호

					// 현재 페이지 번호가 단위 페이지수와 같으면 다음 단위로 넘어가지 않도록 현재 페이지 번호를 조정해준다.
					if (($cur_page%$page_item_count) == 0){ 
						$start = (floor(($cur_page-1)/$page_item_count)*$page_item_count) + 1; // 페이지 시작 번호	
					} else {
						$start = (floor($cur_page/$page_item_count)*$page_item_count) + 1; // 페이지 시작 번호
					}
				?>
						<li class="page-item">
							<a class="page-link" href="<?= base_url('postmanage/manage?page=1').'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="처음">처음</span></a>
						</li>
				<?php
					// 현재 페이지 번호가 1이면 <<(이전) 표시를 하지 않고 나머지 페이지 번호를 넣어준다.
					if($cur_page == 1) {
						for($i = 0; $i < $page_item_count; $i++ ) {
							$page = $i + $start;
							// 현재 페이지 번호가 전체 페이지수와 같으면 반복문을 중단한다.
							if($page > $all_page) { 
								break;
							}
							if($page == $cur_page) { // 현재 페이지와 같으면 활성화 처리를 해준다.
				?>
								<li class="page-item active"><a class="page-link" href="<?= '/postmanage/manage?page=' . $page.'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="<?= $page ?>"><?= $page ?></span></a></li>
				<?php
							} else {
				?>
								<li class="page-item"><a class="page-link" href="<?= '/postmanage/manage?page=' . $page.'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="<?= $page ?>"><?= $page ?></span></a></li>
				<?php
							}
						}
						if($all_page != 1) {
				?>
						<li class="page-item">
						<a class="page-link" href="<?= '/postmanage/manage?page=' . $next.'&filter='.$filter.'&search='.$search ?>" aria-label="Next">
							<span style="font-size:13;" aria-hidden="true" title="다음">&#9654;</span>
						</a>
					</li>
				<?php
						}
					} elseif($cur_page == $all_page) { // 현재 페이지 번호가 마지막 페이지 번호와 같으면 >>(다음) 표시를 하지 않고 나머지 페이지 번호를 넣어준다.
				?>
						<li class="page-item">
							<a class="page-link" href="<?= '/postmanage/manage?page=' . $pre.'&filter='.$filter.'&search='.$search ?>" aria-label="Previous">
								<span style="font-size:13;" aria-hidden="true" title="이전">&#9664;</span>
							</a>
						</li>
				<?php
						for($i = 0; $i < $page_item_count; $i++ ) {
							$page = $i + $start;
							// 현재 페이지 번호가 전체 페이지수와 같으면 반복문을 중단한다.
							if($page > $all_page) {
								break;
							}
							if($page == $cur_page) { // 현재 페이지와 같으면 활성화 처리를 해준다.
				?>
								<li class="page-item active"><a class="page-link" href="<?= '/postmanage/manage?page=' . $page.'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="<?= $page ?>"><?= $page ?></span></a></li>
				<?php
							} else {
				?>
								<li class="page-item"><a class="page-link" href="<?= '/postmanage/manage?page=' . $page.'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="<?= $page ?>"><?= $page ?></span></a></li>
				<?php
							}
						}
					} else { // 그렇지 않은 경우 <<(이전), >>(다음)표시를 넣어준다.
				?>
						<li class="page-item">
							<a class="page-link" href="<?= '/postmanage/manage?page=' . $pre.'&filter='.$filter.'&search='.$search ?>" aria-label="Previous">
								<span style="font-size:13;" aria-hidden="true" title="이전">&#9664;</span>
							</a>
						</li>
				<?php
						for($i = 0; $i < $page_item_count; $i++ ) {
							$page = $i + $start;
							// 현재 페이지 번호가 전체 페이지수와 같으면 반복문을 중단한다.
							if($page > $all_page) {
								break;
							}
							if($page == $cur_page) { // 현재 페이지와 같으면 활성화 처리를 해준다.
				?>
								<li class="page-item active"><a class="page-link" href="<?= '/postmanage/manage?page=' . $page.'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="<?= $page ?>"><?= $page ?></span></a></li>
				<?php
							} else {
				?>
								<li class="page-item"><a class="page-link" href="<?= '/postmanage/manage?page=' . $page.'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="<?= $page ?>"><?= $page ?></span></a></li>
				<?php
							}
						}
				?>
						<li class="page-item">
							<a class="page-link" href="<?= '/postmanage/manage?page=' . $next.'&filter='.$filter.'&search='.$search ?>" aria-label="Next">
								<span style="font-size:13;" aria-hidden="true" title="다음">&#9654;</span>
							</a>
						</li>
				<?php
					}
				?>
						<li class="page-item">
							<a class="page-link" href="<?= '/postmanage/manage?page='. $all_page.'&filter='.$filter.'&search='.$search ?>"><span style="font-size:13;" title="마지막">마지막</span></a>
						</li>
				</ul>
			</nav>
		</div>

	</div>
	<!-- --end of main content -->
	
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
