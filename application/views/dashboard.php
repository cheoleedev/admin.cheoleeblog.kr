
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-light" style="height:calc(100vh - 167px); overflow:auto;" >
		<div class="pt-2 fs-5 fw-bold">
			<svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"/></svg>
			<span class="fs-5 pt-3">데쉬보드</span>
		</div>
		<hr>
		<!-- 주제별 현황 -->
		<div class="border-bottom border-1" id="subject_statics">
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold setting_title" ><span class="me-1">주제별 현황</span>
					<!-- 버튼 외의 곳을 클릭하면 팝오버를 감춰지도록 하는 사용법 -->
					<!-- <a tabindex="0" id="popover_dismiss_subject" role="button" data-bs-toggle="popover" data-bs-trigger="focus" title="주제별 현황" data-bs-content="각 주제를 기준으로 포스트 등록 비율, 댓글 등록 개수, 대댓글 등록 개수 등을 통계로 보여줍니다. 또한, 포스트의 랭킹 포인트를 기준으로 순위를 매긴 리스트로 확인할 수 있습니다." >?</a> -->
					<!-- 버튼 토글로 팝오버 요소 보이기/감추기 하는 사용법  -->
					<a role="button" id="popover_subject" data-bs-toggle="popover" title="주제별 현황" data-bs-content="각 주제를 기준으로 포스트 등록 비율, 댓글 등록 개수, 대댓글 등록 개수 등의 통계를 보여줍니다. 또한, 포스트의 랭킹 포인트를 기준으로 한 순위 리스트도 확인할 수 있습니다." ><span class="text-secondary info_text">(?)</span></a>
				</p>
			</div>
			<div id="total_statics_content_wrap" class="row">
				<div class="col-sm-12 col-lg-12" id="main_subject">
					<div class="row border-bottom border-3 py-1">
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">#</div>
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">포스트</div>
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">댓글</div>
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">대댓글</div>
					</div>
					<div class="row border-bottom border-3 py-1" style="background-color:#c6ebf2;">
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;"><span style="color:blue; font-weight:bolder;">전체</span></div>
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
							<span style="color:blue; font-weight:bolder;">
							<?php
								echo $this->main->count_all_data();  // 전체 포스트 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
							<span style="color:blue; font-weight:bolder;">
							<?php
								echo $this->main->count_all_comment();  // 전체 댓글 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
							<span style="color:blue; font-weight:bolder;">
							<?php
								echo $this->main->count_all_comment_reply();  // 전체 대댓글 수
							?>
							</span>
						</div>
					</div>
					
					<?php
						for($i=0; $i < count($main_subject); $i++) {
							$sub_subject = $this->main->get_sub_subject($main_subject[$i]['main_subject_code']); // 각 메인주제의 하위주제를 가져온다.
							if(($i+1)%2 == 0) {		// 짝수 번째 행인 경우
					?>
					<div class="row border-bottom border-3 py-1 main_subject_statics" id="<?= $main_subject[$i]['main_subject_code'] ?>" subject_title="<?= $main_subject[$i]['main_subject_name'] ?>" onclick="open_sub_subject_statics(this,get_sub_subject_post_data,get_sub_subject_post_reply_data)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="background-color:#F2F2F2; cursor:pointer;">
						<div class="col align-middle text-center fw-bolder main_subject_name" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name" color="<?= $main_subject[$i]['color'] ?>"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
						<div class="col align-middle text-center fw-bolder main_subject_post" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post">
							<span>
							<?php
								echo $this->main->count_main_subject_post_data($main_subject[$i]['main_subject_name']); //각 메인주제 별 포스트 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center fw-bolder main_subject_comment" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment">
							<span>
							<?php
								echo $this->main->count_main_subject_comment_data($main_subject[$i]['main_subject_name']); // 각 메인주제 별 댓글 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center fw-bolder main_subject_comment_reply" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply">
							<span>
							<?php
								echo $this->main->count_main_subject_comment_reply_data($main_subject[$i]['main_subject_name']); // 각 메인주제 별 대댓글 수
							?>
							</span>
						</div>
					</div>
					<!-- 메인 주제의 하위 주제 통계 -->
					<div id="<?= $main_subject[$i]['main_subject_code'] ?>_sub" style="display: none;"> 
						<?php 
									for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
										if($j == (count($sub_subject)-1)){
						?>
						<div class="row border-bottom border-1 py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>" style="background-color:#F2F2F2;">
						<?php
										} else {
						?>
						<div class="row py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>" style="background-color:#F2F2F2;">
						<?php
										} // -- end of count($sub_subject) 
						?>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name" color="<?= $sub_subject[$j]['color'] ?>"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post">
								<span>
								<?php
									echo $this->main->count_sub_subject_post_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); //각 하위주제 별 포스트 수
								?>
								</span>
							</div>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment">
								<span>
								<?php
									echo $this->main->count_sub_subject_comment_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 댓글 수
								?>
								</span>
							</div>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply">
								<span>
								<?php
									echo $this->main->count_sub_subject_comment_reply_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 대댓글 수
								?>
								</span>
							</div>
						</div>
						<?php
									}  // -- end of for sub_subject
						?>
						<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
						<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_charts" style="background-color:#F2F2F2;">
							<div class="col-sm-12 col-lg-6">
								<!-- 포스트 등록 현황 파이 차트 -->
								<div>
									<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [전체] 포스트 등록 비율 </p>
								</div>
								<div id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_pie"></div>
							</div>
							<div class="col-sm-12 col-lg-6" style="margin-top:7px;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_bar">
								<!-- 포스트 및 댓글 등록 현황 막대 차트 -->
							</div>
						</div>
						<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
					</div>
					<!-- end of 메인 주제의 하위 주제 통계 -->
					<?php
							} else {	// 홀수 번째 행인 경우
					?>
					<div class="row border-bottom border-3 py-1 main_subject_statics" id="<?= $main_subject[$i]['main_subject_code'] ?>" subject_title="<?= $main_subject[$i]['main_subject_name'] ?>" onclick="open_sub_subject_statics(this,get_sub_subject_post_data,get_sub_subject_post_reply_data)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="cursor:pointer;">
						<div class="col align-middle text-center fw-bolder main_subject_name" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name" color="<?= $main_subject[$i]['color'] ?>"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
						<div class="col align-middle text-center fw-bolder main_subject_post" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post">
							<span>
							<?php
								echo $this->main->count_main_subject_post_data($main_subject[$i]['main_subject_name']);
							?>
							</span>
						</div>
						<div class="col align-middle text-center fw-bolder main_subject_comment" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment">
							<span>
							<?php
								echo $this->main->count_main_subject_comment_data($main_subject[$i]['main_subject_name']);
							?>
							</span>
						</div>
						<div class="col align-middle text-center fw-bolder main_subject_comment_reply" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply">
							<span>
							<?php
								echo $this->main->count_main_subject_comment_reply_data($main_subject[$i]['main_subject_name']);
							?>
							</span>
						</div>
					</div>
					<!-- 메인 주제의 하위 주제 통계 -->
					<div id="<?= $main_subject[$i]['main_subject_code'] ?>_sub" style="display: none;"> 
						<?php
									for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
										if($j == (count($sub_subject)-1)){
						?>
						<div class="row border-bottom border-1 py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>">
						<?php
										} else {
						?>
						<div class="row py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>">
						<?php
										} // -- end of count($sub_subject) 
						?>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name" color="<?= $sub_subject[$j]['color'] ?>"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post">
								<span>
								<?php
									echo $this->main->count_sub_subject_post_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); //각 하위주제 별 포스트 수
								?>
								</span>
							</div>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment">
								<span>
								<?php
									echo $this->main->count_sub_subject_comment_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 댓글 수
								?>
								</span>
							</div>
							<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply">
								<span>
								<?php
									echo $this->main->count_sub_subject_comment_reply_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 대댓글 수
								?>
								</span>
							</div>
						</div>
						<?php
									}  // -- end of for sub_subject
						?>
						<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
						<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_charts">
							<div class="col-sm-12 col-lg-6">
								<!-- 포스트 등록 현황 파이 차트 -->
								<div>
									<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [전체] 포스트 등록 비율 </p>
								</div>
								<div id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_pie"></div>
							</div>
							<div class="col-sm-12 col-lg-6" style="margin-top:7px;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_bar">
								<!-- 포스트 및 댓글 등록 현황 막대 차트 -->
							</div>
						</div>
						<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
					</div>
					<!-- end of 메인 주제의 하위 주제 통계 -->
					<?php
							} // -- end of if (짝수행)
						} // -- end of for main_subject
					?>
					
				</div>
			
				<div class="col-sm-12 col-lg-6" style="background-color:#D1E9F1; height:305px;">
					<!-- 포스트 등록 현황 파이 차트 -->
					<div>
						<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [전체] 포스트 등록 비율 </p>
					</div>
					<div id="post_statics_pie"></div>
				</div>
				<div class="col-sm-12 col-lg-6 px-0" id="post_ranking_list_total" style="background-color:#D1E9F1; height:305px;">
					<!-- 포스트 순위 리스트 -->
					<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [전체] 포스트 인기 순위 </p>
					<div class="row mx-0">
						<div class="col-2 text-center">
							<span class="info_text fw-bold">순위</span>
						</div>
						<div class="col-6 text-ellipsis text-center">
							<span class="info_text fw-bold">제목</span>
						</div>
						<div class="col-4 text-ellipsis text-center">
							<span class="info_text fw-bold">분야</span>
						</div>
					</div>
					<div class="list-group" style="overflow:auto; height:230px;" id="post_ranking_list_item_total_wrap">
						<div id="post_ranking_list_item_total">
							<!-- 포스트 순위 리스트 표시 영역-->
						</div>
						<!-- <div class="d-flex justify-content-center w-100">
							<button type="button" class="btn btn-outline-secondary btn-sm py-0 mt-2 mb-1" mark="total" onclick="add_post_list(this)"><span class="info_text">더보기</span></button>
						</div> -->
					</div>
				</div>
				<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="post_statics_bar" style="background-color:#D1E9F1;">
					<!-- 포스트 및 댓글 등록 현황 막대 차트 -->
				</div>
			</div>
		</div>
		<input type="hidden" id="start_order_total" value="1">
		<!-- end of 주제별 현황 -->
		<!-- 기간별 현황 -->
		<div class="row border-bottom border-1 mt-3 " id="period_statics">
			<div class=" d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold setting_title" ><span class="me-1">기간별 현황</span>
					<a role="button" id="popover_period" data-bs-toggle="popover" title="기간별 현황" data-bs-content="설정한 기간 동안의 데이터에 대해 각 주제를 기준으로 포스트 등록 비율, 댓글 등록 개수, 대댓글 등록 개수 등의 통계를 보여줍니다. 또한, 설정된 기간 중 등록된 포스트의 랭킹 포인트를 기준으로 한 순위 리스트도 확인할 수 있습니다." ><span class="text-secondary info_text">(?)</span></a>
				</p>
			</div>
			
			<nav>
				<div class="nav nav-tabs" id="daily_statics_nav_tab" role="tablist">
					<button class="nav-link active" id="daily_statics_tab" data-bs-toggle="tab" data-bs-target="#daily_statics" type="button" onclick="daily_statics(draw_daily_chart,post_ranking_list_daily_period)" role="tab" aria-controls="daily_statics" aria-selected="true">일별</button>
					<button class="nav-link" id="monthly_statics_tab" data-bs-toggle="tab" data-bs-target="#monthly_statics" type="button" onclick="monthly_statics(draw_monthly_chart,post_ranking_list_monthly_period)" role="tab" aria-controls="monthly_statics" aria-selected="false">월별</button>
					
				</div>
			</nav>
				<div class="tab-content" id="nav-tabContent">
					<!-- 일별 현황 tab  -->
					<div class="tab-pane fade show active" id="daily_statics" role="tabpanel" aria-labelledby="daily_statics_tab">
						<form class="row g-3 align-items-center mt-1">
							<div class="col-5">
								<div class="input-group">
									<div class="input-group-text" style="font-size:12px;">시작일</div>
									<input class="form-control" type="text" id="start_day" style="font-size:.75rem;" value="<? echo date('Y-m-d',strtotime("-1 week")); ?>" >
								</div>
							</div>
							<div class="col-5">
								<div class="input-group">
									<div class="input-group-text" style="font-size:12px;">종료일</div>
									<input class="col-10 form-control" type="text" id="end_day" style="font-size:.75rem;" value="<? echo date('Y-m-d',time()); ?>" >
								</div>
							</div>
							<div class="col-2">
								<button class="btn btn-outline-secondary" id="daily_statics_button" type="button" onclick="daily_statics(draw_daily_chart,post_ranking_list_daily_period);"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#search"/></svg></button>
							</div>
						</form>
						<br>
						<!-- 일별 현황 요소가 들어갈 wrap 요소 -->
						<div id="daily_statics_content_wrap" class="row">
							<!-- 일별 현황 요소 표시 -->
						</div>
						<input type="hidden" id="start_order_daily_period" value="1">

					</div>
					<!-- 월별 현황 tab -->
					<div class="tab-pane fade" id="monthly_statics" role="tabpanel" aria-labelledby="monthly_statics_tab">
						<form class="row g-3 align-items-center mt-1">
							<div class="col-5">
								<div class="input-group" id="start_month_div">
									<div class="input-group-text" style="font-size:12px;">시작월</div>
									<input class="form-control" type="text" mark="start" id="start_month" style="font-size:.75rem;" value="<?php echo date('Y-m',strtotime("-4 month")); ?>" >
									<input type="hidden" id="start_month_old" style="font-size:.75rem;" value="<?php echo date('Y-m',strtotime("-4 month")); ?>" >
									
								</div>
							</div>
							<div class="col-5">
								<div class="input-group">
									<div class="input-group-text" id="end_month_div" style="font-size:12px;">종료월</div>
									<input class="form-control" type="text" mark="end" id="end_month" style="font-size:.75rem;" value="<?php echo date('Y-m',strtotime("Now")); ?>" >
									<input type="hidden" id="end_month_old" style="font-size:.75rem;" value="<?php echo date('Y-m',strtotime("Now")); ?>" >
								</div>
							</div>
							<input type="hidden" id="year_old" style="font-size:.75rem;" value="0" >
							<input type="hidden" id="month_old" style="font-size:.75rem;" value="0" >
							<div class="col-2">
								<button class="btn btn-outline-secondary" id="monthly_statics_button" type="button" onclick="monthly_statics(draw_monthly_chart,post_ranking_list_monthly_period);"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#search"/></svg></button>
							</div>
						</form>
						<br>
						<!-- 월별 현황 요소가 들어갈 wrap 요소 -->
						<div id="monthly_statics_content_wrap" class="row">
							<!-- 월별 현황 요소 표시 -->
						</div>
						<input type="hidden" id="start_order_monthly_period" value="1">
					</div>
				</div>
		</div>
		<!-- Modal -->
		<!-- strart_month_modal -->
		<div class="modal fade" id="start_month_modal" tabindex="-1" aria-labelledby="start_month_modal_Label" aria-hidden="true">
			<!-- Vertically centered modal -->
			<div class="modal-dialog modal-dialog-centered " >
				<div class="modal-content ">
					<div class="modal-header">
						<h5 class="modal-title" id="start_month_modal_Label">안내</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body text-center">
						종료월보다 이후 시점을 선택할 수 없습니다.
					</div>
					<!-- <div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					</div> -->
				</div>
			</div>
		</div>
		<!-- end_month_modal -->
		<div class="modal fade" id="end_month_modal" tabindex="-1" aria-labelledby="end_month_modal_Label" aria-hidden="true">
			<!-- Vertically centered modal -->
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="end_month_modal_Label">안내</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body text-center">
						시작월보다 이전 시점을 선택할 수 없습니다.
					</div>
					<!-- <div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					</div> -->
				</div>
			</div>
		</div>
		
		
	</div>
	<!-- --end of main content -->

	<script>
		$(function(){
			var post_statics_data = '';  // 포스트 등록 현황 데이터 변수를 전역 변수로 선언한다.
			var comment_statics_data = '';  // 댓글 등록 현황 데이터 변수를 전역 변수로 선언한다.
			var comment_reply_statics_data = '';  // 댓글에 대한 댓글 등록 현황 데이터 변수를 전역 변수로 선언한다.
			var subject_name_arr = '';  // 각 주제 이름 데이터 변수를 전역 변수로 선언한다.
			var post_ranking_list_total_data = ''; // 전체 포스트 랭킹 리스트 변수를 전역 변수로 선언한다.
			var post_ranking_list_by_daily_data = ''; // 선택한 기간의 포스트 랭킹 리스트 변수를 전역 변수로 선언한다. 
			var limit = ''; // 포스트 리스트를 한번에 넣을 개수 제한 변수를 전역 변수로 선언한다.
			var year = '';
			var month_old = '';
			var mark = '';
			var subject_colors = '';
			

			// 포스트 등록 비율 현황 파이 차트를 그린다.
			get_main_subject_post_data(post_statics_pie_draw);

			// 랭킹 포인트를 업데이트 하고 포스트 순위 리스트를 가져온다.
			// update_ranking_point(post_ranking_list_total);

			// 포스트 순위 리스트를 가져온다.
			post_ranking_list_total();

			// 포스트 등록 및 댓글 현황 막대 차트를 그린다.
			get_main_subject_post_reply_data(post_statics_bar_draw);

			// 일별 현황 요소를 로딩한다.
			$('#daily_statics_tab').click();

			// popover 트리거들을 초기화 한다.
			popovers_init();
			
			// 월 입력 요소 modal을 초기화 한다.
			var start_month_modal = modal_init('start_month_modal');	// 안내 메세지 모달창을 초기화 한다.
			var end_month_modal = modal_init('end_month_modal');	// 안내 메세지 모달창을 초기화 한다.

			// datepicker 설정
			$.datepicker.regional['ko'] = {
				show: 'both',
				nextText: '다음 달',
				prevText: '이전 달', 
				dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
				dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'], 
				monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				dateFormat: "yy-mm-dd",
				yearRange: 'c-1:c+10',
				closeText: '닫기',
			};
			$.datepicker.setDefaults($.datepicker.regional['ko']);
			//////////////////////////////
			// 일별 현황에 사용되는 요소
			//////////////////////////////
			// 시작일 요소
			$( "#start_day" ).datepicker({
				// changeMonth: true, 
				// changeYear: true,
				maxDate: 0,
				onClose: function( selectedDate ) {    
					//시작일(startDate) datepicker가 닫힐때
					//종료일(endDate)의 선택할수있는 최소 날짜(minDate)를 선택한 시작일로 지정
					$("#end_day").datepicker( "option", "minDate", selectedDate );
				}    

			});
			// 종료일 요소
			$( "#end_day" ).datepicker({
				// changeMonth: true, 
				// changeYear: true,
				maxDate: 0,                       // 선택할수있는 최소날짜, ( 0 : 오늘 이후 날짜 선택 불가)
				onClose: function( selectedDate ) {    
					// 종료일(endDate) datepicker가 닫힐때
					// 시작일(startDate)의 선택할수있는 최대 날짜(maxDate)를 선택한 시작일로 지정
					$("#start_day").datepicker( "option", "maxDate", selectedDate );
				}    
			});
			
			//monthpicker 옵션
			options = {
				pattern: 'yyyy-mm',
				year: 2021,
				autoclose: true
			}

			//monthpicker 적용
			//////////////////////////////
			// 월별 현황에 사용되는 요소
			//////////////////////////////
			// 시작월 요소
			$('#start_month').monthpicker(options);
			$('#start_month').monthpicker().bind('monthpicker-click-month', function(e, month){
				// console.log(e);
				// var new_date = $('#start_month').monthpicker('getDate'); // getDate 메소드로 입력되있던 날짜 정보를 가져온다.
				var start_month_old = $('#start_month_old').val();
				var start_date_old = new Date($('#start_month_old').val()+'-01'); 
				var new_date = new Date($('#start_month').val()+'-01');
				var end_month = new Date($('#end_month').val()+'-01');
				console.log(new_date+' , '+end_month);
				year = new_date.getFullYear();
				year_old = Number(start_date_old.getFullYear());
				month_old = Number(start_date_old.getMonth()) + 1;
				
				$('#year_old').val(year_old);
				$('#month_old').val(month_old);
				console.log(year+'년'+ month + "월이 선택 되었습니다.");
				if(Number(month) < 10) {
					month = '0'+month
				}
				var new_start_month_old = year+'-'+month
				settings = {
						pattern: 'yyyy-mm',
                        selectedMonth: Number(month_old),
                        selectedMonthName: '',
                        selectedYear: Number(year_old),
                        startYear: year - 10,
                        finalYear: year + 10,
                        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                        id: "monthpicker_" + (Math.random() * Math.random()).toString().replace('.', ''),
                        openOnFocus: true,
                        disabledMonths: []
				}

				if(new_date <= end_month){
					$('#year_old').val(0);
					$('#month_old').val(0);
					$('#start_month').monthpicker('setValue', settings);
					$('#start_month').val(new_start_month_old);	// 선택한 월을 start_month 요소에 넣어준다.
					$('#start_month_old').val(new_start_month_old);	// 선택한 월을 hidden 요소에 넣어준다.
				}else {
					start_month_modal.show(); // 안내 메세지 modal을 띄운다.
					// console.log(settings);
					// console.log('month_old : '+month_old);
					$('#start_month').monthpicker('setValue', settings);
					// $('#start_month').monthpicker(options);
					console.log($('#start_month').data());
					$('#start_month').val(start_month_old);
				}
			});

			// 종료월 요소
			$('#end_month').monthpicker(options);
			$('#end_month').monthpicker().bind('monthpicker-click-month', function(e,month){
				var end_month_old = $('#end_month_old').val();
				var end_date_old = new Date($('#end_month_old').val()+'-01'); 
				var new_date = new Date($('#end_month').val()+'-01');
				var start_month = new Date($('#start_month').val()+'-01');
				console.log(start_month+' , '+new_date);
				year = new_date.getFullYear();
				year_old = Number(end_date_old.getFullYear());
				month_old = Number(end_date_old.getMonth()) + 1;
				$('#year_old').val(year_old);
				$('#month_old').val(month_old);
				console.log(year+'년'+month + "월이 선택 되었습니다.");
				if(Number(month) < 10) {
					month = '0'+month
				}
				var new_end_month_old = year+'-'+month
				settings = {
						pattern: 'yyyy-mm',
                        selectedMonth: Number(month_old),
                        selectedMonthName: '',
                        selectedYear: Number(year_old),
                        startYear: year - 10,
                        finalYear: year + 10,
                        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                        id: "monthpicker_" + (Math.random() * Math.random()).toString().replace('.', ''),
                        openOnFocus: true,
                        disabledMonths: []
				}

				if(start_month <= new_date){
					$('#year_old').val(0);
					$('#month_old').val(0);
					$('#end_month').monthpicker('setValue', settings);
					$('#end_month').val(new_end_month_old);	// 선택한 월을 end_month 요소에 넣어준다.
					$('#end_month_old').val(new_end_month_old);	// 선택한 월을 hidden 요소에 넣어준다.
				}else {
					end_month_modal.show(); // 안내 메세지 modal을 띄운다..
					$('#end_month').monthpicker('setValue', settings);
					$('#end_month').val(end_month_old);
				}
			});
		});

		//특정 인덱스 문자열 치환 함수
		String.prototype.replaceAt = function(index, character) {
			return this.substr(0, index) + character + this.substr(index+character.length);
		};

	</script>			
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
