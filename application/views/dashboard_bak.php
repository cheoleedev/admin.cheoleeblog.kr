
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-light" style="height:calc(100vh - 167px); overflow:auto;" >
		<div class="pt-2 fs-5 fw-bold">
			<svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"/></svg>
			<span class="fs-5 pt-3">데쉬보드</span>
		</div>
		<hr>
		<!-- 주제별 현황 -->
		<div class="row border-bottom border-1" id="subject_statics">
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold setting_title" >주제별 현황</p>
			</div>
			<div class="col-sm-12 col-lg-12" id="main_subject">
				<div class="row border-bottom border-3 py-1">
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">#</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">포스트</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">댓글</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">대댓글</div>
				</div>
				<div class="row border-bottom border-3 py-1" style="background-color:#c6ebf2;">
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;"><span style="color:blue; font-weight:bolder;">전체</span></div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
						<span style="color:blue; font-weight:bolder;">
						<?php
							echo $this->main->count_all_data();  // 전체 포스트 수
						?>
						</span>
					</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
						<span style="color:blue; font-weight:bolder;">
						<?php
							echo $this->main->count_all_comment();  // 전체 댓글 수
						?>
						</span>
					</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
						<span style="color:blue; font-weight:bolder;">
						<?php
							echo $this->main->count_all_comment_reply();  // 전체 대댓글 수
						?>
						</span>
					</div>
				</div>
				
				<?php
					for($i=0; $i < count($main_subject); $i++) {
						$sub_subject = $this->main->get_sub_subject($main_subject[$i]['main_subject_code']); // 각 메인주제의 하위주제를 가져온다.
						if(($i+1)%2 == 0) {		// 짝수 번째 행인 경우
				?>
				<div class="row border-bottom border-3 py-1 main_subject_statics" id="<?= $main_subject[$i]['main_subject_code'] ?>" subject_title="<?= $main_subject[$i]['main_subject_name'] ?>" onclick="open_sub_subject_statics(this,get_sub_subject_post_data,get_sub_subject_post_reply_data)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="background-color:#F2F2F2; cursor:pointer;">
					<div class="col align-middle text-center fw-bolder main_subject_name" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
					<div class="col align-middle text-center fw-bolder main_subject_post" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post">
						<span>
						<?php
							echo $this->main->count_main_subject_post_data($main_subject[$i]['main_subject_name']); //각 메인주제 별 포스트 수
						?>
						</span>
					</div>
					<div class="col align-middle text-center fw-bolder main_subject_comment" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment">
						<span>
						<?php
							echo $this->main->count_main_subject_comment_data($main_subject[$i]['main_subject_name']); // 각 메인주제 별 댓글 수
						?>
						</span>
					</div>
					<div class="col align-middle text-center fw-bolder main_subject_comment_reply" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply">
						<span>
						<?php
							echo $this->main->count_main_subject_comment_reply_data($main_subject[$i]['main_subject_name']); // 각 메인주제 별 대댓글 수
						?>
						</span>
					</div>
				</div>
				<!-- 메인 주제의 하위 주제 통계 -->
				<div id="<?= $main_subject[$i]['main_subject_code'] ?>_sub" style="display: none;"> 
					<?php 
								for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
									if($j == (count($sub_subject)-1)){
					?>
					<div class="row border-bottom border-1 py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>" style="background-color:#F2F2F2;">
					<?php
									} else {
					?>
					<div class="row py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>" style="background-color:#F2F2F2;">
					<?php
									} // -- end of count($sub_subject) 
					?>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post">
							<span>
							<?php
								echo $this->main->count_sub_subject_post_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); //각 하위주제 별 포스트 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment">
							<span>
							<?php
								echo $this->main->count_sub_subject_comment_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 댓글 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply">
							<span>
							<?php
								echo $this->main->count_sub_subject_comment_reply_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 대댓글 수
							?>
							</span>
						</div>
					</div>
					<?php
								}  // -- end of for sub_subject
					?>
					<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
					<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_charts" style="background-color:#F2F2F2;">
						<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_pie">
							<!-- 포스트 등록 현황 파이 그래프 -->
						</div>
						<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_bar">
							<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
						</div>
					</div>
					<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
				</div>
				<!-- end of 메인 주제의 하위 주제 통계 -->
				<?php
						} else {	// 홀수 번째 행인 경우
				?>
				<div class="row border-bottom border-3 py-1 main_subject_statics" id="<?= $main_subject[$i]['main_subject_code'] ?>" subject_title="<?= $main_subject[$i]['main_subject_name'] ?>" onclick="open_sub_subject_statics(this,get_sub_subject_post_data,get_sub_subject_post_reply_data)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="cursor:pointer;">
					<div class="col align-middle text-center fw-bolder main_subject_name" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
					<div class="col align-middle text-center fw-bolder main_subject_post" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post">
						<span>
						<?php
							echo $this->main->count_main_subject_post_data($main_subject[$i]['main_subject_name']);
						?>
						</span>
					</div>
					<div class="col align-middle text-center fw-bolder main_subject_comment" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment">
					<span>
						<?php
							echo $this->main->count_main_subject_comment_data($main_subject[$i]['main_subject_name']);
						?>
						</span>
					</div>
					<div class="col align-middle text-center fw-bolder main_subject_comment_reply" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply">
					<span>
						<?php
							echo $this->main->count_main_subject_comment_reply_data($main_subject[$i]['main_subject_name']);
						?>
						</span>
					</div>
				</div>
				<!-- 메인 주제의 하위 주제 통계 -->
				<div id="<?= $main_subject[$i]['main_subject_code'] ?>_sub" style="display: none;"> 
					<?php
								for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
									if($j == (count($sub_subject)-1)){
					?>
					<div class="row border-bottom border-1 py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>">
					<?php
									} else {
					?>
					<div class="row py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>">
					<?php
									} // -- end of count($sub_subject) 
					?>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post">
							<span>
							<?php
								echo $this->main->count_sub_subject_post_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); //각 하위주제 별 포스트 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment">
							<span>
							<?php
								echo $this->main->count_sub_subject_comment_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 댓글 수
							?>
							</span>
						</div>
						<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply">
							<span>
							<?php
								echo $this->main->count_sub_subject_comment_reply_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 대댓글 수
							?>
							</span>
						</div>
					</div>
					<?php
								}  // -- end of for sub_subject
					?>
					<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
					<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_charts">
						<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_pie">
							<!-- 포스트 등록 현황 파이 그래프 -->
						</div>
						<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_bar">
							<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
						</div>
					</div>
					<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
				</div>
				<!-- end of 메인 주제의 하위 주제 통계 -->
				<?php
						} // -- end of if (짝수행)
					} // -- end of for main_subject
				?>
				
			</div>
			<div class="col-sm-12 col-lg-6" id="post_statics_pie" style="background-color:#D1E9F1;">
				<!-- 포스트 등록 현황 파이 그래프 -->
			</div>
			<div class="col-sm-12 col-lg-6" id="post_statics_bar" style="background-color:#D1E9F1;">
				<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
			</div>
		</div>
		<!-- end of 주제별 현황 -->
		<!-- 기간별 현황 -->
		<div class="row border-bottom border-1 mt-3 " id="period_statics">
			<div class=" d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold setting_title" >기간별 현황</p>
			</div>
			
			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<button class="nav-link active" id="daily_status_tab" data-bs-toggle="tab" data-bs-target="#daily_status" type="button" role="tab" aria-controls="daily_status" aria-selected="true">일별</button>
					<button class="nav-link" id="monthly_status_tab" data-bs-toggle="tab" data-bs-target="#monthly_status" type="button" role="tab" aria-controls="monthly_status" aria-selected="false">월별</button>
					<button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">연도별</button>
				</div>
			</nav>
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="daily_status" role="tabpanel" aria-labelledby="daily_status_tab">
						<form class="row row-cols-lg-auto g-3 align-items-center mt-1">
							<div class="col-12">
								<div class="input-group">
									<div class="input-group-text">시작일</div>
									<input class="form-control" type="text" id="start_day" value="<? echo date('Y-m-d',strtotime("-1 week")); ?>" >
								</div>
							</div>
							<div class="col-12">
								<div class="input-group">
									<div class="input-group-text">종료일</div>
									<input class="col-10 form-control" type="text" id="end_day" value="<? echo date('Y-m-d',time()); ?>" >
								</div>
							</div>
							<div class="col-12">
								<button type="submit" class="btn btn-secondary" onclick="daily_status(draw_daily_chart);">검색</button>
							</div>
						</form>
						<br>
						<div class="col-sm-12 col-lg-12" id="daily_status_content">
							<div class="row border-bottom border-3 py-1">
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">#</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">포스트</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">댓글</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">대댓글</div>
							</div>
							<div class="row border-bottom border-3 py-1 " style="background-color:#e6f7fa;">
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;"><span style="color:blue; font-weight:bolder;">전체</span></div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
									<span style="color:blue; font-weight:bolder;">
									<?php
										echo $this->main->count_all_data();  // 전체 포스트 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
									<span style="color:blue; font-weight:bolder;">
									<?php
										echo $this->main->count_all_comment();  // 전체 댓글 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
									<span style="color:blue; font-weight:bolder;">
									<?php
										echo $this->main->count_all_comment_reply();  // 전체 대댓글 수
									?>
									</span>
								</div>
							</div>
							
							<?php
								for($i=0; $i < count($main_subject); $i++) {
									$sub_subject = $this->main->get_sub_subject($main_subject[$i]['main_subject_code']); // 각 메인주제의 하위주제를 가져온다.
									if(($i+1)%2 == 0) {		// 짝수 번째 행인 경우
							?>
							<div class="row border-bottom border-3 py-1 main_subject_statics" id="<?= $main_subject[$i]['main_subject_code'] ?>" subject_title="<?= $main_subject[$i]['main_subject_name'] ?>" onclick="open_sub_subject_statics(this,get_sub_subject_post_data,get_sub_subject_post_reply_data)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="background-color:#F2F2F2; cursor:pointer;">
								<div class="col align-middle text-center fw-bolder main_subject_name" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
								<div class="col align-middle text-center fw-bolder main_subject_post" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post">
									<span>
									<?php
										echo $this->main->count_main_subject_post_data($main_subject[$i]['main_subject_name']); //각 메인주제 별 포스트 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment">
									<span>
									<?php
										echo $this->main->count_main_subject_comment_data($main_subject[$i]['main_subject_name']); // 각 메인주제 별 댓글 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment_reply" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply">
									<span>
									<?php
										echo $this->main->count_main_subject_comment_reply_data($main_subject[$i]['main_subject_name']); // 각 메인주제 별 대댓글 수
									?>
									</span>
								</div>
							</div>
							<!-- 메인 주제의 하위 주제 통계 -->
							<div id="<?= $main_subject[$i]['main_subject_code'] ?>_sub" style="display: none;"> 
								<?php 
											for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
												if($j == (count($sub_subject)-1)){
								?>
								<div class="row border-bottom border-1 py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>" style="background-color:#F2F2F2;">
								<?php
												} else {
								?>
								<div class="row py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>" style="background-color:#F2F2F2;">
								<?php
												} // -- end of count($sub_subject) 
								?>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post">
										<span>
										<?php
											echo $this->main->count_sub_subject_post_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); //각 하위주제 별 포스트 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 댓글 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_reply_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 대댓글 수
										?>
										</span>
									</div>
								</div>
								<?php
											}  // -- end of for sub_subject
								?>
								<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
								<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_charts" style="background-color:#F2F2F2;">
									<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_pie">
										<!-- 포스트 등록 현황 파이 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_bar">
										<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
									</div>
								</div>
								<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
							</div>
							<!-- end of 메인 주제의 하위 주제 통계 -->
							<?php
									} else {	// 홀수 번째 행인 경우
							?>
							<div class="row border-bottom border-3 py-1 main_subject_statics" id="<?= $main_subject[$i]['main_subject_code'] ?>" subject_title="<?= $main_subject[$i]['main_subject_name'] ?>" onclick="open_sub_subject_statics(this,get_sub_subject_post_data,get_sub_subject_post_reply_data)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="cursor:pointer;">
								<div class="col align-middle text-center fw-bolder main_subject_name" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
								<div class="col align-middle text-center fw-bolder main_subject_post" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post">
									<span>
									<?php
										echo $this->main->count_main_subject_post_data($main_subject[$i]['main_subject_name']);
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment">
								<span>
									<?php
										echo $this->main->count_main_subject_comment_data($main_subject[$i]['main_subject_name']);
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment_reply" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply">
								<span>
									<?php
										echo $this->main->count_main_subject_comment_reply_data($main_subject[$i]['main_subject_name']);
									?>
									</span>
								</div>
							</div>
							<!-- 메인 주제의 하위 주제 통계 -->
							<div id="<?= $main_subject[$i]['main_subject_code'] ?>_sub" style="display: none;"> 
								<?php
											for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
												if($j == (count($sub_subject)-1)){
								?>
								<div class="row border-bottom border-1 py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>">
								<?php
												} else {
								?>
								<div class="row py-1 sub_subject_statics" id="<?= $sub_subject[$j]['sub_subject_code'] ?>">
								<?php
												} // -- end of count($sub_subject) 
								?>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post">
										<span>
										<?php
											echo $this->main->count_sub_subject_post_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); //각 하위주제 별 포스트 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 댓글 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_reply_data($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name']); // 각 하위주제 별 대댓글 수
										?>
										</span>
									</div>
								</div>
								<?php
											}  // -- end of for sub_subject
								?>
								<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
								<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_charts">
									<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_pie">
										<!-- 포스트 등록 현황 파이 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-6" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_statics_bar">
										<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
									</div>
								</div>
								<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
							</div>
							<!-- end of 메인 주제의 하위 주제 통계 -->
							<?php
									} // -- end of if (짝수행)
								} // -- end of for main_subject
							?>
							
						</div>
						<div class="col-sm-12 col-lg-6" id="post_statics_pie" style="background-color:#D1E9F1;">
							<!-- 포스트 등록 현황 파이 그래프 -->
						</div>
						<div class="col-sm-12 col-lg-6" id="post_statics_bar" style="background-color:#D1E9F1;">
							<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
						</div>
						1. 선택한 기간의 주제별 포스트 및 댓글 등록 데이터 테이블</br>
						2. 선택한 기간의 주제별 포스트 등록 비율 파이차트</br>
						3. 선택한 기간의 주제별 포스트 등록 추이 선 차트</br>
						4. 선택한 기간의 주제별 포스트 댓글 추이 선 차트</br>
						5. 인기 포스트 Top 10</br>
					</div>
					<div class="tab-pane fade" id="monthly_status" role="tabpanel" aria-labelledby="monthly_status_tab">
						<form class="row row-cols-lg-auto g-3 align-items-center mt-1">
							<div class="col-12">
								<div class="input-group">
									<div class="input-group-text">연도</div>
									<input class="form-control" type="number" id="year" name="year" min="2020" max="2050" value="<?php echo explode('-',date('Y-m-d',time()))[0]?>" >
								</div>
							</div>
							<div class="col-12">
								<div class="input-group">
									<div class="input-group-text">월</div>
									<input class="col-10 form-control" type="number" id="month" name="month" min="1" max="12" value="<?php echo explode('-',date('Y-m-d',time()))[1] ?>" >
								</div>
							</div>
							<div class="col-12">
								<button type="submit" class="btn btn-secondary" onclick="monthly_status(draw_monthly_chart);">검색</button>
							</div>
						</form>
						<br>
						1. 선택한 기간의 주제별 포스트 및 댓글 등록 데이터 테이블</br>
						2. 선택한 기간의 주제별 포스트 등록 비율 파이차트</br>
						3. 선택한 기간의 주제별 포스트 등록 추이 선 차트</br>
						4. 선택한 기간의 주제별 포스트 댓글 추이 선 차트</br>
						5. 인기 포스트 Top 10</br>
					</div>
					<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
					<br>
						1. 선택한 기간의 주제별 포스트 및 댓글 등록 데이터 테이블</br>
						2. 선택한 기간의 주제별 포스트 등록 비율 파이차트</br>
						3. 선택한 기간의 주제별 포스트 등록 추이 선 차트</br>
						4. 선택한 기간의 주제별 포스트 댓글 추이 선 차트</br>
						5. 인기 포스트 Top 10</br>
					</div>
				</div>
		</div>
		
		
	</div>
	<!-- --end of main content -->

	<script>
		$(function(){
			var post_statics_data = '';  // 포스트 등록 현황 데이터 변수를 전역 변수로 선언한다.
			var comment_statics_data = '';  // 댓글 등록 현황 데이터 변수를 전역 변수로 선언한다.
			var comment_reply_statics_data = '';  // 댓글에 대한 댓글 등록 현황 데이터 변수를 전역 변수로 선언한다.
			var subject_name_arr = '';  // 각 주제 이름 데이터 변수를 전역 변수로 선언한다.

			// 포스트 등록 비율 현황 파이 그래프를 그린다.
			get_main_subject_post_data(post_statics_pie_draw);

			// 포스트 등록 및 댓글 현황 막대 그래프를 그린다.
			get_main_subject_post_reply_data(post_statics_bar_draw);
			
			// datepicker 설정
			$.datepicker.setDefaults($.datepicker.regional['ko']);
				$( "#start_day" ).datepicker({
				// changeMonth: true, 
				// changeYear: true,
				nextText: '다음 달',
				prevText: '이전 달', 
				dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
				dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'], 
				monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				dateFormat: "yy-mm-dd",
				maxDate: 0,                       // 선택할수있는 최소날짜, ( 0 : 오늘 이후 날짜 선택 불가)
				yearRange: 'c-1:c+10',
				closeText: '닫기',
				onClose: function( selectedDate ) {    
					//시작일(startDate) datepicker가 닫힐때
					//종료일(endDate)의 선택할수있는 최소 날짜(minDate)를 선택한 시작일로 지정
					$("#end_day").datepicker( "option", "minDate", selectedDate );
				}    

			});
			$( "#end_day" ).datepicker({
				// changeMonth: true, 
				// changeYear: true,
				nextText: '다음 달',
				prevText: '이전 달', 
				dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
				dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'], 
				monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				dateFormat: "yy-mm-dd",
				maxDate: 0,                       // 선택할수있는 최대날짜, ( 0 : 오늘 이후 날짜 선택 불가)
				yearRange: 'c-1:c+10',
				closeText: '닫기',
				onClose: function( selectedDate ) {    
					// 종료일(endDate) datepicker가 닫힐때
					// 시작일(startDate)의 선택할수있는 최대 날짜(maxDate)를 선택한 시작일로 지정
					$("#start_day").datepicker( "option", "maxDate", selectedDate );
				}    

			});

		});

	</script>			
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
