<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
		<symbol id="home" viewBox="0 0 16 16">
			<path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
		</symbol>
		<symbol id="speedometer" viewBox="0 0 16 16">
			<path d="M8 2a.5.5 0 0 1 .5.5V4a.5.5 0 0 1-1 0V2.5A.5.5 0 0 1 8 2zM3.732 3.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 8a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 7.31A.91.91 0 1 0 8.85 8.569l3.434-4.297a.389.389 0 0 0-.029-.518z"></path>
			<path fill-rule="evenodd" d="M6.664 15.889A8 8 0 1 1 9.336.11a8 8 0 0 1-2.672 15.78zm-4.665-4.283A11.945 11.945 0 0 1 8 10c2.186 0 4.236.585 6.001 1.606a7 7 0 1 0-12.002 0z"></path>
		</symbol>
		<symbol id="speedometer2" viewBox="0 0 16 16">
    		<path d="M8 4a.5.5 0 0 1 .5.5V6a.5.5 0 0 1-1 0V4.5A.5.5 0 0 1 8 4zM3.732 5.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 10a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 10zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 9.31a.91.91 0 1 0 1.302 1.258l3.434-4.297a.389.389 0 0 0-.029-.518z"></path>    
			<path fill-rule="evenodd" d="M0 10a8 8 0 1 1 15.547 2.661c-.442 1.253-1.845 1.602-2.932 1.25C11.309 13.488 9.475 13 8 13c-1.474 0-3.31.488-4.615.911-1.087.352-2.49.003-2.932-1.25A7.988 7.988 0 0 1 0 10zm8-7a7 7 0 0 0-6.603 9.329c.203.575.923.876 1.68.63C4.397 12.533 6.358 12 8 12s3.604.532 4.923.96c.757.245 1.477-.056 1.68-.631A7 7 0 0 0 8 3z"></path>
		<symbol id="write" viewBox="0 0 16 16">
			<path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
		</symbol>
		<symbol id="post_manage" viewBox="0 0 16 16">
			<path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
  			<path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5L9.5 0zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
		</symbol>
		<symbol id="setting" viewBox="0 0 16 16">
			<path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
  			<path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
		</symbol>
		<symbol id="search" viewBox="0 0 16 16">
			<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
		</symbol>
		<symbol id="key" viewBox="0 0 16 16">
			<path d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z"/>
			<path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
		</symbol>
		<symbol id="statics" viewBox="0 0 16 16">
			<path d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1V2zm1 12h2V2h-2v12zm-3 0V7H7v7h2zm-5 0v-3H2v3h2z"/>
		</symbol>
		<symbol id="lock" viewBox="0 0 16 16">
			<path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2zM5 8h6a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1z"/>
		</symbol>
	</svg>

	<header>
	<!-- 상단 메뉴 영역(반응형 햄버거 메뉴) -->
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<div class="container-fluid">
				<a class="navbar-brand" href="/main/enter">cheoleeblog admin</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="true" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarCollapse">
					<ul class="navbar-nav me-auto mb-2 mb-md-0">
					<li class="nav-item text-truncate">
						<a class="nav-link" aria-current="page" name="main" href="/main/enter">
							<svg class="bi me-1" width="16" height="16"><use xlink:href="#speedometer2"/></svg>
							데쉬보드
						</a>
					</li>
					<li class="nav-item text-truncate">
						<a class="nav-link" name="postwrite" href="/postwrite/write">
							<svg class="bi me-1" width="16" height="16"><use xlink:href="#write"/></svg>
							포스트 작성
						</a>
					</li>
					<li class="nav-item text-truncate">
						<a class="nav-link" name="postmanage" href="/postmanage/manage">
							<svg class="bi me-1" width="16" height="16"><use xlink:href="#post_manage"/></svg>
							포스트 관리
						</a>
					</li>
					<li class="nav-item text-truncate">
						<a class="nav-link" name="setting" href="/setting/index">
							<svg class="bi me-1" width="16" height="16"><use xlink:href="#setting"/></svg>
							환경 설정
						</a>
					</li>
					</ul>
					
					<ul class="navbar-nav">
						<li class="nav-item text-truncate">
							<a class="nav-link" aria-current="page" name="accountmanage" href="/accountmanage/info">
								<svg class="bi me-1" width="16" height="16"><use xlink:href="#key"/></svg>
								계정 관리
							</a>
						</li>
					</ui>

					<form class="d-flex" action="/main/logout">

						<button class="btn btn-outline-success" type="submit">logout</button>
					</form>
				</div>
			</div>
		</nav>
	</header>
	
					
	<!-- main -->
	<main class="flex-shrink-0">
		<div class="container-lg" style="margin-top:56px;">
			<div class="row">
				<!-- content -->
				<div class="row" style="padding: 0; margin:auto; ">
					<!-- breadcrumb(사이트 이동 경로) -->
					<nav class="bg-light" aria-label="breadcrumb" style="padding-top:15px;">
						<div class="d-flex justify-content-between align-items-center">
							<!-- clock -->
							<div id="clock" class="d-flex"></div>
							<!-- --end of clock -->
							<div class="d-flex align-items-center">
								<!-- <span class="border rounded bg-secondary bg-gradient text-white" style="margin-bottom: 1rem; margin-right: 0.5rem; padding: 0 10px; font-size:0.85rem">위치</span> -->
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/main/enter">데쉬보드</a></li>
								</ol>
							</div>
						</div>
					</nav>
					<!-- --end of breadcrumb -->
					
					<!-- sidebar menu -->
					<div id="sidebarMenu" class="sidebar-sticky col-sm-12 col-lg-2 d-lg-block bg-light collapse" >
						<div class="d-flex flex-column bg-light" >
							<!-- <a href="/" class="d-flex align-items-center pb-3 mb-3 link-dark text-decoration-none border-bottom">
								<svg class="bi me-2" width="30" height="24"><use xlink:href="#bootstrap"/></svg>
								<span class="fs-5 fw-semibold">Collapsible</span>
							</a> -->
							<ul class="nav nav-pills flex-column mb-auto">
								<li class="nav-item">
									<a name="main" href="/main/enter" class="nav-link link-dark fw-bold" onclick="nav_click(this);" style="font-size:0.875em;">
										<svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"/></svg>
										데쉬보드
									</a>
								</li>
								<li class="nav-item">
									<a name="postwrite" href="/postwrite/write" class="nav-link link-dark fw-bold"  onclick="nav_click(this);" style="font-size:0.875em;">
										<svg class="bi me-2" width="16" height="16"><use xlink:href="#write"/></svg>
										포스트 작성
									</a>
								</li>
								<li class="nav-item">
									<a name="postmanage" href="/postmanage/manage" class="nav-link link-dark fw-bold" onclick="nav_click(this);" style="font-size:0.875em;">
										<svg class="bi me-2" width="16" height="16"><use xlink:href="#post_manage"/></svg>
										포스트 관리
									</a>
								</li>
								<li class="nav-item">
									<a name="setting" href="/setting/index" class="nav-link link-dark fw-bold" onclick="nav_click(this);" style="font-size:0.875em;">
										<svg class="bi me-2" width="16" height="16"><use xlink:href="#setting"/></svg>
										환경 설정
									</a>
								</li>
							</ul>
							
						</div>
					</div>
					<!-- ---end of sidebar menu -->
	<script>
		$(function(){
			var c_url = document.URL;  // 현재 주수창의 URL을 가져온다.
			var url_arr = c_url.split('/');
			console.log(url_arr);
			// var u_length = url_arr.length ;
			var c_menu = url_arr[3];
			console.log(c_menu);

			//좌측 nav 요소 처리
			$('#sidebarMenu').find('a').each(function(index){
				var name_tmp= $(this).attr('name');
				console.log(c_menu+','+name_tmp);
				if(c_menu == name_tmp){
				$(this).css({'backgroundColor' : '#007bff',
							'color' : '#fff'
							});
				$(this).attr('aria-current','page');
				} 
				else {
				// $(this).css('color','#333');
				$(this).removeAttr('aria-current');
				}
			});
			
			//상단 nav 요소 처리
			$('#navbarCollapse').find('a').each(function(index){
				var name_tmp= $(this).attr('name');
				console.log(c_menu+','+name_tmp);
				if(c_menu == name_tmp){
				$(this).attr('class','nav-link active');
				$(this).attr('aria-current','page');
				} 
				else {
				$(this).attr('class','nav-link');
				$(this).removeAttr('aria-current');
				}
			});

			
		});

	</script>