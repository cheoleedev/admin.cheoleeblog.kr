<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$nocache = strtotime('now');
?>
<!DOCTYPE html>
	<html lang="ko" class="h-100">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>cheoleeblog admin</title>
			
		<!-- favicon 적용  -->
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		
		<!-- Bootstrap CSS -->
		<!-- CSS only -->
		<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"> -->
		<link rel="stylesheet" type="text/css" href="/css/bootstrap5/bootstrap.min.css">
		
		<!-- custom CSS -->
		<link rel="stylesheet" type="text/css" href="/css/common_style.css">

		<!-- jQuery & Bootstrap JS  -->
		<script
			src="https://code.jquery.com/jquery-3.6.0.js"
			integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
			crossorigin="anonymous">
		</script>
		<script src="/js/common.js?<?= $nocache?>"></script>
		<script src="/js/sidebars.js?<?= $nocache?>"></script> <!-- 사이드 메뉴 관련 자바스크립트 -->
		<!-- Option 1: Bootstrap Bundle with Popper -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
		
		<!-- viewer.js -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.7/viewer.min.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.7/viewer.min.js"></script>

		<!-- APEXCHARTS 오픈소스 라이브러리 참조 -->
		<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

		<!-- datepicker 오픈소스 라이브러리 참조  -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>  
        <script src="https://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>

		<!-- monthpicker 플러그 인 -->
		<script src="/js/jquery.mtz.monthpicker.js?<?= $nocache?>"></script>

		<!-- ckeditor 4 & ckfinder 3-->
		<script src="/js/lib/ckeditor/ckeditor.js?<?= $nocache?>"></script>
		<script src="/js/lib/ckfinder/ckfinder.js?<?= $nocache?>"></script>
		<!-- <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script> -->
		
		<!-- theme CSS -->
		<link rel="stylesheet" type="text/css" href="/css/sidebars.css"> <!-- 사이드 메뉴 관련 스타일시트 -->
		<link rel="stylesheet" type="text/css" href="/css/sticky-footer.css">

	</head>
	<!-- <body class="d-flex flex-column h-100" onload="LoadPage();"> -->
	<body class="d-flex flex-column h-100 bg-light">