<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
	
						<div class="col-sm-12 col-lg-12" id="monthly_statics_content">
							<div class="row border-bottom border-3 py-1">
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">#</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">포스트</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">댓글</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">대댓글</div>
							</div>
							<div class="row border-bottom border-3 py-1 " style="background-color:#e6f7fa;">
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;"><span style="color:blue; font-weight:bolder;">전체</span></div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
									<span style="color:blue; font-weight:bolder;">
									<?php
										echo $this->main->count_all_data_monthly_period($start_day,$end_day);  // 전체 포스트 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
									<span style="color:blue; font-weight:bolder;">
									<?php
										echo $this->main->count_all_comment_monthly_period($start_day,$end_day);  // 전체 댓글 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">
									<span style="color:blue; font-weight:bolder;">
									<?php
										echo $this->main->count_all_comment_reply_monthly_period($start_day,$end_day);  // 전체 대댓글 수
									?>
									</span>
								</div>
							</div>
							
							<?php
								for($i=0; $i < count($main_subject); $i++) {
									$sub_subject = $this->main->get_sub_subject($main_subject[$i]['main_subject_code']); // 각 메인주제의 하위주제를 가져온다.
									if(($i+1)%2 == 0) {		// 짝수 번째 행인 경우
							?>
							<div class="row border-bottom border-3 py-1 main_subject_statics_monthly_period" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period" subject_name="<?= $main_subject[$i]['main_subject_name'] ?>" subject_code="<?= $main_subject[$i]['main_subject_code'] ?>" onclick="open_sub_subject_statics_monthly_period(this,get_sub_subject_post_data_monthly_period,get_sub_subject_post_reply_data_monthly_period,get_sub_subject_post_reply_data_line_monthly_period)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="background-color:#F2F2F2; cursor:pointer;">
								<div class="col align-middle text-center fw-bolder main_subject_name_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name_monthly_period" color="<?= $main_subject[$i]['color'] ?>"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
								<div class="col align-middle text-center fw-bolder main_subject_post_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_monthly_period">
									<span>
									<?php
										echo $this->main->count_main_subject_post_data_monthly_period($main_subject[$i]['main_subject_name'],$start_day,$end_day); //각 메인주제 별 포스트 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_monthly_period">
									<span>
									<?php
										echo $this->main->count_main_subject_comment_data_monthly_period($main_subject[$i]['main_subject_name'],$start_day,$end_day); // 각 메인주제 별 댓글 수
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment_reply_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply_monthly_period">
									<span>
									<?php
										echo $this->main->count_main_subject_comment_reply_data_monthly_period($main_subject[$i]['main_subject_name'],$start_day,$end_day); // 각 메인주제 별 대댓글 수
									?>
									</span>
								</div>
							</div>
							<!-- [선택 기간] 메인 주제의 하위 주제 통계 -->
							<div id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub" style="display: none;"> 
								<?php 
											for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
												if($j == (count($sub_subject)-1)){
								?>
								<div class="row border-bottom border-1 py-1 sub_subject_statics_monthly_period" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_monthly_period" style="background-color:#F2F2F2;">
								<?php
												} else {
								?>
								<div class="row py-1 sub_subject_statics_monthly_period" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_monthly_period" style="background-color:#F2F2F2;">
								<?php
												} // -- end of count($sub_subject) 
								?>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name_monthly_period" color="<?= $sub_subject[$j]['color'] ?>"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post_monthly_period">
										<span>
										<?php
											echo $this->main->count_sub_subject_post_data_monthly_period($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name'],$start_day,$end_day); //각 하위주제 별 포스트 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_monthly_period">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_data_monthly_period($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name'],$start_day,$end_day); // 각 하위주제 별 댓글 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply_monthly_period">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_reply_data_monthly_period($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name'],$start_day,$end_day); // 각 하위주제 별 대댓글 수
										?>
										</span>
									</div>
								</div>
								<?php
											}  // -- end of for sub_subject
								?>
								<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
								<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_charts" style="background-color:#F2F2F2;">
									<div class="col-sm-12 col-lg-6">
										<!-- 포스트 등록 현황 파이 차트 -->
										<div>
											<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [선택 기간] 포스트 등록 비율 </p>
										</div>
										<div id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_post_statics_pie"></div>
									</div>
									<div class="col-sm-12 col-lg-6" style="margin-top:7px;" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_post_statics_bar">
										<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_post_statics_line">
										<!-- 포스트 등록 현황 라인 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_comment_statics_line">
										<!-- 댓글 등록 현황 라인 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_comment_reply_statics_line">
										<!-- 대댓글 등록 현황 라인 그래프 -->
									</div>
								</div>
								<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
							</div>
							<!-- end of 메인 주제의 하위 주제 통계 -->
							<?php
									} else {	// 홀수 번째 행인 경우
							?>
							<div class="row border-bottom border-3 py-1 main_subject_statics_monthly_period" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period" subject_name="<?= $main_subject[$i]['main_subject_name'] ?>" subject_code="<?= $main_subject[$i]['main_subject_code'] ?>" onclick="open_sub_subject_statics_monthly_period(this,get_sub_subject_post_data_monthly_period,get_sub_subject_post_reply_data_monthly_period,get_sub_subject_post_reply_data_line_monthly_period)" sub_status="close" sub_count="<?= count($sub_subject) ?>" style="cursor:pointer;">
								<div class="col align-middle text-center fw-bolder main_subject_name_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_name_monthly_period" color="<?= $main_subject[$i]['color'] ?>"><span><?= $main_subject[$i]['main_subject_name'] ?></span></div>
								<div class="col align-middle text-center fw-bolder main_subject_post_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_post_monthly_period">
									<span>
									<?php
										echo $this->main->count_main_subject_post_data_monthly_period($main_subject[$i]['main_subject_name'],$start_day,$end_day);
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_monthly_period">
								<span>
									<?php
										echo $this->main->count_main_subject_comment_data_monthly_period($main_subject[$i]['main_subject_name'],$start_day,$end_day);
									?>
									</span>
								</div>
								<div class="col align-middle text-center fw-bolder main_subject_comment_reply_monthly_period" style="font-size:.75rem;" id="<?= $main_subject[$i]['main_subject_code'] ?>_comment_reply_monthly_period">
								<span>
									<?php
										echo $this->main->count_main_subject_comment_reply_data_monthly_period($main_subject[$i]['main_subject_name'],$start_day,$end_day);
									?>
									</span>
								</div>
							</div>
							<!-- [선택 기간] 메인 주제의 하위 주제 통계 -->
							<div id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub" style="display: none;"> 
								<?php
											for($j=0; $j < count($sub_subject); $j++ ){ // 각 하위주제별 정보를 추가한다.
												if($j == (count($sub_subject)-1)){
								?>
								<div class="row border-bottom border-1 py-1 sub_subject_statics_monthly_period" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_monthly_period">
								<?php
												} else {
								?>
								<div class="row py-1 sub_subject_statics_monthly_period" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_monthly_period">
								<?php
												} // -- end of count($sub_subject) 
								?>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_name" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_name_monthly_period" color="<?= $sub_subject[$j]['color'] ?>"><span><?= $sub_subject[$j]['sub_subject_name'] ?></span></div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_post" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_post_monthly_period">
										<span>
										<?php
											echo $this->main->count_sub_subject_post_data_monthly_period($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name'],$start_day,$end_day); //각 하위주제 별 포스트 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_comment" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_monthly_period">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_data_monthly_period($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name'],$start_day,$end_day); // 각 하위주제 별 댓글 수
										?>
										</span>
									</div>
									<div class="col align-middle text-center <?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_sub_subject_comment_reply" style="font-size:.75rem;" id="<?= $sub_subject[$j]['sub_subject_code'] ?>_comment_reply_monthly_period">
										<span>
										<?php
											echo $this->main->count_sub_subject_comment_reply_data_monthly_period($main_subject[$i]['main_subject_name'], $sub_subject[$j]['sub_subject_name'],$start_day,$end_day); // 각 하위주제 별 대댓글 수
										?>
										</span>
									</div>
								</div>
								<?php
											}  // -- end of for sub_subject
								?>
								<!-- 해당 메인 주제의 하위 주제 통계 차트 영역 -->
								<div class="row" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_charts">
									<div class="col-sm-12 col-lg-6">
										<!-- 포스트 등록 현황 파이 차트 -->
										<div>
											<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [선택 기간] 포스트 등록 비율 </p>
										</div>
										<div id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_post_statics_pie"></div>
									</div>
									<div class="col-sm-12 col-lg-6" style="margin-top:7px;" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_post_statics_bar">
										<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_post_statics_line">
										<!-- 포스트 등록 현황 라인 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_comment_statics_line">
										<!-- 댓글 등록 현황 라인 그래프 -->
									</div>
									<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="<?= $main_subject[$i]['main_subject_code'] ?>_monthly_period_comment_reply_statics_line">
										<!-- 대댓글 등록 현황 라인 그래프 -->
									</div>
								</div>
								<!-- end of 메인 주제의 하위 주제 통계 차트 영역 -->
							</div>
							<!-- end of 메인 주제의 하위 주제 통계 -->
							<?php
									} // -- end of if (짝수행)
								} // -- end of for main_subject
							?>
							
						</div>

						<div class="col-sm-12 col-lg-6" style="background-color:#e6f7fa; height:305px;">
							<!-- 포스트 등록 현황 파이 그래프 -->
							<div>
								<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [선택 기간] 포스트 등록 비율 </p>
							</div>
							<div id="post_statics_pie_monthly_period"></div>
						</div>
						<div class="col-sm-12 col-lg-6 px-0" id="post_ranking_list_monthly_period" style="background-color:#e6f7fa; height:305px;">
							<!-- 포스트 인기 순위 리스트 -->
							<p class="fw-bold mb-1" style="font-size:.9rem; margin-top:7px;"> [선택 기간] 포스트 인기 순위 </p>
							<div class="row mx-0">
								<div class="col-2 text-center">
									<span class="info_text fw-bold">순위</span>
								</div>
								<div class="col-6 text-ellipsis text-center">
									<span class="info_text fw-bold">제목</span>
								</div>
								<div class="col-4 text-ellipsis text-center">
									<span class="info_text fw-bold">분야</span>
								</div>
							</div>
							<div class="list-group" style="overflow:auto; height:230px;" id="post_ranking_list_item_monthly_period_wrap">
								<div id="post_ranking_list_item_monthly_period">
									<!-- 포스트 순위 리스트 표시 영역-->
								</div>
								<!-- <div class="d-flex justify-content-center w-100">
									<button type="button" class="btn btn-outline-secondary btn-sm py-0 mt-2 mb-1" mark="monthly_period" onclick="add_post_list(this)"><span class="info_text">더보기</span></button>
								</div> -->
							</div>
						</div>
						<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="post_statics_bar_monthly_period" style="background-color:#e6f7fa;">
							<!-- 포스트 및 댓글 등록 현황 막대 그래프 -->
						</div>
						<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="post_statics_line_monthly_period" style="background-color:#e6f7fa;">
							<!-- 포스트 등록 현황 라인 그래프 -->
						</div>
						<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="comment_statics_line_monthly_period" style="background-color:#e6f7fa;">
							<!-- 댓글 등록 현황 라인 그래프 -->
						</div>
						<div class="col-sm-12 col-lg-12 d-flex align-items-center" id="comment_reply_statics_line_monthly_period" style="background-color:#e6f7fa;">
							<!-- 대댓글 등록 현황 라인 그래프 -->
						</div>