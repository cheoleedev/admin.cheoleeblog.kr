<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
	<html lang="ko" class="h-100">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>cheoleeblog admin</title>
				
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="/css/bootstrap5/bootstrap.min.css">
		
		<!-- custom CSS -->
		<link rel="stylesheet" type="text/css" href="/css/common_style.css">

		<!-- jQuery & Bootstrap JS  -->
		<script
			src="https://code.jquery.com/jquery-3.6.0.js"
			integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
			crossorigin="anonymous">
		</script>
		<script src="/js/common.js"></script>	
		<!-- Option 1: Bootstrap Bundle with Popper -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	
	<!-- custom CSS -->
	<link rel="stylesheet" type="text/css" href="/css/login.css">
	
	</head>
	<body class="text-center">
	<?= $this->session->flashdata('message') ?>
		<!-- 로그인 폼-->
		<main class="form-login">
		
		<form id="login_form" method="post" action="/main/login_validation">
			<!-- <img class="mb-4" src="/cheoleeblog_admin/img/bootstrap-fill.svg" alt="" width="72" height="57"> -->
			<p class="fs-1 fw-bolder">cheoleeblog admin</p>
			<!-- <h1 class="h5 mb-3 fw-normal">Please sign in</h1> -->

			<div class="form-floating">
			<input type="text" class="form-control" name="admin_id" id="admin_id" placeholder="id">
			<label for="id">Id</label>
			</div>
			<div class="form-floating">
			<input type="password" class="form-control" name="passwd" id="passwd" placeholder="Password">
			<label for="passwd">Password</label>
			</div>

			<div class="checkbox mb-3">
			<label>
				<!-- <input type="checkbox" value="remember-me"> Remember me -->
			</label>
			</div>
			<button class="w-100 btn btn-lg btn-primary" type="submit">Log in</button>

		</form>
		<p class="mt-5 mb-3 text-muted">Copyright 2021. cheoleeblog.<br> All rights reserved.</p>
		
		</main>

  </body>

</html>