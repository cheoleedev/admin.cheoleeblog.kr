
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>

		
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-light" >
		<?= $this->session->flashdata('message'); ?>	
		<div class="pt-2 fs-5 fw-bold">
				<svg class="bi me-1 fs-5" width="16" height="16"><use xlink:href="#post_manage"/></svg>
				<span class="">포스트 보기</span>
		</div>
		<hr>
		<!-- <div class="row d-flex align-items-center justify-content-between"> -->
		<div class="d-flex align-items-center mx-0 p-0 text-truncate">
			<span>
				<img src="<?= '/img/post_profile/'.$thumbnail ?>" class="img-thumbnail" width="30" alt="포스트 썸네일">
				<?= '['.$foreword.'] '.$title ?>
			</span>
		</div>
		<div class="d-flex align-items-center justify-content-between">
			<small class="mr-2">
				<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">포스트 번호</i>
				<?= ' : '.$slug ?>
			</small>
			
			<small class="mr-2 float-end">
				<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">작성자</i>
				<?= ' : '.$writer_id ?>
			</small>
		</div>	
		<!-- </div> -->
		<hr class="my-1">
		<div class="d-flex align-items-center justify-content-between">
			<small class="mr-2">
				<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">주제</i>
				&nbsp;:&nbsp;<span class="info_text" id="main_subject_name_<?= $slug ?>"><?= $main_subject_name ?></span>&nbsp;>&nbsp;<span class="info_text" id="sub_subject_name_<?= $slug ?>"><?= $sub_subject_name ?></span>
			</small>
			<small class="mr-2">
				<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">조회수</i>
				<?= ' : '.$view_count ?>
			</small>
			<small class="mr-2">
				<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">등록일</i>
				<?= ' : '.date('Y-m-d', strtotime($reg_date))  ?>
			</small>
		</div>
		<hr class="shadow mt-1">
		<div class="viewer-items m-0 p-0">
		<?= $content ?>
		</div>
		

		<!-- 목록 보기, 편집, 삭제 요소 추가 -->
		<div class="d-flex justify-content-end mt-3 pb-1">
			<a href="<?= '/postmanage/manage/?page='.$cur_page.'&filter='.$filter.'&search='.$search ?>" class="text-dark text-decoration-none p-0 m-0">
				<img src="<?= '/img/file-earmark-text.svg' ?>" class="img-thumbnail me-1" width="40" alt="목록 보기" title="목록 보기">
			</a>
			<a href="<?= '/postmanage/edit/' . $slug .'?page='.$cur_page.'&filter='.$filter.'&search='.$search?>" class="text-dark text-decoration-none p-0 m-0">
				<img src="<?= '/img/pencil-square.svg' ?>" class="img-thumbnail me-1" width="40" alt="편집" title="편집">
			</a>
			<a href="<?= '/postmanage/delete/' . $slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search ?>" class="text-dark text-decoration-none p-0 m-0">
				<img src="<?= '/img/trash.svg' ?>" class="img-thumbnail" width="40" alt="삭제" title="삭제">
			</a>
		</div>
		<hr>
		<!-- 코멘트 입력 및 저장 요소 -->
		<div class="mt-3 pb-1">
			<div class="row mb-2">
				<div class="col-3 pe-0">
					<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_nickname_<?= $slug ?>" name="comment_nickname_<?= $slug ?>" value="운영자">
				</div>
				<div class="col-3 pe-0">
					<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_passwd_<?= $slug ?>" name="comment_passwd_<?= $slug ?>" value="">
				</div>
				<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">
					<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_<?= $slug ?>" name="is_secret_<?= $slug ?>">
					<label class="form-check-label" for="is_secret_<?= $slug ?>">
						<span style="font-size:.75rem;">비밀글</span>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="col-9 form-floating d-flex justify-content-center pe-0">
					<textarea class="form-control" placeholder="댓글 남기기" id="post_comment_<?= $slug ?>" name="post_comment_<?= $slug ?>" style="height: 100px; width:98%;"></textarea>
					<label class="ps-4" for="post_comment_<?= $slug ?>">댓글 남기기</label>
				</div>
				<div class="col-3 d-grid ps-0">
					<button type="button" class="btn btn-outline-primary" slug="<?= $slug ?>" onclick="save_comment(this)" style="width:95%;" id="comment_save_btn_<?= $slug ?>">
						<span class="info_text" title="저장">
							<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
								<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
							</svg>
						</span>
					</button>
				</div>
			</div>
		</div>

		<!-- 코멘트 리스트 -->
		<div id="comment_list_<?= $slug ?>" status="close">
			<?php
				$comments = $this->post->get_all_comment($slug);
				$comments_reply = $this->post->get_all_comment_reply_by_slug($slug);
			?>
			<!-- 코멘트 개수 -->
			<div id="comment_count">
				<span class="fw-bolder me-2"> 댓글 <span style="color:blue;" id="comment_count_<?= $slug ?>"><?= count($comments) ?></span> 개 </span>
				<span class="me-2" status="open" id="comment_list_open_<?= $slug ?>" style="font-size:.75rem; cursor:pointer; " slug="<?= $slug ?>" onclick="comment_list_toggle(this)">댓글 보기</span>
				<span class="" status="close" id="comment_list_close_<?= $slug ?>" style="font-size:.75rem; cursor:pointer; display:none;" slug="<?= $slug ?>" onclick="comment_list_toggle(this)">댓글 숨기기</span>
			</div>
			<div id="comment_list_item_<?= $slug ?>" style="display:none;">
				<?php for($j=0; $j < count($comments); $j++){ ?>
				<!-- 코멘트 내용 -->
				<div class="border-top" id="comment_<?= $comments[$j]['idx'] ?>">
					<div class="row no-gutters mt-2 mx-0 px-2">
						<div class="col-md-12">
							<div class="d-flex align-items-center mx-0 p-0">
								<p class="text-center">
								<?php if($comments[$j]['comment_nickname'] == "운영자"){?>
									<img src="/img/logo.png" width="16" height="16">
									<span class="ms-1 text-primary" style="font-size:.75rem; font-weight:bolder" title="<?= $comments[$j]['comment_nickname'] ?>">
										<?= $comments[$j]['comment_nickname'] ?>
									</span>
								<?php } else { ?>
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
										<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
										<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
									</svg>
									<span style="font-size:.75rem; font-weight:bolder" title="<?= $comments[$j]['comment_nickname'] ?>">
										<?= $comments[$j]['comment_nickname'] ?>
									</span>
								<?php } // -- end of if(운영자) ?>
									&nbsp;&nbsp;
									<small class="mx-2 text-dark" style="font-size:.5rem;">
										<?= $comments[$j]['reg_date']  ?>
									</small>
								<?php if($comments[$j]['is_secret'] == 'Y'){ ?>
									<span class="text-secondary"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#lock"/></svg></span>
								<?php } ?>
								</p>
							</div>
							<div class="row">
								<?php
									$text = nl2br($comments[$j]['post_comment']); // 댓글 데이터의 줄바꿈 문자를 <br /> 테그 문자로 변경한다.
									$string_count = count(explode(' ',$comments[$j]['post_comment']));
									// echo "{$string_count}";
									// echo htmlspecialchars($text);
									$new_text = explode('<',$text); // 태그 시작문자 '<'로 댓글 문자열을 쪼갠다.
									$br_count = 0; //'br' 태그 개수
									foreach($new_text as $value ){
										$each_text = explode(' ',$value);
										if($each_text[0] == 'br') { //br 태그의 개수를 카운팅 한다.
											$br_count += 1;
										}
									}
									// echo var_dump($new_text[0],$br_count);
								?>
								<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_<?= $comments[$j]['idx'] ?>">
									<?= word_limiter(nl2br($comments[$j]['post_comment']),5) ?><br>
									<?php
										if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
									?>
									<div>
										<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="unfold_comment(this)">자세히 보기</span>
									</div>
									<?php
										}
									?>
								</div>
								<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem; display:none;" id="long_comment_<?= $comments[$j]['idx'] ?>">
									<?= nl2br($comments[$j]['post_comment']) ?><br>
									
									<?php
										if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
									?>	
									<div>						
										<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="fold_comment(this)">간략히 보기</span>
									</div>
									<?php
										}
									?>
								</div>
								<!-- 댓글 수정을 위한 textarea hidden 요소 -->
								<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_<?= $comments[$j]['idx'] ?>">
									<div class="col-12 form-check d-flex align-items-center">
									<?php if($comments[$j]['is_secret'] == 'Y'){?>
										<input class="form-check-input me-2" type="checkbox" checked value="" id="is_secret_<?= $comments[$j]['idx'] ?>">
									<?php } else { ?>
										<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_<?= $comments[$j]['idx'] ?>">
									<?php }?>
										<label class="form-check-label me-3" for="is_secret_<?= $comments[$j]['idx'] ?>">
											<span style="font-size:.75rem;">비밀글</span>
										</label>
										<!-- <span class="fold me-2" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="update_comment(this)">저장</span> -->
										<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="fold_comment(this)">취소</span>
										
									</div>
									<div class="form-floating">
										<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_modify_<?= $comments[$j]['idx'] ?>" name="comment_modify_<?= $comments[$j]['idx'] ?>" style="height: 100px; font-size:.75rem;" value=""><?= $comments[$j]['post_comment'] ?></textarea>
										<label for="comment_modify_<?= $comments[$j]['idx'] ?>">댓글 수정하기</label>
									</div>
									<br>
								</div>
								
								<!-- 댓글 수정,삭제 관련 요소 -->
								<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">
									<div class="row d-flex justify-content-center px-0">
										<div class="d-flex justify-content-center" style="height:80%;" >
											<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="<?= $comments[$j]['idx'] ?>" onclick="update_comment(this)" style="width:100%;" id="comment_modify_save_btn_<?= $comments[$j]['idx'] ?>">
												<span class="info_text" title="저장">
													<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
														<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
													</svg>
												</span>
											</button>
										</div>
										<div class="comment_pw_check mb-1" id="passwd_check_<?= $comments[$j]['idx'] ?>">
											<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value(this,undo_input,unfold_hidden_comment,delete_comment)" id="modify_passwd_<?= $comments[$j]['idx'] ?>" idx="<?= $comments[$j]['idx'] ?>" post_slug ="<?= $slug ?>" placeholder="비밀번호" maxlength="4" title="" value="">
										</div>
									</div>
									<div class="d-flex justify-content-center align-items-end mb-1" style="height:16px;">
										<div class="comment_icon_div me-2">
											<span class="me-2 comment-icon" id="reply_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="comment_reply(this)" idx="<?= $comments[$j]['idx'] ?>" title="댓글 남기기">
												<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
													<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
												</svg>
											</span>
										</div>
										<div class="comment_icon_div me-2">
											<span class="me-2 comment-icon" id="modify_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment(this)" idx="<?= $comments[$j]['idx'] ?>" title="수정">
												<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
													<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
													<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
												</svg>
											</span>
										</div>
										<!-- <span class="comment-icon" id="delete_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="del_comment(this)" idx="<?= $comments[$j]['idx'] ?>" post_slug ="<?= $slug ?>" title="삭제">
											<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
												<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
												<path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
											</svg>
										</span> -->
										<div class="comment_icon_div del_btn" id="delete_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" post_slug ="<?= $slug ?>" mark="comment" title="삭제">										
											<p class="trash_cap_wrap text-center">
												<span class="trash_cap py-0 my-0"></span>	
											</p>

											<span class="trash comment_icon">
												<!-- <span class="py-0 my-0"></span> -->
												<!-- <i></i> -->
											</span>
										</div>
									</div>
								</div>
								<!-- 댓글에 대한 댓글 등록 요소  -->
								<div class="comment_reply mt-2 p-1 border rounded" id="comment_reply_reg_<?= $comments[$j]['idx'] ?>">
									<div class="row mb-2">
										<div class="col-3 pe-0">
											<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_reply_nickname_<?= $comments[$j]['idx'] ?>" name="comment_reply_nickname_<?= $comments[$j]['idx'] ?>" value="운영자">
										</div>
										<div class="col-3 pe-0">
											<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_reply_passwd_<?= $comments[$j]['idx'] ?>" name="comment_reply_passwd_<?= $comments[$j]['idx'] ?>" value="">
										</div>
										<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">
											<input class="form-check-input me-2" type="checkbox" value="" id="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>" name="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>">
											<label class="form-check-label" for="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>">
												<span style="font-size:.75rem;">비밀글</span>
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-9 form-floating d-flex justify-content-center pe-0">
											<textarea class="form-control" placeholder="댓글에 댓글 남기기" id="post_comment_reply_<?= $comments[$j]['idx'] ?>" name="post_comment_reply_<?= $comments[$j]['idx'] ?>" style="height: 100px; width:98%;" ></textarea>
											<label class="ps-4" for="post_comment_reply_<?= $comments[$j]['idx'] ?>">댓글에 댓글 남기기</label>
										</div>
										<div class="col-3 d-grid ps-0">
											<button type="button" class="btn btn-outline-primary" slug="<?= $slug ?>" org_idx="<?= $comments[$j]['idx'] ?>" onclick="save_comment_reply(this)" style="width:95%;">
												<span class="info_text" title="저장">
													<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
														<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
													</svg>
												</span>
											</button>
										</div>
									</div>
								</div>
							</div>												
						</div>
					</div>
					<!-- end of comment -->
					<!-- 댓글에 대한 댓글 내용 -->
					<div id="comment_reply_list_<?= $comments[$j]['idx'] ?>">
						<?php
							$comment_reply_count = 0; // 대댓글 개수를 세기 위한 변수
							for($k = 0; $k < count($comments_reply); $k++) { // 댓글에 대한 댓글 갯수만큼 반복한다.
								$comment_idx = $comments[$j]['idx']; // 원본 댓글의 idx
								$comment_reply_org_idx = $comments_reply[$k]['org_idx']; // 원본 댓글에 대한 댓글 정보의 idx
								if( $comment_idx == $comment_reply_org_idx) { // 원본 댓글의 idx에 해당하는지 확인한다.
									$comment_reply_count++ ; // 댓글에 대한 댓글 개수를 센다.
						?>
						<div class="border-top comment_reply_content" id="comment_reply_<?= $comments_reply[$k]['idx'] ?>">
							<div class="row no-gutters mt-2 mx-0 px-2">
								<div class="col-md-12">
									<div class="d-flex align-items-center mx-0 p-0">
										<p class="text-center">
											<span class="text-primary me-1">
												<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-right" viewBox="0 0 16 16">
													<path fill-rule="evenodd" d="M1.5 1.5A.5.5 0 0 0 1 2v4.8a2.5 2.5 0 0 0 2.5 2.5h9.793l-3.347 3.346a.5.5 0 0 0 .708.708l4.2-4.2a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 8.3H3.5A1.5 1.5 0 0 1 2 6.8V2a.5.5 0 0 0-.5-.5z"/>
												</svg>
											</span>  
										<?php if($comments_reply[$k]['comment_reply_nickname'] == "운영자"){?>
											<img src="/img/logo.png" width="16" height="16">
											<span class="ms-1 text-primary" style="font-size:.75rem; font-weight:bolder" title="<?= $comments_reply[$k]['comment_reply_nickname'] ?>">
												<?= $comments_reply[$k]['comment_reply_nickname'] ?>
											</span>
										<?php } else { ?>
											<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
												<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
												<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
											</svg>
											<span style="font-size:.75rem; font-weight:bolder" title="<?= $comments_reply[$k]['comment_reply_nickname'] ?>">
												<?= $comments_reply[$k]['comment_reply_nickname'] ?>
											</span>
										<?php } // -- end of if(운영자) ?>
											&nbsp;&nbsp;
											<small class="mx-2 text-dark" style="font-size:.5rem;">
												<?= $comments_reply[$k]['reg_date']  ?>
											</small>
											<?php if($comments_reply[$k]['is_secret'] == 'Y'){ ?>
												<span class="text-secondary"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#lock"/></svg></span>
											<?php } ?>
										</p>
									</div>
									<div class="row">
										<?php
											$text = nl2br($comments_reply[$k]['post_comment_reply']); // 댓글 데이터의 줄바꿈 문자를 <br /> 테그 문자로 변경한다.
											$string_count = count(explode(' ',$comments_reply[$k]['post_comment_reply']));
											// echo "{$string_count}";
											// echo htmlspecialchars($text);
											$new_text = explode('<',$text); // 태그 시작문자 '<'로 댓글 문자열을 쪼갠다.
											$br_count = 0; //'br' 태그 개수
											foreach($new_text as $value ){
												$each_text = explode(' ',$value);
												if($each_text[0] == 'br') { //br 태그의 개수를 카운팅 한다.
													$br_count += 1;
												}
											}
											// echo var_dump($new_text[0],$br_count);
										?>
										<div class="col-10 ps-5" style="font-size:.75rem;" id="short_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
											<?= word_limiter(nl2br($comments_reply[$k]['post_comment_reply']),5) ?><br>
											<?php
												if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
											?>
											<div>
												<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="unfold_comment_reply(this)">자세히 보기</span>
											</div>
											<?php
												}
											?>
										</div>
										<div class="col-10 ps-5" style="font-size:.75rem; display:none;" id="long_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
											<?= nl2br($comments_reply[$k]['post_comment_reply']) ?><br>
											
											<?php
												if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
											?>	
											<div>						
												<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="fold_comment_reply(this)">간략히 보기</span>
												</div>
											<?php
												}
											?>
										</div>
										<!-- 댓글에 대한 댓글 수정을 위한 textarea hidden 요소 -->
										<div class="col-10 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
											<div class="col-12 form-check d-flex align-items-center">
											<?php if($comments_reply[$k]['is_secret'] == 'Y'){?>
												<input class="form-check-input me-2" type="checkbox" checked value="" id="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
											<?php } else { ?>
												<input class="form-check-input me-2" type="checkbox" value="" id="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
											<?php }?>
												<label class="form-check-label me-3" for="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
													<span style="font-size:.75rem;">비밀글</span>
												</label>
												<!-- <span class="fold me-2" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="update_comment_reply(this)">저장</span> -->
												<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="fold_comment_reply(this)">취소</span>
												
											</div>
											<div class="form-floating">
												<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>" name="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>" style="height: 100px; font-size:.75rem;" value=""><?= $comments_reply[$k]['post_comment_reply'] ?></textarea>
												<label for="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>">댓글 수정하기</label>
											</div>
											
											<br>									
										</div>
										
										<!-- 댓글에 대한 댓글 수정,삭제 관련 요소 -->
										<div class="col-2 row m-0 p-0 d-flex justify-content-center align-items-end">
											<div class="row d-flex justify-content-center px-0">	
												<div class="d-flex justify-content-center" style="height:80%;" >
													<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="update_comment_reply(this)" style="width:100%;" id="comment_reply_modify_save_btn_<?= $comments_reply[$k]['idx'] ?>">
														<span class="info_text" title="저장">
															<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
															</svg>
														</span>
													</button>
												</div>
												<div class="comment_pw_check mb-1" id="reply_passwd_check_<?= $comments_reply[$k]['idx'] ?>">
													<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value_reply(this,undo_input,unfold_hidden_comment_reply,delete_comment_reply)" id="modify_reply_passwd_<?= $comments_reply[$k]['idx'] ?>" idx="<?= $comments_reply[$k]['idx'] ?>" placeholder="비밀번호" maxlength="4" title="" value="">
												</div>
											</div>
											<div class="d-flex justify-content-center align-items-end">
												<div class="comment_icon_div me-2">
													<span class="me-2 comment-icon" id="reply_modify_button_<?= $comments_reply[$k]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment_reply(this)" idx="<?= $comments_reply[$k]['idx'] ?>" title="수정">
														<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
															<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
															<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
														</svg>
													</span>
												</div>
												<!-- <span class="comment-icon" id="reply_delete_button_<?= $comments_reply[$k]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="del_comment_reply(this)" idx="<?= $comments_reply[$k]['idx'] ?>" title="삭제">
													<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
														<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
														<path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
													</svg>
												</span> -->
												<div class="comment_icon_div del_btn" id="reply_delete_button_<?= $comments_reply[$k]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" mark="comment_reply" title="삭제">										
													<p class="trash_cap_wrap text-center">
														<span class="trash_cap py-0 my-0"></span>	
													</p>

													<span class="trash comment_icon">
														<!-- <span class="py-0 my-0"></span> -->
														<!-- <i></i> -->
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>
						<!-- end of comment_reply -->
						<?php
								} // --end of if (comment_reply_org_idx)
							} // --end of for (comment_reply)
						?>
					</div>
					<!-- end of comment_reply_list -->
					<input type="hidden" id="comment_reply_count_<?= $comments[$j]['idx'] ?>" value="<?= $comment_reply_count ?>" >
				</div>
				<!-- end of comment -->
	<?php 
	} // --end of for (comment) 
	?>
			</div>
			<!-- end of comment_list_item  -->
		</div>
		<!-- end of comment_list -->
		<hr>
		<div class="my-3" id="post_list">
			<p><span class="fw-bolder">관련 포스트 목록</span></p>
			<ul class="sibling_posts">
			<?php
				for($i=0; $i < count($posts); $i++) {
					$page = ceil(($i+1)/$posts_per_page); // 현재 포스트의 페이지 번호
					if($posts[$i]['slug'] == $slug) {
			?>
				<li class="fw-bolder"><a style="text-decoration:none;" href="<?= '/postmanage/detail/' . $posts[$i]['slug'] .'?page='.$page.'&filter='.$filter.'&search='.$search ?>" target="_blank"><?= '['.$posts[$i]['foreword'].'] '.$posts[$i]['title'] ?></a></li>
			<?php	} else {
			?>
				<li class=""><a style="text-decoration:none;" href="<?= '/postmanage/detail/' . $posts[$i]['slug'] .'?page='.$page.'&filter='.$filter.'&search='.$search ?>" target="_blank"><?= '['.$posts[$i]['foreword'].'] '.$posts[$i]['title'] ?></a></li>
			<?php   } 
				}	
			?>
			</ul>
		</div>
		<!-- end of post_list -->
	
	
	</div>
	<!-- --end of main content -->
<script>
	trash_init();
	viewer_init();
	// if($('.viewer-items').length > 0){
	// 	var viewer = new Viewer(document.querySelector('.viewer-items'), {});
	// 	console.log('viewer가 생성되었습니다.');
	// }
</script>
			
<?	
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>