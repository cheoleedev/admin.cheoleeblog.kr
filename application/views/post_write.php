
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>

		
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-light" style="height:calc(100vh - 167px); overflow:auto;">
		<?= $this->session->flashdata('message') ?>	
		<div class="pt-2 fs-5 fw-bold">
			<svg class="bi me-1" width="16" height="16"><use xlink:href="#write"/></svg>
			<span class="">포스트 작성</span>
		</div>
		<hr>
		<div class="row">
		<!-- ckeditorform -->
		
		<?= form_open_multipart('postwrite/save') ?>
			<div class="input-group mt-3 mb-1" style="font-size:13px; font-weight:bold;">
				<label for="main_subject" class="col-sm-1 col-form-label text-center me-1 text-truncate"><span title="메인 주제">메인 주제</span></label>
				<select class="form-select form-select-sm me-1" aria-label=".form-select-sm example" style="font-size:13px; font-weight:bold;" id="main_subject" name="main_subject">
					<option value="">--선택--</option>
					<?php for($i=0; $i < count($main_subject_data); $i++) {
							$main_subject_name = $main_subject_data[$i]['main_subject_name'];
							$main_subject_code = $main_subject_data[$i]['main_subject_code'];
							$is_activate = $main_subject_data[$i]['is_activate'];
							
							if($is_activate == 'Y') {
								if($main_subject_name_selected != '') { // 저장 전 선택한 메인 주제 이름이 있는 경우
									if($main_subject_name_selected == $main_subject_name) { // 메인 주제 목록중 해당 포스트의 메인 주제와 같은 경우 selected 속성을 추가해준다.
					?>
										<option value="<?= $main_subject_code; ?>" selected><?= $main_subject_name_selected; ?></option>
					<?php 
										} else { ?>
										<option value="<?= $main_subject_code; ?>"><?= $main_subject_name_selected; ?></option>
					<?php
										}
									} else { // 저장 전 선택한 메인 주제 이름이 있는 경우, 포스트를 새로 작성하는 경우
					?>
										<option value="<?= $main_subject_code; ?>"><?= $main_subject_name; ?></option>
					<?php					
									}
								}
							} // -- end of for
					?> 
				</select>
				<!-- 메인 주제 이름을 확인하기 위해 히든 요소에 값을 넣어준다. -->
				<input type="hidden" id="main_subject_name" name="main_subject_name" value="<?= $main_subject_name; ?>">
				<?php echo form_error('main_subject', '<div class="text-danger ml-2 ">', '</div>') ?>
				<label for="sub_subject" class="col-sm-1 col-form-label text-center me-1 text-truncate"><span title="하위 주제">하위 주제</span></label>
				<select class="form-select form-select-sm me-1" aria-label=".form-select-sm example" style="font-size:13px; font-weight:bold;" id="sub_subject" name="sub_subject">
					<option value="<?= $sub_subject_name; ?>"><?= $sub_subject_name; ?></option>
				</select>
				<?php echo form_error('sub_subject', '<div class="text-danger ml-2 ">', '</div>') ?>
			</div>
			<script>
				$(function(){
					// ckeditor 4 & ckfinder 3 integration
					var editor = CKEDITOR.replace( 'contents' );
					CKFinder.setupCKEditor( editor ,{
						skin: 'jquery-mobile',
						swatch: 'b',
						onInit: function( finder ) {
							finder.on( 'files:choose', function( evt ) {
								var file = evt.data.files.first();
								console.log( 'Selected: ' + file.get( 'name' ) );
							} );
						}
					
					},
					{
						type: 'Files', 
						currentFolder: '/archive/'
					});

					// 메인 주제를 선택하면 해당 주제의 하위 주제를 가져온다.
					$('#main_subject').change(function(e){
						var selected_main_subject = $(this).val();

						$.ajax({
							// 서버와  ajax()함수 사이의 http 통신 정보
							url: '/postwrite/getsubsubject',   //정보 처리를 할 서버 위치
							type: 'post',                             //정보 전달 방식
							data: {                                   //서버에 전달될 정보 (변수이름 : 값)
								selected_main_subject : selected_main_subject
							},
							dataType: 'json',                         //처리 후 돌려받을 정보의 형식
							async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
							cache: false,                             //데이터 값을 캐싱 할건지 여부
							success: function(data) {
							// $('#loading').fadeOut(200);

								if (data.code != 0) {
									alert(data.message + '\n(오류코드 : ' + data.code + ')');
									return;
								}
								
								var sub_subject = data.extra;
								console.log(sub_subject);

								 //메인 주제 이름이 담긴 배열을 꺼낸다. (마지막 배열 제거 및 리턴)
								var main_subject_name_array = sub_subject.pop();
								var main_subject_name = main_subject_name_array.main_subject_name;

								// console.log('배열값:'+main_subject_name);
								// 하위 주제 요소가 있는 경우 #sub_subject 요소에 넣어준다.
								if(sub_subject.length == 0) {
									alert('하위 주제가 없습니다.');
									$("#sub_subject").empty();
									$("#sub_subject").append('<option value="">--선택--</option>');
								} else {
									$("#sub_subject").empty();
									$("#sub_subject").append('<option value="">--선택--</option>');
									for(i=0; i < sub_subject.length; i++) {
									var sub_subject_name = sub_subject[i].sub_subject_name;
									// var sub_subject_code = sub_subject[i].sub_subject_code;
									var is_activate = sub_subject[i].is_activate;
									
										if(is_activate == 'Y') {
											$("#sub_subject").append('<option value="'+sub_subject_name+'">'+sub_subject_name+'</option>'                     
											);
										} 
									}
								}

								// 메인 주제 이름을 히든 요소 value에 넣어준다.
								$('#main_subject_name').val(main_subject_name);

							},
							error: function(err) {
							// $('#loading').fadeOut(200);

							var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
							alert('내부 오류가 발생하였습니다.' + error_message);
							}
						});

					});
				});
			</script>
			<div class="form-group" style="font-size:13px;">
				<div class="col-sm-12 col-lg-4 float-lg-start">
					<div class="input-group mt-3 mb-1">
						<label for="foreword" class="col-form-label text-center text-truncate me-1 fw-bold"><span title="머리말">머리말</span></label>
						<input class="form-control" type="text" id="foreword" name="foreword" size="40" style="font-size:13px; font-weight:bold;" value="<?= $foreword ?>">
						<?php echo form_error('foreword', '<div class="text-danger ml-2 ">', '</div>') ?>
					</div>
				</div>
				<div class="col-sm-12 col-lg-8 float-lg-end">
					<div class="input-group mt-3 mb-1">
						<label for="title" class="col-sm-1 col-form-label text-center text-truncate mx-1 fw-bold"><span title="제목">제목</span></label>
						<input class="form-control rounded-end" type="text" id="title" name="title" size="40" style="font-size:13px; font-weight:bold;" value="<?= $title ?>">
						<?php echo form_error('title', '<div class="text-danger ml-2 ">', '</div>') ?>
						<!-- 작성자 아이디 -->
						<input type="hidden" id="writer_id" name="writer_id" value="<?= $_SESSION['admin_id']; ?>">
					</div>
				</div>
			</div> 
			
			<div class="input-group mb-3">
				<label class="form-label" for="contents" style="font-size:13px; font-weight:bold;">내용</label>
				<textarea id="contents" name="contents" style="display:none;" value="<?= set_value('contents'); ?>" placeholder="내용을 입력하세요.">
					<?= htmlspecialchars($content) ?>
				</textarea>
				<?php echo form_error('contents', '<div class="text-danger ml-2 ">', '</div>') ?>
			</div>
			<div class="input-group mb-3"> 
				<label for="thumbnail" class="col-sm-1 col-form-label" style="font-size:13px; font-weight:bold;">썸네일</label>
				<input class="form-control ms-1" type="file" id="thumbnail" name="thumbnail" style="font-size:13px;">
				<?php echo form_error('thumbnail', '<div class="text-danger ml-2 ">', '</div>') ?>
			</div> 
			<div class="mb-1">
				<input class="form-control" type="submit" value="포스트 등록" style="background-color:#CEECF5;">
			</div> 
		<?= form_close() ?>
		<!-- -- end of ckeditorform -->
			
		</div>
		
	</div>
	<!-- --end of main content -->
				
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
