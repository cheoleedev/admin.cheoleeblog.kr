
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-light" >
		<?= $this->session->flashdata('message') ?>
		<div class="pt-2 fs-5 fw-bold">
			<svg class="bi me-2 " width="16" height="16"><use xlink:href="#setting"/></svg>
			<span class="">환경 설정</span>
		</div>
		<hr>
		<div id="title_change">							
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold setting_title" >타이틀 이미지 변경</p>
			</div>
			<?= form_open_multipart('setting/titleupload') ?>
			<div class="input-group mb-2">
				<input type="file" class="form-control" id="new_image" name="new_image">
				<input type="submit" class="btn btn-outline-secondary" value="업로드">
				<?php echo form_error('new_image', '<div class="text-danger ml-2 ">', '</div>') ?>
			</div>
			<?= form_close() ?>
			<div class="input-group mb-2">
				<span class="input-group-text" id="slected_title_text">선택한 이미지</span>
  				<input type="text" class="form-control" aria-label="selected_title" aria-describedby="selected_title" placeholder="선택한 이미지" id="selected_title" name="selected_title" value="<?php if($active_title_img != null){ echo $active_title_img['title_img']; }?>" readonly>
			</div>
			<div class="input-group">
				<span class="input-group-text" id="title_href_text">link</span>
  				<input type="text" class="form-control" aria-label="title_href" aria-describedby="title_href" placeholder="타이틀 링크 url" id="title_href" name="title_href" value="<?php if($active_title_img != null){ echo $active_title_img['title_href']; }?>">
				<input type="button" class="btn btn-outline-secondary" onclick="title_href_update();" value="업데이트">
			</div>
			<?= form_close() ?>
			<div class="row">
				<div class="col-6 form-check form-switch d-flex justify-content-between align-items-center" >
					<div class="col-8"><span class="info_text fw-bolder">사용 설정</span></div>
					<?php 
						if($active_title_img == null){
							$is_activate = '';
							$is_use = '';
							$title_img = '';
							$images[0]['is_activate'] = 'temp'; // 대표 타이틀이 없을 경우 첫번째 인덱스 타이틀을 임시 대표로 설정해준다. 
						} else {
							$is_activate = $active_title_img['is_activate'];
							$is_use = $active_title_img['is_use'];
							$title_img = $active_title_img['title_img'];
						}
					?>
					<div class="col-4" id="is_use_checkbox">
					<?php if($is_use == 'Y') { ?>
						<span title="사용 여부" ><input class="form-check-input ms-1" type="checkbox" id="is_use_<?= $title_img ?>" name="is_use" checked  onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_use"></span>
						<?php } else { ?>
							<span title="사용 여부" ><input class="form-check-input ms-1" type="checkbox" id="is_use_<?= $title_img ?>" name="is_use" onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_use"></span>
						<?php } // --end of if ?>
					</div>
				</div>
				<div class="col-6 ps-0 form-check form-switch d-flex justify-content-between align-items-center" >
					<div class="col-8 "><span class="info_text fw-bolder px-0">대표 이미지 설정</span></div>
					<div class="col-4" id="is_activate_checkbox">
					<?php if($is_activate == 'Y') { ?>
					<span title="대표 이미지 여부" ><input class="form-check-input ms-1" type="checkbox" id="is_activate_<?= $title_img ?>" name="is_activate" checked  onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_activate"></span>
					<?php } else { ?>
						<span title="대표 이미지 여부"><input class="form-check-input ms-1" type="checkbox" id="is_activate_<?= $title_img ?>" name="is_activate" onclick="title_status_update(this)" style="width:3em; height:1.5em; cursor:pointer;" mark="is_activate"></span>
					<?php } // --end of if ?>
					</div>
				</div>
			</div>
		</div>
		
		<div id="title_images" class="carousel slide" data-bs-ride="false">
		
			<div class="carousel-indicators">
			<?php for($i=0; $i < count($images); $i++ ){
				$num = $i + 1;
				if($i == 0){ ?>
					<button type="button" data-bs-target="#title_images" data-bs-slide-to="<?= $i ?>" class="active" aria-current="true" aria-label="Slide <?= $num ?>"></button>
				<?php } else { ?>
				<button type="button" data-bs-target="#title_images" data-bs-slide-to="<?= $i ?>" aria-label="Slide <?= $num ?>"></button>
				<?php }
				} // --end of for
			?>
			</div>
			<div class="carousel-inner">
			<?php for($i=0; $i < count($images); $i++ ){
				$num = $i + 1;

				if($images[$i]['is_activate'] == 'Y' or $images[$i]['is_activate'] == 'temp'){ ?>
					<div class="carousel-item active" file_name="<?= $images[$i]['title_img'];?>">
						<img src="<?= '/img/title/'.$images[$i]['title_img'];?>" class="d-block w-100" style="cursor:pointer;" alt="타이틀 이미지" onclick="title_select(this);" file_name="<?= $images[$i]['title_img'];?>">
					</div>
				<?php } else { ?>
					<div class="carousel-item" file_name="<?= $images[$i]['title_img'];?>">
						<img src="<?= '/img/title/'.$images[$i]['title_img'];?>" class="d-block w-100" style="cursor:pointer;" alt="타이틀 이미지" onclick="title_select(this);" file_name="<?= $images[$i]['title_img'];?>">
					</div>
				<?php }
				} // --end of for
			?>	
			</div>
			<button class="carousel-control-prev" type="button" data-bs-target="#title_images" data-bs-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Previous</span>
			</button>
			<button class="carousel-control-next" type="button" data-bs-target="#title_images" data-bs-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Next</span>
			</button>
		</div>
		<hr style="background-color:grey;">
		<!-- crousel 관련 스크립트 -->
		<script>
			var myCarousel = document.querySelector('#title_images');
			var carousel = new bootstrap.Carousel(myCarousel, {
				pause : true,  // 이미지 자동 슬라이드 하지 않음
				interval : false,
				wrap : true // 순환 (전체 이미지를 계속 순환함)
			});

			myCarousel.addEventListener('slide.bs.carousel', function () {
				var cur_slide = event.relatedTarget;
				console.log($(cur_slide).attr('file_name'));
				title_select(cur_slide);
			});
		</script>
		<!-- end of crousel -->
		<div id="subject_manage">
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold setting_title" >주제 관리</p>
			</div>
			
			<div class="d-flex row">
				<!-- 메인 주제 요소 -->
				<div class="col-sm-12 col-lg-6">
					<?= form_open_multipart('setting/mainsubjectadd') ?>
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="메인 주제 입력" aria-label="main subject" aria-describedby="main_subject" id="main_subject" name="main_subject">
						<?php echo form_error('main_subject', '<div class="text-danger ml-2 ">', '</div>') ?>
						<button type="button" class="btn btn-outline-secondary" onclick="main_subject_add()" value="추가">추가</button>
					</div>
					<div class="border rounded mb-3" style="font-size:13px; height:120px; overflow:auto;">
						<ul id="main_subject_list" class="list-group list-group-flush">
							<?php for($i=0; $i < count($main_subject_data); $i++) {
								$main_subject_name = $main_subject_data[$i]['main_subject_name'];
								$main_subject_code = $main_subject_data[$i]['main_subject_code'];
								$main_subject_color = $main_subject_data[$i]['color'];
								$is_activate = $main_subject_data[$i]['is_activate'];
							?>
							<li class="list-group-item border-bottom subject" name="<?= $main_subject_name ?>">
								<div class="form-check form-switch d-flex justify-content-between align-items-center p-0" >
									<div class="flex-grow-1" >
										<label class="form-check-label w-100" for="" onclick="selected_subject(this)" code="<?= $main_subject_code ?>" style="cursor:pointer;"><input class="form-control subject_input" id="subject_value_<?= $main_subject_code ?>" type="text" value="<?= $main_subject_name ?>"></label>
										<input type="hidden" id="old_subject_value_<?= $main_subject_code ?>" value="<?= $main_subject_name ?>">
									</div>
									<!-- <div class="align-middle text-center mx-1"> -->
									<input type="color" id="subject_color_<?= $main_subject_code ?>" value="<?= $main_subject_color ?>" style="width:25px; height:25px; border:none; margin-right: 8px; padding:0;">
									<button type="button" class="btn btn-sm btn-outline-primary" onclick="update_subject(this)" code="<?= $main_subject_code ?>" style="margin-right:40px;">	
										<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
											<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
										</svg></span>
									</button>
									<!-- </div> -->
									<?php if($is_activate == 'Y') { ?>
									<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="<?= $main_subject_code ?>" checked  onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
									<?php } else { ?>
										<span title="활성화 여부"><input class="form-check-input" type="checkbox" id="<?= $main_subject_code ?>" onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
									<?php } // --end of if ?>								
								</div>
							</li>
							<?php 
							} // --end of for
							?>
							
						</ul>
					</div>
					<?= form_close() ?>
				</div>

				<!-- 하위 주제 요소 -->
				<div class="col-sm-12 col-lg-6">
					<?= form_open_multipart('setting/subsubjectadd') ?>
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="선택한 메인 주제" id="main_subject_name" name="main_subject_name" value="<?= $selected_subject_name ?>" readonly>
						<input type="text" class="form-control" placeholder="하위 주제 입력" aria-label="sub subject" aria-describedby="sub_subject" id="sub_subject" name="sub_subject">
						<input type="hidden" id="main_subject_code" name="main_subject_code" value="<?= $selected_subject_code ?>" readonly>
						<?php echo form_error('sub_subject', '<div class="text-danger ml-2 ">', '</div>') ?>
						<button type="button" class="btn btn-outline-secondary" onclick="sub_subject_add();" value="추가">추가</button>
					</div>
					<div class="border rounded" style="font-size:13px; height:120px; overflow:auto;">
						<ul id="sub_subject_list" class="list-group list-group-flush">
							<?php if(empty($sub_subject_data)) { ?>
								<span class="text" style="color:gray;">메인 주제를 선택해주세요.</span>
							<?php } // --end of if ?>						
						</ul>
					</div>
					<?= form_close() ?>
				</div>		

			</div>
		</div>

		<hr style="background-color:grey;">
		
		<!-- 포스트 목록 관리 -->
		<div id="post_listing_setting">
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold setting_title" >포스트 목록 관리</p>
			</div>
			<div class="row border-bottom border-3 py-1 mx-0">
				<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">적용</div>
				<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">페이지 당 포스트 개수</div>
				<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">페이지 아이템 표시 개수</div>
			</div>
			<div class="row py-1 border-bottom mx-0" style="background-color:#F2F2F2;">
				<div class="col align-middle text-center" >
					<button type="button" class="btn btn-sm btn-outline-primary" onclick="post_listing_setting()" >	
						<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
							<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
						</svg></span>
					</button>
				</div>
				<div class="col align-middle text-center d-flex justify-content-center"><input type="number" class="form-control py-0 account-info text-center" min='1' id="posts_per_page" name="posts_per_page" value="<?= $config->posts_per_page ?>"></div>
				<div class="col align-middle text-center d-flex justify-content-center"><input type="number" class="form-control py-0 account-info text-center" min='1' id="page_item_count" name="page_item_count" value="<?= $config->page_item_count ?>"></div>
			</div>
		</div>

		<hr style="background-color:grey;">

	</div>
	<!-- --end of main content -->
					
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
