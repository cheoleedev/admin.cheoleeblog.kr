<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_model {

	function __construct(){ 
		parent::__construct(); 
        //위에서 설정한 /application/config/database.php 파일에서 $db['cheolee'] 설정값을 불러오겠다는 뜻입니다.
        $this->db = $this->load->database('db', TRUE);
	} 
    
    // 모든 타이틀 이미지 정보를 가져온다. (연관 배열)
    public function get_all_title()
    {
        // "SELECT * FROM blog_title_image ORDER BY 'idx' ASC"; ( DESK =  오름차순 정렬 ) 
        $this->db->order_by('idx', 'ASC'); //내림차순 정렬
        return $this->db->get('blog_title_image')->result_array();
    }

    // 업로드한 타이틀 이미지 정보를 입력한다.
    public function insert_image($table, $data)
    {
       $this->db->insert($table, $data);
    }

    // 선택한 타이틀 이미지의 활성화 여부를 업데이트 한다.
    public function update_title($table,$title_img,$status,$mark){
        if($mark == 'is_use'){
            $this->db->update($table,array('is_use' => $status),array('title_img' => $title_img));
        } else { // is_activate
            if($status == 'Y'){
                $this->db->update($table,array('is_activate' => 'Y'),array('title_img' => $title_img));
                $this->db->update($table,array('is_activate' => 'N'),array('title_img !=' => $title_img)); // 선택한 타이틀 이미지 이외의 데이터를 비활성화 시킨다.
            }else{
                $this->db->update($table,array('is_activate' => 'N'),array('title_img' => $title_img));
            }
        }        
    }

    // 모든 메인 주제 정보를 가져온다. (연관 배열)
    public function get_all_main_subject()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' ASC";
        $this->db->order_by('idx', 'ASC'); //내림차순 정렬
        return $this->db->get('main_subject')->result_array();
    }

    // 메인 주제를 추가한다.
    public function insert_main_subject($table, $data)
    {
        $this->db->insert($table, $data);
    }

    // 선택된 메인 주제를 가져온다. (연관 배열)
    public function get_selected_main_subject($main_subject_code)
    {
        // "SELECT * FROM main_subject ORDER BY 'idx' ASC";
        return $this->db->get_where('main_subject',array('main_subject_code' => $main_subject_code))->result_array();
    }

    // 새로 추가된 메인 주제를 가져온다. (연관 배열)
    public function get_new_main_subject($reg_date)
    {
        // "SELECT * FROM main_subject ORDER BY 'idx' ASC";
        return $this->db->get_where('main_subject',array('reg_date' => $reg_date))->row_array();
    }

    // 메인 주제 활성화 여부를 업데이트 한다.
    public function main_subject_activate_check($table,$subject_code,$is_activate)
    {
        $this->db->update($table,array('is_activate' => $is_activate),array('main_subject_code' => $subject_code));
    }

    // 모든 하위 주제 정보를 가져온다. (연관 배열)
    public function get_all_sub_subject()
    {
        // "SELECT * FROM sub_subject ORDER BY 'idx' ASC";
        $this->db->order_by('idx', 'ASC'); //내림차순 정렬
        return $this->db->get('sub_subject')->result_array();
    }
    
    // 선택한 메인 주제의 하위 주제 정보를 가져온다. (연관 배열)
    public function get_sub_subject($subject_code)
    {
        // "SELECT * FROM sub_subject ORDER BY 'idx' ASC";
        $this->db->order_by('idx', 'ASC'); //내림차순 정렬
        return $this->db->get_where('sub_subject',array('main_subject_code' => $subject_code))->result_array();
    }

    // 선택한 메인 주제의 하위 주제 정보를 가져온다. (연관 배열 한개)
    public function get_new_sub_subject($reg_date)
    {
        // "SELECT * FROM sub_subject ORDER BY 'idx' ASC";
        $this->db->order_by('idx', 'ASC'); //내림차순 정렬
        return $this->db->get_where('sub_subject',array('reg_date' => $reg_date))->row_array();
    }

    // 하위 주제를 추가한다.
    public function insert_sub_subject($table, $data)
    {
        $this->db->insert($table, $data);
    }

    // 하위 주제 활성화 여부를 업데이트 한다.
    public function sub_subject_activate_check($table,$is_activate,$where)
    {
        $this->db->update($table,array('is_activate' => $is_activate),$where);
    }

    // config 데이터를 가져온다. 
    public function get_config()
    {
        return $this->db->get('config')->row(); // 한줄의 '객체배열'을 반환한다.
    }

    // config 데이터를 업데이트 한다.
    public function update_config($table,$data)
    {
        $this->db->update($table,$data);
    }

    // FUNCTION HAPUS
    public function deletePost($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete('post_content', $id);
    }

    public function getById($id)
    {
        return $this->db->get_where('post_content', $id)->row(); //한줄의 '객체배열'을 반환한다.
    }

    public function _deleteImage($id)
    {
        $posts = $this->getById($id);
        $filename = explode(".", $posts->thumbnail)[0];

        // delte old thumbnail
        $delImg = array_map('unlink', glob(FCPATH . "img/post/$filename.*"));
        $delImg = array_map('unlink', glob(FCPATH . "img/post/resize/$filename.*"));
        return $delImg;
    }
}