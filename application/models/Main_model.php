<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_model {

	function __construct(){ 
		parent::__construct(); 
        //위에서 설정한 /application/config/database.php 파일에서 $db['cheolee'] 설정값을 불러오겠다는 뜻입니다.
      	$this->db = $this->load->database('db', TRUE);
	} 
    
    function login($admin_id,$admin_pw) {
        // var_dump (PDO :: getAvailableDrivers ());
        //query 함수는 DB에 쿼리를 요청하는 것이며, 이 과정에서 오류가 뜨면 오류메시지가 나옵니다.
      
        $query = "SELECT * FROM admin WHERE admin_id = ? AND passwd = ?";
        $stmt = $this->db->query($query, array($admin_id,$admin_pw));
        // $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        // 쿼리 결과를 연관배열 형태로 화면에 뿌려준다.
        //var_dump($stmt->result());

        // 쿼리 결과를 연관배열로 가져온다.
        // $result = $stmt->result_array();
        $result = $stmt->num_rows();

        if($result > 0) {
            return true;
        } else {
            return false;
        }

    }
    // 마지막 로그인 일자를 업데이트 해준다.
    function update_last_login_date($table,$data,$session_data) {

        $this->db->update($table,$data,$session_data);
        // "UPDATE $table SET 'last_login_date' = $last_login_date WHERE 'admin_id' = $admin_id" 쿼리 실행과 동일

    }

    // 로그인 횟수를 업데이트 해준다.
    function update_login_count($table,$session_data) {
        $result = $this->db->get_where($table,$session_data)->result_array();
        $login_count = $result[0]['login_count'] + 1;

        $this->db->update($table,array('login_count' => $login_count),$session_data);
    }
    // 모든 post의 데이터를 가져온다. (연관배열)
    public function get_all_post()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->result_array();
    }

    // 포스트 세부 데이터를 가져온다. (연관배열)
    public function getDetailPost($slug)
    {
        return $this->db->get_where('post_content', array('slug' => $slug))->row_array();
    }

    // 모든 메인 주제 데이터를 가져온다. (인덱스가 있는 연관배열)
    public function get_all_main_subject()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' ASC"; (오름차순 정렬, 등록순)
        $this->db->order_by('idx', 'ASC');
        return $this->db->get('main_subject')->result_array();
    }

    // 모든 하위 주제 데이터를 가져온다. (인덱스가 있는 연관배열)
    public function get_all_sub_subject_data()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' ASC"; (오름차순 정렬, 등록순)
        $this->db->order_by('idx', 'ASC');
        return $this->db->get('sub_subject')->result_array();
    }

    // 선택한 메인 주제의 하위 주제 데이터를 가져온다. (인덱스가 있는 연관배열)
    public function get_all_sub_subject($subject_code)
    {
        // "SELECT * FROM post_content ORDER BY 'idx' ASC"; (오름차순 정렬, 등록순)
        $this->db->order_by('idx', 'ASC');
        return $this->db->get_where('sub_subject',array('main_subject_code' => $subject_code))->result_array();
    }

    // 주제별 post 데이터의 개수 가져온다.
    public function count_main_subject_post_data($main_subject)
    {
        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_content',array('main_subject_name' => $main_subject))->num_rows();
    }

    // 설정된 기간의(일 기준) 주제별 post 데이터의 개수 가져온다.
    public function count_main_subject_post_data_daily_period($main_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";

        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_content',array('main_subject_name' => $main_subject))->num_rows();
    }

    // 설정된 기간의(월 기준) 주제별 post 데이터의 개수 가져온다.
    public function count_main_subject_post_data_monthly_period($main_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
        
        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_content',array('main_subject_name' => $main_subject))->num_rows();
    }

    // 주제별 post 데이터의 개수 가져온다.
    public function count_sub_subject_post_data($main_subject, $sub_subject)
    {
        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_content',array('main_subject_name' => $main_subject, 'sub_subject_name' => $sub_subject))->num_rows();
    }

    // 설정된 기간의(일별) 주제별 post 데이터의 개수 가져온다.
    public function count_sub_subject_post_data_daily_period($main_subject, $sub_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_content',array('main_subject_name' => $main_subject, 'sub_subject_name' => $sub_subject))->num_rows();
    }

    // 설정된 기간의(월별) 주제별 post 데이터의 개수 가져온다.
    public function count_sub_subject_post_data_monthly_period($main_subject, $sub_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_content',array('main_subject_name' => $main_subject, 'sub_subject_name' => $sub_subject))->num_rows();
    }

    // 주제별 post의 comment 데이터의 개수 가져온다.
    public function count_main_subject_comment_data($main_subject)
    {
        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_comment',array('main_subject_name' => $main_subject))->num_rows();
    }

    // 설정된 기간의(일별) 주제별 post의 comment 데이터의 개수 가져온다.
    public function count_main_subject_comment_data_daily_period($main_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
        
        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_comment',array('main_subject_name' => $main_subject))->num_rows();
    }

    // 설정된 기간의(월별) 주제별 post의 comment 데이터의 개수 가져온다.
    public function count_main_subject_comment_data_monthly_period($main_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_comment',array('main_subject_name' => $main_subject))->num_rows();
    }

    // 주제별 post의 comment 데이터의 개수 가져온다.
    public function count_sub_subject_comment_data($main_subject,$sub_subject)
    {
        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_comment',array('main_subject_name' => $main_subject,'sub_subject_name' => $sub_subject))->num_rows();
    }

    // 설정된 기간의 주제별 post의 comment 데이터의 개수 가져온다. (일별)
    public function count_sub_subject_comment_data_daily_period($main_subject,$sub_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_comment',array('main_subject_name' => $main_subject,'sub_subject_name' => $sub_subject))->num_rows();
    }

    // 설정된 기간의 주제별 post의 comment 데이터의 개수 가져온다. (월별)
    public function count_sub_subject_comment_data_monthly_period($main_subject,$sub_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get_where('post_comment',array('main_subject_name' => $main_subject,'sub_subject_name' => $sub_subject))->num_rows();
    }
    

    // 메인 주제별 post의 comment_reply 데이터의 개수 가져온다.
    public function count_main_subject_comment_reply_data($main_subject)
    {
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $main_subject))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $post_comment_reply_temp = $this->db->get_where('post_comment_reply',array('org_idx' => $org_idx))->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 설정된 기간의(일별) 메인 주제별 post의 comment_reply 데이터의 개수 가져온다.
    public function count_main_subject_comment_reply_data_daily_period($main_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $main_subject))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
            $post_comment_reply_temp = $this->db->get_where('post_comment_reply',array('org_idx' => $org_idx))->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 설정된 기간의(월별) 메인 주제별 post의 comment_reply 데이터의 개수 가져온다.
    public function count_main_subject_comment_reply_data_monthly_period($main_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $main_subject))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
            $post_comment_reply_temp = $this->db->get_where('post_comment_reply',array('org_idx' => $org_idx))->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 하위 주제별 post의 comment_reply 데이터의 개수 가져온다.
    public function count_sub_subject_comment_reply_data($main_subject, $sub_subject)
    {
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $main_subject, 'sub_subject_name' => $sub_subject))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $post_comment_reply_temp = $this->db->get_where('post_comment_reply',array('org_idx' => $org_idx))->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 설정된 기간의(일별) 하위 주제별 post의 comment_reply 데이터의 개수 가져온다.
    public function count_sub_subject_comment_reply_data_daily_period($main_subject, $sub_subject,$start_day,$end_day)
    {
        // $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $main_subject, 'sub_subject_name' => $sub_subject))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
            $post_comment_reply_temp = $this->db->get_where('post_comment_reply',array('org_idx' => $org_idx))->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 설정된 기간의(월별) 하위 주제별 post의 comment_reply 데이터의 개수 가져온다.
    public function count_sub_subject_comment_reply_data_monthly_period($main_subject, $sub_subject,$start_day,$end_day)
    {
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";$last_day = explode('-',$end_day)[2];

        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $main_subject, 'sub_subject_name' => $sub_subject))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
            $post_comment_reply_temp = $this->db->get_where('post_comment_reply',array('org_idx' => $org_idx))->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 모든 post의 데이터의 수를 구한다.
    public function count_all_data()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 조건에 맞는 post의 데이터의 수를 구한다. (일 기준 누적)
    public function count_daily_post($day_temp)
    {
        $start_day = '2021-04-01';
        $last_day = date('Y-m-d',strtotime($day_temp.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 조건에 맞는 post의 데이터의 수를 구한다. (월 기준 누적)
    public function count_monthly_post($month_temp)
    {
        $start_day = '2021-04-01';
        $end_day_start = $month_temp.'-01';
        $month_days = date('t',strtotime($end_day_start));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 조건에 맞는 post의 데이터의 수를 구한다. (선택한 메인 주제의 특정 날짜 데이터)
    public function count_daily_sub_post($subject_name,$day_temp)
    {
        $this->db->where("main_subject_name = '$subject_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 조건에 맞는 post의 데이터의 수를 구한다. (선택한 메인 주제의 특정 월의 데이터)
    public function count_monthly_sub_post($subject_name,$month_temp)
    {
        $start_day = $month_temp.'-01';
        $end_day_start = $month_temp.'-01';
        $month_days = date('t',strtotime($end_day_start));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("main_subject_name = '$subject_name' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }
    
    // 조건에 맞는 post의 데이터의 수를 구한다.(메인 주제별, 특정 날짜 데이터)
    public function count_daily_subject_post($series_name,$day_temp)
    {
        
        $this->db->where("main_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');

        // echo json_encode(array('code' => 500, 'message' => $day_temp.' , '.$this->db->get('post_content')->num_rows(), 'extra' => null, 'debug' => null)); exit;

        return $this->db->get('post_content')->num_rows();
    }

    // 조건에 맞는 post의 데이터의 수를 구한다.(메인 주제별, 특정 월 데이터)
    public function count_monthly_subject_post($series_name,$month_temp)
    {
        $start_day = $month_temp.'-01'; // start_day를 설정해준다.
        $month_days = date('t',strtotime($start_day));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("main_subject_name = '$series_name' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
        
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 조건에 맞는 post의 데이터의 수를 구한다.(선택한 메인 주제의 하위 주제별, 특정 날짜 데이터)
    public function count_daily_sub_subject_post($subject_name,$series_name,$day_temp)
    {
        $this->db->where("main_subject_name = '$subject_name' AND sub_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 조건에 맞는 post의 데이터의 수를 구한다.(선택한 메인 주제의 하위 주제별, 특정 월의 데이터)
    public function count_monthly_sub_subject_post($subject_name,$series_name,$month_temp)
    {
        $start_day = $month_temp.'-01'; // start_day를 설정해준다.
        $month_days = date('t',strtotime($start_day));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $this->db->where("main_subject_name = '$subject_name' AND sub_subject_name = '$series_name' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.
        
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 설정된 기간의 모든 post의 데이터의 수를 구한다. (일 기준)
    public function count_all_data_daily_period($start_day,$end_day)
    {   
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 설정된 기간의 모든 post의 데이터의 수를 구한다. (월 기준)
    public function count_all_data_monthly_period($start_day,$end_day)
    {   
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";$last_day = explode('-',$end_day)[2];
    
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->num_rows();
    }

    // 해당 포스트의 모든 댓글 데이터를 가져온다. (인덱스가 있는 연관배열)
    public function get_all_comment($slug)
    {
        return $this->db->get_where('post_comment', array('post_slug' => $slug, 'is_del' => 'N'))->result_array();
    }

    // 해당 포스트의 모든 댓글 데이터의 수를 구한다.
    public function count_all_comment_by_slug($slug)
    {
        return $this->db->get_where('post_comment', array('post_slug' => $slug, 'is_del' => 'N'))->num_rows();
    }

    // 모든 comment 데이터의 수를 구한다.
    public function count_all_comment()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }
    // 조건에 맞는 comment 데이터의 수를 구한다. (일 기준 누적)
    public function count_daily_comment($day_temp)
    {
        $start_day = '2021-04-01';
        $last_day = date('Y-m-d',strtotime($day_temp.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)

        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 조건에 맞는 comment 데이터의 수를 구한다. (월 기준 누적)
    public function count_monthly_comment($month_temp)
    {
        $start_day = '2021-04-01';
        $end_day_start = $month_temp.'-01';
        $month_days = date('t',strtotime($end_day_start));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");
        
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 조건에 맞는 comment 데이터의 수를 구한다. (선택한 메인 주제의 특정 날짜 데이터)
    public function count_daily_sub_comment($subject_name,$day_temp)
    {
        $this->db->where("main_subject_name = '$subject_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 조건에 맞는 comment 데이터의 수를 구한다. (선택한 메인 주제의 특정 월의 데이터)
    public function count_monthly_sub_comment($subject_name,$month_temp)
    {
        $start_day = $month_temp.'-01';
        $last_day = date('t',strtotime($start_day));
        $end_day = $month_temp.'-'.$last_day; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)

        $this->db->where("main_subject_name = '$subject_name' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }
    
    // 조건에 맞는 comment 데이터의 수를 구한다. (일별)
    public function count_daily_subject_comment($series_name,$day_temp)
    {
        $this->db->where("main_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 조건에 맞는 comment 데이터의 수를 구한다. (월별)
    public function count_monthly_subject_comment($series_name,$month_temp)
    {
        $start_day = $month_temp.'-01'; // start_day를 설정해준다.
        $month_days = date('t',strtotime($start_day));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        $this->db->where("main_subject_name = '$series_name' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 조건에 맞는 comment 데이터의 수를 구한다.(선택한 메인 주제의 하위 주제별, 특정 날짜 데이터)
    public function count_daily_sub_subject_comment($subject_name,$series_name,$day_temp)
    {
        $this->db->where("main_subject_name = '$subject_name' AND sub_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 조건에 맞는 comment 데이터의 수를 구한다.(선택한 메인 주제의 하위 주제별, 특정 월의 데이터)
    public function count_monthly_sub_subject_comment($subject_name,$series_name,$month_temp)
    {
        $start_day = $month_temp.'-01'; // start_day를 설정해준다.
        $month_days = date('t',strtotime($start_day));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        $this->db->where("main_subject_name = '$subject_name' AND sub_subject_name = '$series_name' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 설정된 기간의 모든 comment 데이터의 수를 구한다. (일 기준)
    public function count_all_comment_daily_period($start_day,$end_day)
    {
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";

        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 설정된 기간의 모든 comment 데이터의 수를 구한다. (월 기준)
    public function count_all_comment_monthly_period($start_day,$end_day)
    {
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
      
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment')->num_rows();
    }

    // 모든 comment_reply의 데이터의 수를 구한다.
    public function count_all_comment_reply()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment_reply')->num_rows();
    }

    // 조건에 맞는 comment_reply 데이터의 수를 구한다. (일 기준 누적)
    public function count_daily_comment_reply($day_temp)
    {
        $start_day = '2021-04-01';
        $last_day = date('Y-m-d',strtotime($day_temp.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)

        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment_reply')->num_rows();
    }

    // 조건에 맞는 comment_reply 데이터의 수를 구한다. (월 기준 누적)
    public function count_monthly_comment_reply($month_temp)
    {
        $start_day = '2021-04-01'; // start_day를 설정해준다.
        $end_day_start = $month_temp.'-01';
        $month_days = date('t',strtotime($end_day_start));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");

        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment_reply')->num_rows();
    }

    // 조건에 맞는 comment_reply 데이터의 수를 구한다. (선택한 메인 주제의 특정 날짜 데이터)
    public function count_daily_sub_comment_reply($subject_name,$day_temp)
    {
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $subject_name))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("org_idx = '$org_idx' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
            $post_comment_reply_temp = $this->db->get('post_comment_reply')->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 조건에 맞는 comment_reply 데이터의 수를 구한다. (선택한 메인 주제의 특정 월의 데이터)
    public function count_monthly_sub_comment_reply($subject_name,$month_temp)
    {
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $subject_name))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        $start_day = '2021-04-01'; // start_day를 설정해준다.
        $end_day_start = $month_temp.'-01';
        $month_days = date('t',strtotime($end_day_start));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("org_idx = '$org_idx' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");

            $post_comment_reply_temp = $this->db->get('post_comment_reply')->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }
    
    // 조건에 맞는 comment_reply 데이터의 수를 구한다. (특정일)
    public function count_daily_subject_comment_reply($series_name,$day_temp)
    {
        // $this->db->where("main_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $series_name))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("org_idx = '$org_idx' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
            $post_comment_reply_temp = $this->db->get('post_comment_reply')->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 조건에 맞는 comment_reply 데이터의 수를 구한다. (특정월)
    public function count_monthly_subject_comment_reply($series_name,$month_temp)
    {
        // $this->db->where("main_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $series_name))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        $start_day = $month_temp.'-01'; // start_day를 설정해준다.
        $month_days = date('t',strtotime($start_day));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("org_idx = '$org_idx' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");

            $post_comment_reply_temp = $this->db->get('post_comment_reply')->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 조건에 맞는 comment_reply 데이터의 수를 구한다.(선택한 메인 주제의 하위 주제별, 특정 날짜 데이터)
    public function count_daily_sub_subject_comment_reply($subject_name,$series_name,$day_temp)
    {
        // $this->db->where("main_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $subject_name, 'sub_subject_name' => $series_name))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("org_idx = '$org_idx' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
            $post_comment_reply_temp = $this->db->get('post_comment_reply')->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 조건에 맞는 comment_reply 데이터의 수를 구한다.(선택한 메인 주제의 하위 주제별, 특정 월의 데이터)
    public function count_monthly_sub_subject_comment_reply($subject_name,$series_name,$month_temp)
    {
        // $this->db->where("main_subject_name = '$series_name' AND DATE_FORMAT(reg_date, '%Y-%m-%d') = '$day_temp'");
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->order_by('idx', 'DESC');
        $post_comment_data = $this->db->get_where('post_comment',array('main_subject_name' => $subject_name, 'sub_subject_name' => $series_name))->result_array(); // 주제에 해당하는 comment 정보를 가져온다.
        $post_comment_reply = array(); // comment_reply를 담을 배열을 초기화 한다.
        $start_day = $month_temp.'-01'; // start_day를 설정해준다.
        $month_days = date('t',strtotime($start_day));
        $end_day = $month_temp.'-'.$month_days; // 해당 월의 일수를 구해서 end_Day를 설정해준다.
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        for($i = 0; $i < count($post_comment_data); $i++){
            $org_idx = $post_comment_data[$i]['idx']; // 원본 코멘트의 idx를 가져온다.
            $this->db->where("org_idx = '$org_idx' AND reg_date BETWEEN DATE('$start_day') and DATE('$last_day')");

            $post_comment_reply_temp = $this->db->get('post_comment_reply')->result_array(); // 원본 코멘트의 idx에 해당하는 comment_reply들을 가져온다.
            for($j = 0; $j < count($post_comment_reply_temp);$j++){
                $post_comment_reply[] = $post_comment_reply_temp[$j]; // 원본 idx에 대한 comment_reply 요소들을 comment_reply 배열에 담는다. 
            }
        }
        return count($post_comment_reply);  // 해당 주제의 모든 post_comment_reply의 개수를 반환한다.
    }

    // 설정된 기간(일별)의 모든 comment_reply의 데이터의 수를 구한다.
    public function count_all_comment_reply_daily_period($start_day,$end_day)
    {
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment_reply')->num_rows();
    }

    // 설정된 기간(월별)의 모든 comment_reply의 데이터의 수를 구한다.
    public function count_all_comment_reply_monthly_period($start_day,$end_day)
    {
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";       
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_comment_reply')->num_rows();
    }

    // 선택한 메인 주제의 하위 주제 정보를 가져온다. (연관 배열)
    public function get_sub_subject($subject_code)
    {
        // "SELECT * FROM sub_subject ORDER BY 'idx' ASC";
        $this->db->order_by('idx', 'ASC'); // 오름차순 정렬
        return $this->db->get_where('sub_subject',array('main_subject_code' => $subject_code))->result_array();
    }

    // 포스트 순위 데이터를 가져온다. (연관 배열)
    public function get_post_ranking_data()
    {
        $this->db->order_by('ranking_point', 'DESC'); // 랭킹 포인트 기준으로 내림차순 정렬
        return $this->db->get('post_content')->result_array();
    }

    // 선택한 기간(일별)의 포스트 순위 데이터를 가져온다. (연관 배열)
    public function get_post_ranking_data_by_daily($start_day,$end_day)
    {
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('ranking_point', 'DESC'); // 랭킹 포인트 기준으로 내림차순 정렬
        return $this->db->get('post_content')->result_array();
    }

    // 선택한 기간(월별)의 포스트 순위 데이터를 가져온다. (연관 배열)
    public function get_post_ranking_data_by_monthly($start_day,$end_day)
    {
        $last_day = date('Y-m-d',strtotime($end_day.'+1 days')); // 설정 기간의 마지막 날을 생성해준다. (종료일에 하루를 더해준다.)
        // "SELECT * FROM post_content ORDER BY 'idx' DESC";
        $this->db->where("reg_date BETWEEN DATE('$start_day') and DATE('$last_day')"); // 기간 설정 조건을 쿼리에 추가한다.

        $this->db->order_by('ranking_point', 'DESC'); // 랭킹 포인트 기준으로 내림차순 정렬
        return $this->db->get('post_content')->result_array();
    }
}