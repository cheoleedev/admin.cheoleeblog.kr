<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Postwrite_model extends CI_model {

	function __construct(){ 
		parent::__construct(); 
        //위에서 설정한 /application/config/database.php 파일에서 $db['cheolee'] 설정값을 불러오겠다는 뜻입니다.
        $this->db = $this->load->database('db', TRUE);
	} 
    
    public function getAllData()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' DESC"; (내림차순 정렬, 최신 데이터가 위로)
        $this->db->order_by('idx', 'DESC');
        return $this->db->get('post_content')->result();
    }

    // 모든 메인 주제 데이터를 가져온다.
    public function get_all_main_subject()
    {
        // "SELECT * FROM post_content ORDER BY 'idx' ASC"; (오름차순 정렬, 등록순)
        $this->db->order_by('idx', 'ASC');
        return $this->db->get('main_subject')->result_array();
    }

    // 선택된 메인 주제를 가져온다. (연관 배열)
    public function get_selected_main_subject($main_subject_code)
    {
        // "SELECT * FROM main_subject ORDER BY 'idx' ASC";
        return $this->db->get_where('main_subject',array('main_subject_code' => $main_subject_code))->result_array();
    }

    // 선택한 메인 주제의 하위 주제 정보를 가져온다. (연관 배열)
    public function get_sub_subject($main_subject_code)
    {
        // "SELECT * FROM sub_subject ORDER BY 'idx' ASC";
        $this->db->order_by('idx', 'ASC'); //내림차순 정렬
        return $this->db->get_where('sub_subject',array('main_subject_code' => $main_subject_code))->result_array();
    }

    // 작성한 포스트 데이터를 테이블에 추가한다.
    public function insertPost($table, $data)
    {

       $this->db->insert($table, $data);

    }

    //포스트의 idx를 가져온다.
    public function get_idx($table,$title){
        $post = $this->db->get_where($table,array('title' => $title))->result_array();
        $idx = $post[0]['idx'];
        return $idx;
    }

    // 가져온 idx를 해당 포스트의 slug에 업데이트 한다.
    public function update_slug($table,$idx,$title){
        $this->db->update($table,array('slug' => $idx),array('title' => $title));
    }

    // 업데이트된 slug를 가져온다.
    public function get_slug($table,$idx) {
        $post = $this->db->get_where($table,array('idx' => $idx))->result_array();
        $slug = $post[0]['slug'];
        return $slug;
    }

    public function getDetailPost($idx)
    {
        return $this->db->get_where('post_content', array('idx' => $idx))->row_array();
    }

    // 조건에 맞는 댓글 정보를 가져온다.
    public function get_comment($where)
    {
        return $this->db->get_where('post_comment', $where)->result_array();
    }

    // 조건에 맞는 대댓글 정보를 가져온다.
    public function get_comment_reply($where)
    {
        return $this->db->get_where('post_comment_reply', $where)->result_array();
    }

    // 해당 포스트의 모든 댓글 데이터를 가져온다. (인덱스가 있는 연관배열)
    public function get_all_comment($slug)
    {
        return $this->db->get_where('post_comment', array('post_slug' => $slug, 'is_del' => 'N'))->result_array();
    }

    // 해당 포스트의 모든 대댓글 데이터를 가져온다. (인덱스가 있는 연관배열)
    public function get_all_comment_reply_by_slug($slug)
    {
        return $this->db->get_where('post_comment_reply', array('post_slug' => $slug, 'is_del' => 'N'))->result_array();
    }

    // 댓글 idx에 해당하는 대댓글을 모두 가져온다. (인덱스가 있는 연관배열)
    public function get_all_comment_reply_by_idx($org_idx)
    {
        return $this->db->get_where('post_comment_reply', array('org_idx' => $org_idx, 'is_del' => 'N'))->result_array();
    }

     // 작성한 댓글 데이터를 테이블에 추가한다.
     public function insert_comment($table, $data)
     {
        $this->db->insert($table, $data);
     }

    // 작성한 대댓글 데이터를 테이블에 추가한다.
    public function insert_comment_reply($table, $data)
    {
        $this->db->insert($table, $data);
    }

    // config 데이터를 가져온다. 
    public function get_config()
    {
        return $this->db->get('config')->row(); // 한줄의 '객체배열'을 반환한다.
    }
    
    // FUNCTION HAPUS
    public function deletePost($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete('post_content', $id);
    }

    public function getById($id)
    {
        return $this->db->get_where('post_content', $id)->row();
    }

    public function _deleteImage($id)
    {
        $posts = $this->getById($id);
        $filename = explode(".", $posts->thumbnail)[0];

        // delte old thumbnail
        $delImg = array_map('unlink', glob(FCPATH . "img/post/$filename.*"));
        $delImg = array_map('unlink', glob(FCPATH . "img/post/resize/$filename.*"));
        return $delImg;
    }
}